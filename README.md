# Planning under Uncertainty to Goal Distributions

This repository hosts code associated with our paper [Planning under Uncertainty to Goal Distributions](https://arxiv.org/abs/2011.04782).

---

## Installation

Currently supported:

  - Ubuntu 20.04
  - Python 3.8
  - ROS Noetic
  
ROS is mainly needed to create visualizations with rviz, but it also makes things easier from a build/setup perspective. We'll use conda and [RoboStack](https://robostack.github.io/) to manage dependencies (including ROS). Follow these steps:

1. Clone this repository and the [ll4ma_util](https://bitbucket.org/robot-learning/ll4ma_util/src/main/) package which is a necessary internal dependency:
	```bash
    cd $HOME
	mkdir -p catkin_ws/src
	cd catkin_ws/src
	git clone git@bitbucket.org:robot-learning/distribution_planning.git
	git clone git@bitbucket.org:robot-learning/ll4ma_util.git
	```
	
2. Run the setup script that creates a conda environment with all the necessary dependencies (it will install miniconda if conda is not already on your machine):
	```bash
	cd $HOME/catkin_ws/src/distribution_planning/conda
	./create_env.sh
	```
	
3. Open a new terminal and activate the conda env:
    ```bash
    conda activate dplan
	```
	
4. Build the catkin packages:
    ```bash
    cd $HOME/catkin_ws
	catkin init
	catkin build
	source devel/setup.bash
	```
	
5. If you want to always activate this environment, you can add these lines to your `.bashrc` file:
    ```bash   
    conda activate dplan
    source $HOME/catkin_ws/devel/setup.bash
    ```
	
6. Install the [manipulation](https://github.com/RussTedrake/manipulation) code from Russ Tedrake's course (needed for arm-reaching environment), note you can put it in any directory you want:
    ```bash
	cd $HOME
	mkdir source_code
	cd source_code
    git clone https://github.com/RussTedrake/manipulation.git
	cd manipulation
    sudo setup/ubuntu/18.04/install_prereqs.sh
    pip install --requirement requirements.txt
    export PYTHONPATH=`pwd`:${PYTHONPATH}
	```
	Note you will want to make that `PYTHONPATH` permanent in your `.bashrc`, I usually just manually type this into my `.bashrc` using the known path instead of that `pwd` command.
	
7. Install [my fork of the bingham package](https://github.com/adamconkey/bingham):
    ```bash
	cd $HOME/source_code
	git clone https://github.com/adamconkey/bingham.git
	cd bingham/c
	make
    sudo make install
	cd ../python
	CFLAGS="-I../c/local/include" LDFLAGS="-L../c/local/lib" python setup.py build_ext --inplace
    CFLAGS="-I../c/local/include" LDFLAGS="-L../c/local/lib" python setup.py install
	```
	
It's likely you hit a problem somewhere in those steps, I wrote this mostly from memory. So ask Adam or raise an issue on this repo if you couldn't get things installed. 

---

## Usage

This is primarily for my own memory to know how I ran the experiments, but it can also serve as a guide to newcomers for some relevant examples.

### Dubins

- Generate plans for each of the examples (this one for uniform example):
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts
  python gradient_solver --config dubins_example_uniform.yaml
  ```
  Note you just need to change the config filename to get the other examples. Do `ls ../config | grep dubins` to see the other options.

- Generate images for paper figures (except point-based plans, done separately):
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dubins
  python dubins_example_figs.py
  ```
  This assumes you've already generated all the plans using the previous step. You can specify a list of specific ones if you don't want to run all of them (e.g. `-e uniform gaussian`).
  
- Generate plans for point-based goals sampled from goal region:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dubins
  python generate_dubins_point_plans.py
  ```
  
- Cleanup the point-based plans (remove ones that didn't actually reach goal region due to getting stuck in local optima):
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dubins
  python cleanup_dubins_point_plans.py
  ```

- Plot point-based plans (last subfigure in the paper for Dubins example figure):
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dubins
  python plot_dubins_point_plans.py
  ```
  
### Ball-Rolling

- Generate the plans for all of the different objectives:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/minigolf
  run_minigolf_solver.py
  ```
- Generate images for paper figures and animations for paper video:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/minigolf
  create_amplifier_figs.py
  ```

### Moving Target

- Do the MPC run to get the results:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts
  python sampling_solver.py --config di_dynamic_goal.yaml --mpc --mpc_iters 70 --iso_var 0.02 --end_iso_var 0.002 --seed 1
  ```

- Create the figure for the paper:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dynamic_goal
  python create_dg_fig.py
  ```

- Create the upper row of cover figure for the paper:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/dynamic_goal
  python create_dg_cover_fig.py
  ```

### Arm-Reaching

- Generate reachable/unreachable data by spawning object randomly around the scene and generating samples from PMM:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/arm_reaching
  python generate_reachable_data.py
  ```
  Note that there's a memory leak somewhere, very very likely in the Python bindings I made for the Bingham code. If you have 32GB of ram then you can probably only collect about 100 instances per session. But the script is setup to continue where it left off, so you just have to periodically increase `--n_poses` as you run the script multiple times.

- Run solver to get arm-reaching plans and numerical results for experiments:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/arm_reaching
  python run_arm_reaching.py
  ```
  You can run that with different seeds (e.g. `--seed 4`) to get different results. Note you can also view numerical results of previously generated plans simply by passing the directory where the run data was saved to the `--save_dir` flag. It will load the data and print the results to terminal. It will only erase that data and re-run the planning if you additionally set the `--regenerate` flag.

- Create identically cropped images from arm-reaching videos for paper:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/arm_reaching
  python create_reaching_vid_imgs.py
  ```
  
- Show color map for appendix:
  ```bash
  roscd distribution_planning/src/distribution_planning/scripts/experiments/arm_reaching
  python show_cmap.py
  ```
  
---