#!/usr/bin/env python
import sys
import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation


class Arm:
    """
    Base class implementing functionality of a robot arm with differentiable forward kinematics.
    """
    def __init__(self, alphas, Ds, As, urdf_path='', ee_index=-1, modified_DH=False):
        """
        Args:
            alphas: List of 'alpha' DH parameters.
            Ds: List of 'd' DH parameters.
            As: List of 'a' DH parameters.

        Note: Assuming all revolute joints, so Ds are always fixed and thetas always variable.
        """
        self.alphas = torch.tensor(alphas).unsqueeze(-1)
        self.Ds = torch.tensor(Ds).unsqueeze(-1)
        self.As = torch.tensor(As).unsqueeze(-1)
        self.n_joints = len(alphas)
        self.urdf_path = urdf_path
        self.ee_index = ee_index
        self.modified_DH = modified_DH
        self.weights = torch.eye(3)

    def fk(self, thetas, return_type='2d_pos'):
        """
        Computes forward kinematics.

        Args:
            thetas (Tensor): Joint angles
        Returns:
            T: Homogeneous TF matrix representing end-effector 3D pose.
        """
        T = self.fk_links(thetas)[-1]
        if return_type == '2d_pos':
            return self._pos_from_homogeneous(T)[:2]
        elif return_type == '3d_pos':
            return self._pos_from_homogeneous(T)
        elif return_type == 'homo':
            return T
        else:
            raise ValueError(f"Unknown return type for FK: {return_type}")

    def fk_links(self, thetas):
        """
        Computes forward kinematics for all links.

        Args:
            thetas (Tensor): Joint angles
        Returns:
            Ts (list): List of homogeneous TF matrices, one for each link in kinematic chain.
        """
        if not isinstance(thetas, torch.Tensor):
            thetas = torch.tensor(thetas)
        if not thetas.size(-1) == 1:
            thetas = thetas.unsqueeze(-1)
        Ts = []
        T = torch.eye(4, dtype=torch.double)
        for i in range(len(thetas)):
            T_i = self._compute_T(self.alphas[i], thetas[i], self.Ds[i], self.As[i])
            T = torch.mm(T, T_i)
            Ts.append(T)
        return Ts

    def jacobian(self, thetas):
        if not isinstance(thetas, torch.Tensor):
            thetas = torch.tensor(thetas)
        return torch.autograd.functional.jacobian(self.fk, thetas).squeeze(-1)
    
    def get_link_positions(self, thetas):
        """
        Computes the position of each link in Cartesian space w.r.t. to robot base.
        """
        return [self._pos_from_homogeneous(T) for T in self.fk_links(thetas)]
                
    def _compute_T(self, alpha, theta, d, a):
        """
        Computes homogeneous transformation matrix.

        If self.modified_DH=True, uses modified DH convention:
        https://en.wikipedia.org/wiki/Denavit%E2%80%93Hartenberg_parameters#Modified_DH_parameters
        """
        ct = torch.cos(theta)
        st = torch.sin(theta)
        ca = torch.cos(alpha)
        sa = torch.sin(alpha)
        # These are a little silly, but necessary for autograd not to yell at you
        one = torch.tensor([1.0])
        zero = torch.tensor([0.0])
        
        if self.modified_DH:
            T = torch.stack([
                torch.stack([     ct,      -st, zero,       a]),
                torch.stack([st * ca,  ct * ca,  -sa, -d * sa]),
                torch.stack([st * sa,  ct * sa,   ca,  d * ca]),
                torch.stack([   zero,     zero, zero,     one])
            ])
        else:
            T = torch.stack([
                torch.stack([  ct, -st * ca,  st * sa, a * ct]),
                torch.stack([  st,  ct * ca, -ct * sa, a * st]),
                torch.stack([zero,       sa,       ca,      d]),
                torch.stack([zero,     zero,     zero,    one])
            ])
            
        return T.squeeze()

    def _pos_from_homogeneous(self, T):
        return T[:3, 3]
    
    def _rot_from_homogeneous(self, T):
        return T[:3, :3]


    
class NLinkArm(Arm):

    def __init__(self, alphas, Ds, As, urdf_path=''):
        super().__init__(alphas, Ds, As, urdf_path)

    def visualize(self, thetas, interval=50, ws_buffer=1):
        """
        https://bitbucket.org/robot-learning/ll4ma-opt-sandbox/src/484ec904f15e9a76bb3795197bc88a22d9f52aee/problems/ik_problem.py
        """
        fig, ax = plt.subplots(1,1)
        fig.set_size_inches(8, 8)
        arm_length = torch.sum(self.As).item()
        ws = arm_length + ws_buffer
        ax.set_xlim(-ws,ws)
        ax.set_ylim(-ws,ws)
        
        arm = ax.plot([], [], 'o-', lw=3, color='green')[0]

        def animate(i):
            xs, ys = self._get_draw_coordinates(thetas[i])
            xs.insert(0, 0)
            ys.insert(0, 0)
            arm.set_data(xs, ys)
            fig.canvas.draw()
            return [arm]
        
        anim = animation.FuncAnimation(fig, animate, frames=thetas.shape[0], interval=interval)
        plt.tight_layout()
        plt.show()
        return anim

    def _get_draw_coordinates(self, thetas):
        """
        Helper function for creating the (x,y) points to be drawn with lines.
        """
        points = self.get_link_positions(thetas)
        xys = [p.cpu().numpy().tolist()[:2] for p in points]
        xs, ys = zip(*xys)
        return list(xs), list(ys)
    
    
class TwoLinkArm(NLinkArm):

    def __init__(self, alphas=[0,0], Ds=[0,0], As=[1,1]):
        super().__init__(alphas, Ds, As)
    

class ThreeLinkArm(NLinkArm):

    def __init__(self, alphas=[0,0,0], Ds=[0,0,0], As=[1,1,1]):
        super().__init__(alphas, Ds, As)
        

if __name__ == '__main__':
    
    arm = ThreeLinkArm()
    # joints = np.stack([np.linspace(0, np.pi/arm.n_joints, 100) for _ in range(arm.n_joints)]).T
    # arm.visualize(joints)

    # q1 = torch.DoubleTensor(3, 1).uniform_(-np.pi/4, np.pi/4)
    q1 = torch.tensor([np.pi/5]*arm.n_joints, dtype=torch.double).unsqueeze(-1)
    q_diff = torch.DoubleTensor(3, 1).uniform_(-0.1, 0.1)
    # q_diff = torch.tensor([0.01]*arm.n_joints, dtype=torch.double).unsqueeze(-1)
    q2 = q1 + q_diff

    x1 = arm.fk(q1)
    x2 = arm.fk(q2)
    x_diff = x2 - x1

    J = arm.jacobian(q1)

    np.set_printoptions(precision=5)
    
    print("Q1", q1.squeeze().cpu().numpy())
    print("Q2", q2.squeeze().cpu().numpy())
    print("X1", x1.squeeze().cpu().numpy())
    print("X2", x2.squeeze().cpu().numpy())
    print("J", J.cpu().numpy())
    print("ACTUAL", x_diff.squeeze().cpu().numpy())
    print("COMPUTED", torch.mm(J, q_diff).squeeze().cpu().numpy())
