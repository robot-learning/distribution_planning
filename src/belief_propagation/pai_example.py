#!/usr/bin/env python
import os
import sys
import numpy as np
import numpy.matlib
import time
import random

import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

# TODO hacking this
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(__file__)), "util"))

from arm import NLinkArm


class Variable:

    def __init__(self, name, dims=2):
        self.name = name
        self.dims = dims
        
        self.edges = {}

        # For drawing
        self.mu = np.matlib.zeros(dims).T
        self.Sigma = np.matlib.eye(dims)


    def send_message(self, out_edge_name):
        print(self.name, "->", out_edge_name)
        
        eta = np.matlib.zeros(self.dims).T
        Lambda = np.matlib.zeros((self.dims, self.dims))

        for name, edge in self.edges.items():
            if name == out_edge_name:
                # Exclude the edge that you're sending the message to
                continue
            print(f"Taking input from {name}")
            # Multiply incoming messagges by adding precisions
            eta += edge.factor_to_variable_message.eta
            Lambda += edge.factor_to_variable_message.Lambda
        self.edges[out_edge_name].variable_to_factor_message.eta = eta.copy()
        self.edges[out_edge_name].variable_to_factor_message.Lambda = Lambda.copy()

        # Update local state estimate from all messages including outward one
        eta += self.edges[out_edge_name].factor_to_variable_message.eta
        Lambda += self.edges[out_edge_name].factor_to_variable_message.Lambda
        # if Lambda[0,0] != 0:
        self.Sigma = Lambda.I
        self.mu = self.Sigma * eta
        self.edges[out_edge_name].variable_to_factor_message.mu = self.mu.copy()
        
    def send_all_messages(self):
        for name in self.edges.keys():
            self.send_message(name)

    def update_mu_sigma(self):
        eta = np.matlib.zeros(self.dims).T
        Lambda = np.matlib.zeros((self.dims, self.dims))

        for edge in self.edges.values():

            # Multiply incoming messages from other factors by adding precisions
            eta += edge.factor_to_variable_message.eta
            Lambda += edge.factor_to_variable_message.Lambda
            # print("LAMBDA", Lambda)
            
        self.Sigma = Lambda.I
        self.mu = self.Sigma * eta

    def draw(self, ax):
        ellipse = Ellipse(self.mu, np.sqrt(self.Sigma[0,0]), np.sqrt(self.Sigma[1,1]))
        ax.add_patch(ellipse)
        

class Factor:

    def __init__(self, name, edges, z):
        self.name = name
        self.edges = edges
        self.z = z
        self.dims = len(z)

    def send_message(self, out_edge_name):
        print(self.name, "->", out_edge_name)

        # TODO linearising, but if it's linear then they'll be constant so you
        # probably shouldn't always do this
        
        # x0     (K_1 + ... + K_N, 1) for N incoming edges each with size K_i
        # J      (K_s, K_1 + ... + K_N)
        # J * x0 (K_s, 1)
        # z      (K_s, 1)
        # h      (K_s, 1)
        h_local = self.h()
        J_local = self.J()
        J_T_Lambda = J_local.T * self.Lambda  # Just to avoid duplicate computation        
        Lambda = J_T_Lambda * J_local
        eta = J_T_Lambda * (J_local * self.x0() + self.z - h_local)
        
        # Condition factors on messages from all edges apart from outward one
        offset = 0
        out_offset = 0
        out_size = 0
        for name, edge in self.edges.items():
            edge_eta = edge.variable_to_factor_message.eta
            edge_Lambda = edge.variable_to_factor_message.Lambda
            edge_size = edge_eta.shape[0]
            if name != out_edge_name:
                # For edges which are not the outward one, condition
                eta[offset:offset+edge_size,:] += edge_eta
                Lambda[offset:offset+edge_size,offset:offset+edge_size] += edge_Lambda
            else:
                # Remember where in the matrix the outward one is
                out_offset = offset
                out_size = edge_size
            offset += edge_size

        # Restructure eta and Lambda so the outward variable of interest is at the top
        eta_top_temp = eta[0:out_size,:].copy()
        eta[0:out_size,:] = eta[out_offset:out_offset+out_size,:]
        eta[out_offset:out_offset+out_size,:] = eta_top_temp
        Lambda_top_temp = Lambda[0:out_size,:].copy()
        Lambda[0:out_size,:] = Lambda[out_offset:out_offset+out_size,:]
        Lambda[out_offset:out_offset+out_size,:] = Lambda_top_temp
        Lambda_left_temp = Lambda[:,0:out_size].copy()
        Lambda[:,0:out_size] = Lambda[:,out_offset:out_offset+out_size]
        Lambda[:,out_offset:out_offset+out_size] = Lambda_left_temp

        # To marginalise, first set up subblocks as in Eustice
        ea = eta[0:out_size,:]
        eb = eta[out_size:,:]
        aa = Lambda[0:out_size,0:out_size]
        ab = Lambda[0:out_size,out_size:]
        ba = Lambda[out_size:,0:out_size]
        bb = Lambda[out_size:,out_size:]
        
        bb_inv = bb.I
        emarg = ea - ab * bb_inv * eb
        Lmarg = aa - ab * bb_inv * ba

        for row in range(out_size):
            self.edges[out_edge_name].factor_to_variable_message.eta[row,0] = emarg[row,0]
            for col in range(out_size):
                self.edges[out_edge_name].factor_to_variable_message.Lambda[row,col] = Lmarg[row,col]

    def send_all_messages(self):
        for name in self.edges.keys():
            self.send_message(name)
       
    def x0(self):
        # Get current overall stacked state vector estimate from input edges.
        # Shape (K_1 + ... + K_N, 1) for N incoming edges each with size K_i
        # for e in self.edges.values():
        #     print(self.name, e.variable_to_factor_message.mu.shape)
        return np.concatenate([e.variable_to_factor_message.mu for e in self.edges.values()])

    def h(self):
        raise NotImplementedError()

    def J(self):
        raise NotImplementedError()
    

class PoseDynamicsFactor(Factor):

    def __init__(self, timestep, edges, observed, sigma=0.1):
        self.Lambda = np.matlib.eye(2) / sigma**2
        self.timestep = timestep
        name = f"pose_dynamics_x{t}_x{t+1}"
        super().__init__(name, edges, observed)

    def h(self):
        # This factor computes relative pose between two timesteps, so simply take
        # the difference between the values estimated at each timestep        
        x_t = self.edges[f"x{self.timestep}"].variable_to_factor_message.mu
        x_tp1 = self.edges[f"x{self.timestep+1}"].variable_to_factor_message.mu
        return x_tp1 - x_t

    def J(self):
        # This maps from space of two temporally consecutive poses (each size 2 for xy
        # for total size of 4), to a space of relative poses between those timesteps
        # (size 2). It works out to this constant linear map
        return np.matrix([[-1.0,  0.0, 1.0, 0.0],
                          [ 0.0, -1.0, 0.0, 1.0]])


class VectorFactor(Factor):

    def __init__(self, name, edges, observed, sigma=0.1):
        self.Lambda = np.matlib.eye(len(observed)) / sigma**2
        super().__init__(name, edges, observed)

    def h(self):
        return list(self.edges.values())[0].variable_to_factor_message.mu

    def J(self):
        return np.matlib.eye(self.dims)


class JointDynamicsFactor(Factor):

    # TODO I think this is fundamentally not different from task dynamics factor,
    # only the dimensionality (and perhaps your starting params) are different.
    # So these can be collapsed into a single kind of factor that you just set
    # the dims differently
    
    def __init__(self, name, edges, observed, sigma=0.1):
        self.n_joints = len(observed) 
        self.Lambda = np.matlib.eye(self.n_joints) / sigma**2
        super().__init__(name, edges, observed)
        
    def h(self):
        q_t = self.edges[0].variable_to_factor_message.mu
        q_tp1 = self.edges[1].variable_to_factor_message.mu
        return q_tp1 - q_t

    def J(self):
        # Needs to be K x (K_1 + ... + K_N) where K_1...K_N are incoming variables,
        # which I think is time t and time t+1 joint positions, so it amounts to K x 2K
        return np.concatenate([-np.eye(self.n_joints), np.eye(self.n_joints)], axis=-1)


class KinematicsFactor(Factor):

    def __init__(self, timestep, edges, q_observed, sigma=0.1):
        self.timestep = timestep
        name = f'kinematics_{timestep}'

        n_joints = len(q_observed)
        self.arm = NLinkArm([0]*n_joints, [0]*n_joints, [1]*n_joints)
        x_observed = np.asmatrix(self.arm.fk(q_observed).unsqueeze(-1).numpy())
        
        self.Lambda = np.matlib.eye(2) / sigma**2
        super().__init__(name, edges, x_observed)
        
    def h(self):
        # Computing diff between FK pose and current pose belief, not sure if this is correct
        x = self.edges[f'x{self.timestep}'].variable_to_factor_message.mu
        q = self.edges[f'q{self.timestep}'].variable_to_factor_message.mu
        x_fk = np.asmatrix(self.arm.fk(q).unsqueeze(-1).numpy())
        return x - x_fk

    def J(self):
        # Concat of identity and negated Jacobian, I think that's what you want if you're
        # computing diff between actual and FK?
        q = self.edges[f'q{self.timestep}'].variable_to_factor_message.mu

        print("Q", q)
        arm_J = np.matrix(self.arm.jacobian(q).numpy()) # (2, 3) for 3R arm
        return np.concatenate([np.matlib.eye(2), -arm_J], axis=-1)
    
    
class VariableToFactorMessage:
    def __init__(self, dims):
        self.eta = np.zeros((dims, 1))
        self.Lambda = np.zeros((dims, dims))
        self.mu = np.zeros((dims, 1))

        
class FactorToVariableMessage:
    def __init__(self, dims):
        self.eta = np.zeros((dims, 1))
        self.Lambda = np.zeros((dims, dims))


class Edge:
    """
    Every edge between a variable node and a factor node is via an edge
    so the variable and factor only need to know about the edge, not about
    each other's internals. An edge stores the recent message in each direction; 
    dimension is the state space size.
    """
    def __init__(self, dims):
        self.dims = dims
        self.variable_to_factor_message = VariableToFactorMessage(dims)
        self.factor_to_variable_message = FactorToVariableMessage(dims)
        self.variable = None
        self.factor = None

    def set_variable(self, variable):
        if variable.dims != self.dims:
            raise ValueError(f"Tried to set variable for edge with {variable.dims} "
                             f"dims but expected {self.dims}")
        self.variable = variable

    def set_factor(self, factor):
        self.factor = factor
    
        
if __name__ == '__main__':
    factors = {}

    T = 8
    n_joints = 3

    start_x = np.matrix([[2.0], [0.5]])
    goal_x = np.matrix([[-1.5], [0.5]])
    start_q = np.matrix([[0.6]]*n_joints)
    
    start_sigma = 0.01
    goal_sigma = 0.05
    dyn_sigma = 0.7
    kin_sigma = 0.1
    
    x_vars = [Variable(f'x_{i}', dims=2) for i in range(T)]
    q_vars = [Variable(f'q_{i}', dims=n_joints) for i in range(T)]
    
    # Add initial factor for observations at first timestep
    edge_x_init = Edge(2)
    edge_x_init.set_variable(x_vars[0])
    factor = VectorFactor('x_obs', {'x_obs': edge_x_init}, start_x, start_sigma)
    factors['x_obs'] = factor
    edge_x_init.set_factor(factor)
    x_vars[0].edges['start'] = edge_x_init

    # edge_q_init = Edge(3)
    # edge_q_init.set_variable(q_vars[0])
    # factor = VectorFactor('q_obs', {'q_obs': edge_q_init}, start_q, start_sigma)
    # factors['q_obs'] = factor
    # edge_q_init.set_factor(factor)
    # q_vars[0].edges['q0'] = edge_q_init

    for t in range(0, T-1):
        # Add pose dynamics factors
        xt_to_f = Edge(2)
        xt_to_f.set_variable(x_vars[t])
        xtp1_to_f = Edge(2)
        xtp1_to_f.set_variable(x_vars[t+1])
        factor = PoseDynamicsFactor(t, {f'x{t}': xt_to_f, f'x{t+1}': xtp1_to_f},
                                    start_x, dyn_sigma)
        factors[factor.name] = factor
        xt_to_f.set_factor(factor)
        xtp1_to_f.set_factor(factor)
        x_vars[t].edges[factor.name] = xt_to_f
        x_vars[t+1].edges[factor.name] = xtp1_to_f

        # # Add kinematics factors
        # qt_to_f = Edge(n_joints)
        # qt_to_f.set_variable(q_vars[0])
        # xt_to_f = Edge(2)
        # xt_to_f.set_variable(x_vars[0])
        # factor = KinematicsFactor(t, {f'x{t}': xt_to_f, f'q{t}': qt_to_f},
        #                           start_q, kin_sigma)
        # factors[factor.name] = factor
        # qt_to_f.set_factor(factor)
        # xt_to_f.set_factor(factor)
        # q_vars[t].edges[factor.name] = qt_to_f
        # x_vars[t].edges[factor.name] = xt_to_f
        
    # Add last factor for goal distribution
    edge_goal = Edge(2)
    edge_goal.set_variable(x_vars[-1])
    factor = VectorFactor('x_goal', {'goal': edge_goal}, goal_x, goal_sigma)
    factors[factor.name] = factor
    edge_goal.set_factor(factor)
    x_vars[-1].edges["goal"] = edge_goal
     
    # Init message
    factors['x_obs'].send_message('x0')
    # x_vars[0].send_all_messages()

    factors['x_goal'].send_message('goal')

    # TODO I think the initial belief isn't updating q0, which makes it zero and then
    # that's a singular arm Jacobian so you get singular matrix

    # print("BEFORE")
    # print(q_vars[0].edges['q0'].variable_to_factor_message.mu)
    # factors['q_obs'].send_message('q0')
    # q_vars[0].send_all_messages()
    # print("AFTER")
    # print(q_vars[0].edges['q0'].variable_to_factor_message.mu)

    
    n_iters = 10
    print(f"Running for {n_iters} iterations...")
    for _ in range(n_iters):

                
        # Forward messages for EE poses
        for t in range(0, T-1, 1):
            factor_name = f'pose_dynamics_x{t}_x{t+1}'
            x_vars[t].send_message(factor_name)

            sys.exit()
            
            factors[factor_name].send_message(f'x{t+1}')

        # factors['x_goal'].send_message('goal')
                
        # Backward messages for EE poses
        for t in range(T-1, 0, -1):
            print("T", t)
            factor_name = f'pose_dynamics_x{t-1}_x{t}'
            x_vars[t].send_message(factor_name)
            factors[factor_name].send_message(f'x{t-1}')

        # factors['x_obs'].send_message('x0')
            
        # factors['x_obs'].send_message('x0')
        # factors['x_goal'].send_message('goal')

            
        # for x_var in x_vars:
        #     x_var.send_all_messages()
        # for factor in factors.values():
        #     factor.send_all_messages()
        
        # # Kinematic coupling
        # for t in range(T-1):
        #     q_vars[t].send_message(f'kinematics_{t}')
        #     factors[f'kinematics_{t}'].send_message(f'x{t}')



        
        # for q_var in q_vars:
        #     q_var.send_all_messages()
        # for factor in factors:
        #     factor.send_all_messages()
    print("\nDONE\n")

    for x_var in x_vars:
        x_var.update_mu_sigma()
        print(x_var.name, x_var.mu)
        # print(f"VAR {x_var.name}\n MU {x_var.mu}\n SIGMA {x_var.Sigma}\n")


    # Display results
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(14, 6)
    
    for x_var in x_vars:
        x_var.draw(ax)

    ax.set_xlim(-3, 3)
    ax.set_ylim(-1, 1)
    ax.set_aspect('equal', adjustable='box')
    plt.show()
