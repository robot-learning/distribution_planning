#!/usr/bin/env python
import sys
import numpy as np
import math
import time
import pygame
import random
from pygame.locals import *
pygame.init()


# set the width and height of the screen (pixels)
WIDTH = 1000
HEIGHT = 1000

SIZE = [WIDTH, HEIGHT]
BLACK = (0,0,0)
GREEN = (0,255,0)
LIGHTBLUE = (0,180,255)
DARKBLUE = (0,40,160)
RED = (255,100,0)
WHITE = (255,255,255)
BLUE = (0,0,255)
GREY = (110,110,110)




# Screen centre will correspond to (x, y) = (0, 0)
u0 = 100
v0 = HEIGHT - 100

POINTSXRANGE = 10.0
POINTSYRANGE = 10.0
NUMBEROFPOINTS = 20
NUMBEROFFACTORS = 50

kx = (WIDTH - 200) / float(POINTSXRANGE)

# For printing text
myfont = pygame.font.SysFont("Jokerman", 40)

# Standard deviations
MEASUREMENTsigma = 0.5
POSEsigma = 2.0

# For laying out measurements in simulation: only connect points up to this distance apart
MAXMEASDISTANCE = 4.0

# For batch solution
big_Sigma = -1
big_mu = -1



class Variable:

    def __init__(self, idx):
        self.idx = idx
        self.edges = []
        self.updated = False

        # For drawing
        self.mu = np.matrix([[0.0], [0.0]])
        self.Sigma = np.matrix([[10.0, 0.0], [0.0, 10.0]])


    def send_message(self, out_edge_idx):
        eta = np.matrix([[0.0], [0.0]])
        Lambda_prime = np.matrix([[0.0, 0.0], [0.0, 0.0]])

        for idx, edge in enumerate(self.edges):
            if idx == out_edge_idx:
                continue
            # Multiply incoming messagges by adding precisions
            eta += edge.factor_to_variable_message.eta
            Lambda_prime += edge.factor_to_variable_message.Lambda
        self.edges[out_edge_idx].variable_to_factor_message.eta = eta.copy()
        self.edges[out_edge_idx].variable_to_factor_message.Lambda = Lambda_prime.copy()

        # Update local state estimate from all messages including outward one
        eta += self.edges[out_edge_idx].factor_to_variable_message.eta
        Lambda_prime += self.edges[out_edge_idx].factor_to_variable_message.Lambda
        if Lambda_prime[0,0] != 0:
            self.Sigma = Lambda_prime.I
        self.mu = self.Sigma * eta
        self.edges[out_edge_idx].variable_to_factor_message.mu = self.mu.copy()
        
    def send_all_messages(self):
        for i, edge in enumerate(self.edges):
            self.send_message(i)

    def update_mu_sigma(self):
        eta = np.matrix([[0.0], [0.0]])
        Lambda_prime = np.matrix([[0.0, 0.0], [0.0, 0.0]])

        # Multiply inward messages from other factors
        for idx, edge in enumerate(self.edges):
            # Multiply incoming messages by adding precisions
            eta += edge.factor_to_variable_message.eta
            Lambda_prime += edge.factor_to_variable_message.Lambda

        self.Sigma = Lambda_prime.I
        self.mu = self.Sigma * eta

    def draw(self):
        color = GREEN if self.updated else LIGHTBLUE
        pygame.draw.circle(screen, color,
                           (int(u0 + kx * self.mu[0,0]), int(v0 - kx * self.mu[1,0])), 4, 0)
        sigma = math.sqrt(self.Sigma[0,0])
        pygame.draw.circle(screen, LIGHTBLUE,
                           (int(u0 + kx * self.mu[0,0]), int(v0 - kx * self.mu[1,0])),
                           int(sigma * kx), 1)
            

class Factor:

    def __init__(self, idx, edges, z):
        self.idx = idx
        self.edges = edges
        self.z = z

        self.eta_stored = -1
        self.Lambda_prime_stored = -1

        self.recalculate_eta_lambda_prime()

    def send_message(self, out_edge_idx):
        # Copy factor precision vector and matrix from stored; could recalculate every time
        # here if needed.
        eta = self.eta_stored.copy()
        Lambda_prime = self.Lambda_prime_stored.copy()

        # Next condition factors on messages from all edges apart from outward one
        offset = 0
        out_offset = 0
        out_size = 0
        for idx, edge in enumerate(self.edges):
            edge_eta = edge.variable_to_factor_message.eta
            edge_Lambda = edge.variable_to_factor_message.Lambda
            edge_size = edge_eta.shape[0]
            if idx != out_edge_idx:
                # For edges which are not the outward one, condition
                eta[offset:offset+edge_size,:] += edge_eta
                Lambda_prime[offset:offset+edge_size,offset:offset+edge_size] += edge_Lambda
            else:
                # Remember where in the matrix the outward one is
                out_offset = offset
                out_size = edge_size
            offset += edge_size

        # Now restructure eta and Lambdaprime so the outward variable of interest is at the top
        eta_top_temp = eta[0:out_size,:].copy()
        eta[0:out_size,:] = eta[out_offset:out_offset+out_size,:]
        eta[out_offset:out_offset+out_size,:] = eta_top_temp
        Lambda_prime_top_temp = Lambda_prime[0:out_size,:].copy()
        Lambda_prime[0:out_size,:] = Lambda_prime[out_offset:out_offset+out_size,:]
        Lambda_prime[out_offset:out_offset+out_size,:] = Lambda_prime_top_temp
        Lambda_prime_left_temp = Lambda_prime[:,0:out_size].copy()
        Lambda_prime[:,0:out_size] = Lambda_prime[:,out_offset:out_offset+out_size]
        Lambda_prime[:,out_offset:out_offset+out_size] = Lambda_prime_left_temp

        # To marginalise, first set up subblocks as in Eustice
        ea = eta[0:out_size,:]
        eb = eta[out_size:,:]
        aa = Lambda_prime[0:out_size,0:out_size]
        ab = Lambda_prime[0:out_size,out_size:]
        ba = Lambda_prime[out_size:,0:out_size]
        bb = Lambda_prime[out_size:,out_size:]

        bb_inv = bb.I
        emarg = ea - ab * bb_inv * eb
        Lmarg = aa - ab * bb_inv * ba

        for row in range(out_size):
            self.edges[out_edge_idx].factor_to_variable_message.eta[row,0] = emarg[row,0]
            for col in range(out_size):
                self.edges[out_edge_idx].factor_to_variable_message.Lambda[row,col] = Lmarg[row,col]

    def send_all_messages(self):
        for idx, edge in enumerate(self.edges):
            self.send_message(idx)
       
    def x0(self):
        # Get current overall stacked state vector estimate from input edges
        for idx, edge in enumerate(self.edges):
            if idx == 0:
                x0_local = edge.variable_to_factor_message.mu
            else:
                x0_local = np.concatenate((x0_local, edge.variable_to_factor_message.mu), axis=0)
        return x0_local

    def h(self):
        raise NotImplementedError()

    def J(self):
        raise NotImplementedError()
    
    def recalculate_eta_lambda_prime(self):
        # Relinearise factor if needed (with linear factor only need to call once)
        
        # First form eta and Lambdaprime for linearised factor
        # Recalculate h and J in principle so we can relinearise
        h_local = self.h()
        J_local = self.J()
        self.Lambda_prime_stored = J_local.T * self.Lambda * J_local
        self.eta_stored = J_local.T * self.Lambda * (J_local * self.x0() + self.z - h_local)

        # Let M = K_1 + ... + K_N for N incoming edges each with size K_i
        # Lambda (K_s, K_s)
        # Lambda_prime (M, M)
        # Eta (M, 1)
        # x0     (M, 1) 
        # J      (K_s, M)
        # J * x0 (K_s, 1)
        # z      (K_s, 1)
        # h      (K_s, 1)


class TwoDMeasFactor(Factor):
    """
    Factor for relative measurement between points
    """
    
    def __init__(self, idx, edges, ground_truth_measurement):
        self.Lambda = np.matrix([[1.0 / (MEASUREMENTsigma * MEASUREMENTsigma), 0.0],
                                 [0.0, 1.0 / (MEASUREMENTsigma * MEASUREMENTsigma)]])
        super().__init__(idx, edges, ground_truth_measurement.z_noisy)

    def h(self):
        x_from = self.edges[0].variable_to_factor_message.mu
        x_to = self.edges[1].variable_to_factor_message.mu
        return x_to - x_from

    def J(self):
        # In general J could depend on variables but here it is constant because the
        # measurement function is linear
        return np.matrix([[-1.0, 0.0, 1.0, 0.0], [0.0, -1.0, 0.0, 1.0]])

    def setLambda(self, newMEASUREMENTsigma):
        self.Lambda = np.matrix(  [[1.0 / (newMEASUREMENTsigma * newMEASUREMENTsigma), 0.0],
                                   [0.0, 1.0 / (newMEASUREMENTsigma * newMEASUREMENTsigma)]]  )


class TwoDPoseFactor(Factor):
    """
    Simple factor for pose anchor measurement of a single point
    """

    def __init__(self, idx, edges, pose_observed, P_sigma):
        self.Lambda = np.matrix([[1.0 / (P_sigma * P_sigma), 0.0],
                                 [0.0, 1.0 / (P_sigma * P_sigma)]])
        super().__init__(idx, edges, pose_observed)

    def h(self):
        print("INIT", self.edges[0].variable_to_factor_message.mu)
        return self.edges[0].variable_to_factor_message.mu

    def J(self):
        return np.matrix([[1.0, 0.0], [0.0, 1.0]])


class VariableToFactorMessage:
    def __init__(self, dimension):
        self.eta = np.zeros((dimension, 1))
        self.Lambda = np.zeros((dimension, dimension))
        self.mu = np.zeros((dimension, 1))

        
class FactorToVariableMessage:
    def __init__(self, dimension):
        self.eta = np.zeros((dimension, 1))
        self.Lambda = np.zeros((dimension, dimension))


class Edge:
    """
    Every edge between a variable node and a factor node is via an edge
    so the variable and factor only need to know about the edge, not about
    each other's internals. An edge stores the recent message in each direction; 
    dimension is the state space size.
    """
    def __init__(self, dimension):
        # VariableToFactorMessage also passes the current state value (3rd arg)
        self.variable_to_factor_message = VariableToFactorMessage(dimension)
        self.factor_to_variable_message = FactorToVariableMessage(dimension)
        self.variable = -1
        self.factor = -1

        
class GroundTruthPoint:
    def __init__(self, index):
        self.index = index
        x = random.uniform (0.0, POINTSXRANGE)
        y = random.uniform (0.0, POINTSXRANGE)
        self.x = np.matrix([[x],[y]])


class GroundTruthMeasurement:
    def __init__(self, from_ground_truth_point, to_ground_truth_point):
        self.from_ground_truth_point = from_ground_truth_point
        self.to_ground_truth_point = to_ground_truth_point
        self.z_true = to_ground_truth_point.x - from_ground_truth_point.x
        self.z_noisy = self.z_true.copy()
        self.z_noisy[0,0] += random.uniform(-MEASUREMENTsigma, MEASUREMENTsigma)
        self.z_noisy[1,0] += random.uniform(-MEASUREMENTsigma, MEASUREMENTsigma)
    

# Just for instructions display
fflag = 1
cflag = 1
hlflag = 0


def updateDisplay(ground_truth_factors, ground_truth_points, variables):
    screen.fill(BLACK)

    for ground_truth_factor in ground_truth_factors:
        x_from = ground_truth_factor.from_ground_truth_point.x[0,0]
        y_from = ground_truth_factor.from_ground_truth_point.x[1,0]
        x_to = ground_truth_factor.to_ground_truth_point.x[0,0]
        y_to = ground_truth_factor.to_ground_truth_point.x[1,0]
        to_noisy = ground_truth_factor.from_ground_truth_point.x + ground_truth_factor.z_noisy
        x_to_noisy = to_noisy[0,0]
        y_to_noisy = to_noisy[1,0]
        
        pygame.draw.line(screen, GREY,
                         (int(u0 + kx * x_from), int(v0 - kx * y_from)),
                         (int(u0 + kx * x_to), int(v0 - kx * y_to)), 2)

    for ground_truth_point in ground_truth_points:
        pygame.draw.circle(screen, WHITE,
                           (int(u0 + kx * ground_truth_point.x[0,0]),
                            int(v0 - kx * ground_truth_point.x[1,0])), 6, 0)


    # Draw optimal batch solution
    for idx, variable in enumerate(variables):
        bx = big_mu[2*idx,0]
        by = big_mu[2*idx+1,0]
        sigma = math.sqrt(big_Sigma[2*idx,2*idx])
        pygame.draw.circle(screen, GREEN, (int(u0 + kx * bx), int(v0 - kx * by)), 4, 0)
        pygame.draw.circle(screen, GREEN, (int(u0 + kx * bx), int(v0 - kx * by)),
                           int(sigma * kx), 1)

    for variable in variables:
        variable.draw()

    if (cflag):
        ssshow = myfont.render("Click: variable sends messages", True, WHITE)
        screen.blit(ssshow, (WIDTH - 20 - ssshow.get_width(), 20))
    if (fflag):
        ssshow = myfont.render("f: start random schedule", True, WHITE)
        screen.blit(ssshow, (WIDTH - 20 - ssshow.get_width(), 60))
    if (hlflag):
        ssshow = myfont.render("h,l change: measurement sigma %02f" %MEASUREMENTsigma, True, WHITE)
        screen.blit(ssshow, (WIDTH - 20 - ssshow.get_width(), 100))

    pygame.display.flip()
    #time.sleep(0.2)

        
if __name__ == '__main__':
    pygame.init()
    
    screen = pygame.display.set_mode(SIZE)
    
    gt_points = []
    gt_factors = []
    variables = []
    factors = []
    pose_factors = []

    print("Initializing points...")
    for i in range(NUMBEROFPOINTS):
        gt_points.append(GroundTruthPoint(i))
        variables.append(Variable(i))
    
        pose_edges = []
        new_pose_edge = Edge(2)
        pose_edges.append(new_pose_edge)
        p_sigma = POSEsigma / 20.0 if i == 0 else POSEsigma
        new_pose_factor = TwoDPoseFactor(i, pose_edges, gt_points[i].x, p_sigma)
        pose_factors.append(new_pose_factor)
        variables[i].edges.append(new_pose_edge)
        new_pose_edge.variable = variables[i]
        new_pose_edge.factor = new_pose_factor
    print("Points initialized")

    print("Initializing factors...")
    for i in range(NUMBEROFFACTORS):
        from_idx = random.randint(0, NUMBEROFPOINTS - 1)
        while True:
            to_idx = random.randint(0, NUMBEROFPOINTS - 1)
            new_gt_measurement = GroundTruthMeasurement(gt_points[from_idx],
                                                        gt_points[to_idx])
            if (np.linalg.norm(new_gt_measurement.z_true) < MAXMEASDISTANCE and
                from_idx != to_idx):
                break
        gt_factors.append(new_gt_measurement)
        # Set up factor node
        new_edges = []
        from_edge = Edge(2)
        to_edge = Edge(2)
        new_edges.append(from_edge)
        new_edges.append(to_edge)
        new_factor = TwoDMeasFactor(i, new_edges, new_gt_measurement)
        factors.append(new_factor)
    
        variables[from_idx].edges.append(from_edge)
        variables[to_idx].edges.append(to_edge)
        from_edge.variable = variables[from_idx]
        from_edge.factor = new_factor
        to_edge.variable = variables[to_idx]
        to_edge.factor = new_factor
    print("Factors initialized")

    # These are necessary to initialize correctly
    for pose_factor in pose_factors:
        pose_factor.send_message(0)
    for variable in variables:
        variable.send_message(0)

        
    print ("Setup Done")
    
    
    big_Lambda = np.zeros((2 * NUMBEROFPOINTS, 2 * NUMBEROFPOINTS))
    big_eta = np.zeros((2 * NUMBEROFPOINTS, 1))
    
    for (i,pose_factor) in enumerate(pose_factors):
        big_eta[2*i:2*i+2,:] = pose_factor.eta_stored.copy()
        big_Lambda[2*i:2*i+2,2*i:2*i+2] = pose_factor.Lambda_prime_stored.copy()
    
    for idx, factor in enumerate(factors):
        from_idx = factor.edges[0].variable.idx
        to_idx = factor.edges[1].variable.idx
        eta_block = factor.eta_stored.copy()
        Lambda_block = factor.Lambda_prime_stored.copy()
        big_eta[2*from_idx:2*from_idx+2,:] += eta_block[0:2,:]
        big_eta[2*to_idx:2*to_idx+2,:] += eta_block[2:4,:]
        big_Lambda[2*from_idx:2*from_idx+2,2*from_idx:2*from_idx+2] += Lambda_block[0:2,0:2]
        big_Lambda[2*from_idx:2*from_idx+2,2*to_idx:2*to_idx+2] += Lambda_block[0:2,2:4]
        big_Lambda[2*to_idx:2*to_idx+2,2*from_idx:2*from_idx+2] += Lambda_block[2:4,0:2]
        big_Lambda[2*to_idx:2*to_idx+2,2*to_idx:2*to_idx+2] += Lambda_block[2:4,2:4]
        big_Sigma = np.matrix(big_Lambda).I.copy()
        big_mu = big_Sigma * big_eta
    
    updateDisplay(gt_factors, gt_points, variables)
    updateDisplay(gt_factors, gt_points, variables)

    
    count = 0
    flag = True
    while flag:
        events = pygame.event.get()
    
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                print ("Click at", mouse_x, mouse_y)
                x = float(mouse_x - u0)/kx
                y = float(-mouse_y + v0)/kx
                print ("Coordinates", x, y)
    
                thresh = 0.2
                for variable in variables:
                    dist = math.sqrt((variable.mu[0,0] - x)**2 +
                                     (variable.mu[1,0] - y)**2)
                    if (dist < thresh):
                        variable.updated = True
                        variable.send_all_messages()
                        # TODO This is all very inefficient with a lot of repeated work!
                        for edge in variable.edges:
                            edge.factor.send_all_messages()
                            for edge2 in edge.factor.edges:
                                edge2.variable.update_mu_sigma()
                    else:
                        variable.updated = False
    
    
            if event.type == KEYDOWN:
                if event.key == K_f:
                    flag = False
                if event.key == K_1:
                    for variable in variables:
                        variable.send_all_messages()
                    for factor in factors:
                        factor.send_all_messages()
                    for variable in variables:
                        variable.update_mu_sigma()
                    count += 1
                if event.key == K_s:
                    myfile = "2dmap" + str(count) + ".png"
                    pygame.image.save(screen, myfile)
                    print ("Saving image to", myfile)
    
        updateDisplay(gt_factors, gt_points, variables)
    
    
    cflag = 0
    fflag = 0
    hlflag = 1
        
    while True:
        events = pygame.event.get()
    
        for variable in variables:   
            variable.send_all_messages()
            # TODO this is all very inefficient with a lot of repeated work!
            for edge in variable.edges:
                edge.factor.send_all_messages()
                for edge2 in edge.factor.edges:
                    edge2.variable.update_mu_sigma()
    
        count += 1
        print ("All variables then factors have sent", count, "messages.")
        
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_h:
                    MEASUREMENTsigma += 0.1
                    for factor in factors:
                        factor.setLambda(MEASUREMENTsigma)
                        factor.recalculate_eta_lambda_prime()
                if event.key == K_l:
                    MEASUREMENTsigma -= 0.1
                    for factor in factors:
                        factor.set_lambda(MEASUREMENTsigma)
                        factor.recalculate_eta_lambda_prime()
                if event.key == K_s:
                    myfile = "screendump.png"
                    pygame.image.save(screen, myfile)
                    print ("Saving image to", myfile)
    
        updateDisplay(gt_factors, gt_points, variables)
