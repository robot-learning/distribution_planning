#!/usr/bin/env python
import sys
import argparse
import numpy as np
import numpy.matlib


DEBUG = True


class Node:

    def __init__(self, name):
        self.name = name
        self.neighbors = {}
        self.msgs = {}

    def send_message(self, name):
        raise NotImplementedError()

    def receive_message(self, msg, from_node):
        if from_node not in self.neighbors:
            raise ValueError(f"Received message from non-neighbor: {from_node}")
        self.msgs[from_node] = msg
        
    def is_valid_neighbor(self, node):
        raise NotImplementedError()
    
    def add_neighbor(self, node):
        if not self.is_valid_neighbor(node):
            raise ValueError(f"Invalid neighbor type for {self.name}: {type(node)}")
        self.neighbors[node.name] = node


class Variable(Node):

    def __init__(self, name, size):
        super().__init__(name)
        self.size = size
        self.eta = None
        self.Lambda = None

    def send_message(self, to_factor):
        if to_factor not in self.neighbors:
            raise ValueError(f"Factor '{to_factor}' is not a neighbor of variable {self.name}")
        # Exclude recipient factor from msg update
        from_factors = [k for k in self.neighbors.keys() if k != to_factor]        
        msg = Message(self.size)
        msg.eta, msg.Lambda = self._compute_eta_Lambda(from_factors)
        if DEBUG:
            msg.show()
        self.neighbors[to_factor].receive_message(msg, self.name)

    def is_valid_neighbor(self, node):
        return isinstance(node, Factor)

    def compute_eta_Lambda(self):
        # Take in messages from all neighbors, Gaussian information form is sum of params
        self.eta, self.Lambda = self._compute_eta_Lambda()
        return self.eta, self.Lambda

    def _compute_eta_Lambda(self, factors=[]):
        # TODO not sure if this is correct
        if not self.msgs:
            return np.matlib.zeros(self.size).T, np.matlib.zeros((self.size, self.size))

        if not factors:
            factors = self.neighbors.keys()
        # TODO I'm not sure if it's necessary to first receive messages from all neighors
        for f in factors:
            if f not in self.msgs:
                raise ValueError(f"No message received yet from factor '{f}'")

        # Gaussian product in information form is respective sum of eta and Lambda params
        # TODO there is np.matlib.sum but was getting frustrated making shapes correct
        eta = np.matrix(np.sum([self.msgs[f].eta for f in factors], axis=0))
        Lambda = np.matrix(np.sum([self.msgs[f].Lambda for f in factors], axis=0))
        return eta, Lambda

    def _compute_mu_Sigma(self):
        if self.eta is None or self.Lambda is None:
            self._compute_eta_Lambda()
        self.Sigma = self.Lambda.I
        self.mu = self.Sigma * self.eta
        return self.mu, self.Sigma
    

class Factor(Node):

    def __init__(self, name, size, sigma=0.1):
        # Size is state size for this factor
        self.size = size
        super().__init__(name)
        self.Lambda = np.matlib.eye(size) / sigma**2

    def send_message(self, to_var):
        if to_var not in self.neighbors:
            raise ValueError(f"Variable '{to_var}' is not a neighbor of factor {self.name}")
        
        # Let M = K_1 + ... + K_N for N incoming edges each with size K_i

        eta, Lambda_prime = self._compute_eta_Lambda_prime()  # eta (M, 1); Lambda_prime (M, M)

        # Condition on messages from variables (except the variable receiving msg)
        offset = 0
        out_start = 0
        size = self.neighbors[to_var].size
        for var_name, var in self.neighbors.items():
            if var_name == to_var:
                out_start = offset  # Remember this spot in the matrix for re-structuring
                out_end = out_start + size
            else:
                start = offset
                end = start + var.size
                # TODO make sure these were computed on var first
                eta[start:end, :] = var.eta
                Lambda_prime[start:end, start:end] = var.Lambda
            offset += var.size

        # Re-structure matrices to compute marginal over variable receiving msg.
        # See [1] for Eq numbers. Assumes params have structure:
        #
        #   eta = | e_a | (44)      Lambda = | L_aa L_ab | (45)
        #         | e_b |                    | L_ba L_bb |
        #
        # [1] Davison, Ortiz; FutureMapping 2: Gaussian belief propagation for spatial AI; 2019
        eta_top_temp = eta[:size, :].copy()
        eta[:size, :] = eta[out_start:out_end, :]
        eta[out_start:out_end, :] = eta_top_temp
        L_top_temp = Lambda_prime[:size, :].copy()
        Lambda_prime[:size, :] = Lambda_prime[out_start:out_end, :]
        Lambda_prime[out_start:out_end, :] = L_top_temp
        L_left_temp = Lambda_prime[:, :size].copy()
        Lambda_prime[:, :size] = Lambda_prime[:, out_start:out_end]
        Lambda_prime[:, out_start:out_end] = L_left_temp
        
        e_a = eta[:size, :]
        e_b = eta[size:, :]
        L_aa = Lambda_prime[:size, :size]
        L_bb = Lambda_prime[size:, size:]
        L_ab = Lambda_prime[:size, size:]
        L_ba = Lambda_prime[size:, :size]

        L_ab__L_bb_inv = L_ab * L_bb.I  # Just to avoid redundant computation, in (46) and (47)

        msg = Message(self.size)
        msg.eta = e_a - L_ab__L_bb_inv * e_b       # Eq. (46) in [1], marginal eta
        msg.Lambda = L_aa - L_ab__L_bb_inv * L_ba  # Eq. (47) in [1], marginal Lambda
        if DEBUG:
            msg.show()
        self.neighbors[to_var].receive_message(msg, self.name)

    def is_valid_neighbor(self, node):
        return isinstance(node, Variable)

    def _compute_eta_Lambda_prime(self):


        # TODO need to compute all of these and settle on what each should be
        x0 = self.x0() # point you're linearizing about, which is stacked variable means
        z = self.z()   # actual measured state value for this factor
        h = self.h(x0) # measurement function mapping combined vars -> factor state
        J = self.J(x0) # Jacobian of h function w.r.t combined vars
        
        J_T_Lambda = J.T * self.Lambda
        Lambda_prime = J_T_Lambda * J

        # print("J", J)
        # print("JTLAMBDA", J_T_Lambda)
        # print("x0", x0)
        # print("z", z)
        # print("h", h)
        
        eta = J_T_Lambda * (J * x0 + z - h)

        return eta, Lambda_prime

class VectorFactor(Factor):

    def __init__(self, name, obs, sigma=0.1):
        self.obs = obs
        super().__init__(name, len(obs), sigma)

    def x0(self):
        if len(self.msgs.values()) != 1:
            raise ValueError(f"Expected exactly one variable msg for VectorFactor "
                             f"but have {len(self.msgs.values())} msgs")
        return next(iter(self.msgs.values())).mu()

    def z(self):
        return self.obs

    def h(self, x0):
        return x0  # Nothing to do, just a pass-through

    def J(self, x0):
        return np.matlib.eye(self.size)  # Measurement function h is just a pass-through

        
class DynamicsFactor(Factor):

    def __init__(self, timestep, size, sigma=0.1):
        self.timestep = timestep
        super().__init__(f'x{timestep}_x{timestep+1}', size, sigma)

    def x0(self):
        x_t = self.msgs[f'x{self.timestep}'].mu()
        x_tp1 = self.msgs[f'x{self.timestep+1}'].mu()
        # print("XT", x_t)
        # print("XTP1", x_tp1)
        return np.concatenate([x_t, x_tp1], axis=0)

    def z(self):
        return self.msgs[f'x{self.timestep}'].mu()

    def h(self, x0):
        return x0[2:,:]  # take only the pose at the next timestep

    def J(self, x0):
        # TODO not sure if this is right
        return np.concatenate([np.matlib.eye(2), np.matlib.eye(2)], axis=-1)

    
class KinematicsFactor(Factor):

    def __init__(self):
        pass

    def x0(self):
        return np.concatenate([self.msgs[f'x{self.timestep}'].mu,
                               self.msgs[f'q{self.timestep}'].mu], axis=0)

    def z(self):
        return self.msgs[f'x{self.timestep}'].mu
    
    def h(self, x0):
        q0 = x0[2:,:]  # First two are 2d pose, only taking joints
        return np.asmatrix(self.arm.fk(q0).unsqueeze(-1).numpy())  # 2d pose (2, 1)

    def J(self, x0):
        """
        Jacobian is (2, 2 + N_q), where the first 2 dims use identity mapping because
        this is only actually computing arm Jacobian but needs to be function of all
        vars this factor depends on.
        """
        q0 = x0[2:,:]  # First two are 2d pose, only taking joints
        arm_J = self.arm.jacobian(q0)  # (2, N_q)
        return np.concatenate([np.matlib.eye(2), arm_J], axis=-1)

        
class FactorGraph:

    def __init__(self, variables={}, factors={}):
        self.variables = variables
        self.factors = factors

    def add_variable(self, node):
        if not isinstance(node, Variable):
            raise ValueError(f"Expected type Variable but got type {type(node)} "
                             "for adding variable to factor graph")
        self.variables[node.name] = node

    def add_variables(self, node_list):
        for node in node_list:
            self.add_variable(node)

    def add_factor(self, node):
        if not isinstance(node, Factor):
            raise ValueError(f"Expected type Factor but got type {type(node)} "
                             "for adding factor to factor graph")
        self.factors[node.name] = node

    def add_factors(self, node_list):
        for node in node_list:
            self.add_factor(node)
        
    def add_edge(self, node_1, node_2):
        if node_1 in self.variables:
            if node_2 in self.variables:
                raise ValueError(f"Cannot add edge between two variable nodes")
            elif node_2 in self.factors:
                self.variables[node_1].add_neighbor(self.factors[node_2])
                self.factors[node_2].add_neighbor(self.variables[node_1])
            else:
                raise ValueError(f"Node '{node_2}' not in factor graph")
        elif node_1 in self.factors:
            if node_2 in self.variables:
                self.variables[node_2].add_neighbor(self.factors[node_1])
                self.factors[node_1].add_neighbor(self.variables[node_2])
            elif node_2 in self.factors:
                raise ValueError(f"Cannot add edge between two factor nodes")
            else:
                raise ValueError(f"Node '{node_2}' not in factor graph")
        else:
            raise ValueError(f"Node '{node_1}' not in factor graph")

    def send_message(self, node_1, node_2):
        if DEBUG:
            print(f"  SENDING MSG from '{node_1}' to '{node_2}'")

        if node_1 in self.variables:
            self.variables[node_1].send_message(node_2)
        elif node_1 in self.factors:
            self.factors[node_1].send_message(node_2)
        else:
            raise ValueError(f"Node '{node_1}' is not a node in the factor graph")
    
class Message:

    def __init__(self, size):
        self.size = size
        self.eta = np.matlib.zeros(size).T
        self.Lambda = np.matlib.zeros((size, size))

    def mu(self):
        if not np.any(self.eta):
            mu = np.matlib.zeros(self.size).T
        else:
            mu = self.Lambda.I * self.eta
        return mu

    def show(self):
        s = f"    MSG:"
        s += " mu=["
        for r in self.mu().tolist():
            for e in r:
                s += f"{e:.1f}, "
        s = s[:-2] + "]\n"        
        s += "         eta=["
        for r in self.eta.tolist():
            for e in r:
                s += f"{e:.1f}, "
        s = s[:-2] + "]\n"
        s += "         Lambda=[["
        for r in self.Lambda.tolist():
            for e in r:
                s += f"{e:.1f}, "
            s = s[:-2] + "]\n                 ["
        s = s[:-1].rstrip() + "]"
        print(s)
                  


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    DEBUG = args.debug

    
    T = 8

    start_x = np.matrix([[2.0], [0.5]])
    goal_x = np.matrix([[-1.5], [0.5]])
    
    start_sigma = 0.01
    goal_sigma = 0.05
    dyn_sigma = 0.7

    x_vars = [Variable(f'x{t}', 2) for t in range(T)]
    
    x_dyn_factors = [DynamicsFactor(t, 2) for t in range(T-1)]
    x_init_factor = VectorFactor('x_init', start_x)
    x_goal_factor = VectorFactor('x_goal', goal_x)

    print("\nInitializing factor graph...")
    g = FactorGraph()
    g.add_variables(x_vars)
    g.add_factors(x_dyn_factors + [x_init_factor, x_goal_factor])
    
    # Connect initial pose distribution
    g.add_edge('x_init', 'x0')
    for t in range(T-1):
        # Connect pose dynamics factors to the corresponding pose vars
        g.add_edge(f'x{t}', f'x{t}_x{t+1}')
        g.add_edge(f'x{t+1}', f'x{t}_x{t+1}')
    # Connect goal pose distribution
    g.add_edge('x_goal', f'x{T-1}')
    print("Factor graph initialized")

    print("\nInitializing beliefs...")
    g.send_message('x0', 'x_init')
    g.send_message(f'x{T-1}', 'x_goal')
    g.send_message('x_init', 'x0')
    g.send_message('x_goal', f'x{T-1}')

    for t in range(T-1):
        g.send_message(f'x{t}', f'x{t}_x{t+1}')
        g.send_message(f'x{t+1}', f'x{t}_x{t+1}')
    print("Beliefs initialized")

    print("\nRunning message passing...")
    for t in range(T-1):
        g.send_message(f'x{t}', f'x{t}_x{t+1}')
        g.send_message(f'x{t}_x{t+1}', f'x{t+1}')
