import sys
import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches
from matplotlib.animation import FuncAnimation

from distribution_planning.distributions import Gaussian, get_cov

from torch.distributions import MultivariateNormal as MVN # TODO temp

from ll4ma_util import file_util


DEFAULT_FC = 'slategrey'


class MiniGolfEnvironment:

    def __init__(self, config):
        self.config = config
        self.obstacles = {}
        self.terrains = {}
        self.artists = {}

        self.terrain_funcs = {}
        self.torch_terrain_funcs = {}

        if 'obstacles' in self.config:
            for name, cfg in self.config['obstacles'].items():
                if cfg['type'] == 'rectangle':
                    xl, yl, xh, yh = cfg['bounds']
                    xy = (xl, yl)
                    width = xh - xl
                    height = yh - yl
                    fc = cfg['fc'] if 'fc' in cfg else DEFAULT_FC
                    self.artists[name] = patches.Rectangle(xy, width, height, fc=fc)
        if 'terrains' in self.config:
            for name, cfg in self.config['terrains'].items():
                self.terrains[name] = cfg
                self.terrain_funcs[name] = self._terrain_factory(cfg)
                self.torch_terrain_funcs[name] = self._torch_terrain_factory(cfg)

        self.time = 0
        self.states = [np.array(self.config['start_point'])]

    def current_state(self):
        return self.states[-1].copy()

    def state_size(self):
        return 4

    def batch_dynamics(self, x, use_noise=True, min_v=1e-5):
        # TODO simplified model for now that assumes planar surface, so that normal force
        # is just going to be equal and opposite the force of gravity. The only important
        # force then is the frictional force which will act in the opposite direction of
        # velocity to slow the ball down
        dt = self.config['dt']
        p = x[:,:2]
        v = x[:,2:]
        
        g = -9.8  # m/s^2
        m = 0.045 # kg, actual mass of golf ball
        N = m*g   # assumption that surface is planar and normal force equals gravitational force
        mu = 0.004 # friction coefficient
        norm_v = torch.linalg.norm(v, dim=-1, keepdim=True)

        F = -mu * np.linalg.norm(N) * v / norm_v
        a = F / m
        if use_noise:
            # TODO can improve this to have more options on type of noise
            covs = self.dynamics_noise(p)[:,:2,:2]
            noise = MVN(torch.zeros(len(a), 2).double(), covs).sample()
            a += torch.randn_like(v) * noise

        next_v = torch.zeros_like(v)
        idxs = (norm_v >= min_v).squeeze()
        next_v[idxs,:] = v[idxs,:] + a[idxs,:] * dt

        # next_v = v + a * dt

        next_p = p + next_v * dt

        if 'obstacles' in self.config:
            for name, cfg in self.config['obstacles'].items():
                xl, yl, xh, yh = cfg['bounds']
                L_pen = (p[:,0] < xl) * (next_p[:,0] >= xl) * \
                        (next_p[:,1] >= yl) * (next_p[:,1] <= yh)
                R_pen = (p[:,0] > xh) * (next_p[:,0] <= xh) * \
                        (next_p[:,1] >= yl) * (next_p[:,1] <= yh)
                B_pen = (p[:,1] < yl) * (next_p[:,1] >= yl) * \
                        (next_p[:,0] >= xl) * (next_p[:,0] <= xh)
                T_pen = (p[:,1] > yh) * (next_p[:,1] <= yh) * \
                        (next_p[:,0] >= xl) * (next_p[:,0] <= xh)
            next_p[L_pen, 0] = xl
            next_v[L_pen, 0] *= -1.            
            next_p[R_pen, 0] = xh
            next_v[R_pen, 0] *= -1.
            next_p[B_pen, 1] = yl
            next_v[B_pen, 1] *= -1.
            next_p[T_pen, 1] = yh
            next_v[T_pen, 1] *= -1.

        return torch.cat([next_p, next_v], dim=-1)

    def dynamics(self, z):
        # TODO trying this for grad solver, can reconcile with batch one later
        p = z[:2]
        v = z[2:]
        dt = self.config['dt']
        
        g = -9.8  # m/s^2
        m = 0.045 # kg, actual mass of golf ball
        N = m*g   # assumption that surface is planar and normal force equals gravitational force
        mu = 0.004 # friction coefficient

        # if sum(abs(v)) < 1e-7:
        #     F = np.zeros(len(v), dtype=v.dtype)
        # else:
        #     F = -mu * np.linalg.norm(N) * v / np.linalg.norm(v)
        
        F = -mu * np.linalg.norm(N) * v / np.linalg.norm(v)
        a = F / m
        next_v = v + a * dt
        next_p = p + next_v * dt

        # for name, cfg in self.config['obstacles'].items():
        #     xl, yl, xh, yh = cfg['bounds']

        #     L_pen = (p[0] < xl) * (next_p[0] >= xl) * (next_p[1] >= yl) * (next_p[1] <= yh)
        #     R_pen = (p[0] > xh) * (next_p[0] <= xh) * (next_p[1] >= yl) * (next_p[1] <= yh)
        #     B_pen = (p[1] < yl) * (next_p[1] >= yl) * (next_p[0] >= xl) * (next_p[0] <= xh)
        #     T_pen = (p[1] > yh) * (next_p[1] <= yh) * (next_p[0] >= xl) * (next_p[0] <= xh)

        #     if L_pen:
        #         next_p[0] = xl
        #         next_v[0] *= -1.
        #     if R_pen:
        #         next_p[0] = xh
        #         next_v[0] *= -1.
        #     if B_pen:
        #         next_p[1] = yl
        #         next_v[1] *= -1.
        #     if T_pen:
        #         next_p[1] = yh
        #         next_v[1] *= -1.
        
        return np.concatenate([next_p, next_v])

    def dynamics_noise(self, state=None, cast_type=False):
        N = 4
        # Get the base dynamics noise specified in config
        R = get_cov(self.config['dynamics_noise'], N)
        if isinstance(state, torch.Tensor):
            R = torch.tensor(R).unsqueeze(0).repeat(len(state), 1, 1)
        if cast_type:
            R = R.astype(state.dtype)
        # Add in additional noise from the terrain functions
        for name, cfg in self.terrains.items():
            # TODO can probably handle tensor logic in the terrain func
            cov = get_cov(cfg['noise'], N)
            if isinstance(state, torch.Tensor):
                # TODO get rid of :2
                coef = self.torch_terrain_funcs[name](state[:,:2]).view(-1, 1, 1)
                cov = torch.tensor(cov).unsqueeze(0).repeat(len(coef), 1, 1)
            else:
                coef = self.terrain_funcs[name](state[:2])
            if cast_type:
                cov = cov.astype(state.dtype)
            R += coef * cov
        return R

    def rollout(self, x, horizon=100, use_noise=True):
        # TODO should add clipping so it obeys the system constraints
        xs = [x]
        for i in range(horizon):
            xs.append(self.batch_dynamics(xs[-1], use_noise))
        xs = torch.stack(xs).transpose(0, 1)  # Make (B, T, D)
        return xs

    def simulate(self):
        fig, ax = self.visualize()

        # TODO testing
        n_samples = 20
        ps = [torch.tensor([5.0, 5.0]).unsqueeze(0).repeat(n_samples, 1)]
        vs = [torch.tensor([1.0, 0.3]).unsqueeze(0).repeat(n_samples, 1)]
        rate = 10.
        dt = 1. / rate
        n_steps = 1000

        for i in range(n_steps):
            p, v = self.batch_dynamics(ps[-1], vs[-1], dt)
            ps.append(p)
            vs.append(v)

        obj = ax.scatter([], [], s=100)
        
        def animate(i):
            # ax.set_title(f"i={i}")
            obj.set_offsets(ps[i])
            return [obj]

        anim = FuncAnimation(fig, animate, frames=n_steps, interval=rate, blit=True)
        plt.show()

    def draw_scene(self, show=False, min_xy=[0,0], max_xy=[6,11], size=(10, 10)):
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(*size)

        if 'start_region' in self.config:
            ax.add_artist(self._get_rectangle_patch(self.config['start_region']))
        min_xy = self.config['plt_min'] if 'plt_min' in self.config else min_xy
        max_xy = self.config['plt_max'] if 'plt_max' in self.config else max_xy
        
        if 'obstacles' in self.config:
            self.draw_obstacles(ax)
        if 'terrains' in self.config:
            self.draw_terrain(ax, min_xy, max_xy)

        ax.set_xlim(min_xy[0], max_xy[0])
        ax.set_ylim(min_xy[1], max_xy[1])
        plt.tick_params(axis='y', which='both', left=False, labelleft=False)
        ax.tick_params(axis='x', which='both', labelsize=18)
        plt.tight_layout()
        if show:
            plt.show()
        return fig, ax

    def draw_obstacles(self, ax):
        for name, cfg in self.config['obstacles'].items():
            if cfg['type'] == 'rectangle':
                ax.add_artist(self._get_rectangle_patch(cfg))
            else:
                raise ValueError(f"Unknown obatcle type: {cfg['type']}")
            
    def draw_terrain(self, ax, min_xy, max_xy, res=100):
        min_x, min_y = min_xy
        max_x, max_y = max_xy
        x = np.linspace(min_x, max_x, res)
        y = np.linspace(min_y, max_y, res)
        xx, yy = np.meshgrid(x,y)
        img = np.zeros((res,res))
        for name, func in self.terrain_funcs.items():
            for i in range(res):
                for j in range(res):
                    img[i,j] += func(np.array([x[i], y[j]]))
        ax.imshow(img.T, interpolation='bilinear', origin='lower',
                  cmap=self.config['terrain_cmap'],
                  extent=[min_x, max_x, min_y, max_y])        
    
    def _terrain_factory(self, cfg):
        if cfg['type'] == 'gaussian':
            mvn = Gaussian(mean=np.array(cfg['coef_mean']), cov=get_cov(cfg['coef_cov'], 2))
            return lambda x: mvn.pdf(x)
        else:
            raise ValueError(f"Unsupported type for terrain: {cfg['type']}")

    def _torch_terrain_factory(self, cfg):
        if cfg['type'] == 'gaussian':
            mvn = MVN(torch.tensor(cfg['coef_mean']), torch.tensor(get_cov(cfg['coef_cov'], 2))) 
            return lambda x: torch.exp(mvn.log_prob(x))
        else:
            raise ValueError(f"Unsupported type for torch terrain: {cfg['type']}")
        
    def _get_rectangle_patch(self, cfg):
        xl, yl, xh, yh = cfg['bounds']
        xy = (xl, yl)
        width = xh - xl
        height = yh - yl
        fc = cfg['fc'] if 'fc' in cfg else DEFAULT_FC
        ec = cfg['ec'] if 'ec' in cfg else None
        hatch = cfg['hatch'] if 'hatch' in cfg else None
        lw = cfg['lw'] if 'lw' in cfg else 1
        artist = patches.Rectangle(xy, width, height, fc=fc, ec=ec, hatch=hatch, lw=lw)
        return artist


if __name__ == '__main__':
    import argparse
    import os.path as osp
    import distribution_planning

    parser = argparse.ArgumentParser()
    parser.add_argument('--config_dir', type=str,
                        default=file_util.get_path(distribution_planning, "config"))
    parser.add_argument('--config', type=str, default='mini_golf.yaml')
    args = parser.parse_args()
    
    config_fn = osp.join(args.config_dir, args.config)
    config = file_util.load_yaml(config_fn)
    env = MiniGolfEnvironment(config)
    
    fig, ax = env.draw_scene()

    n_samples = 100
    n_steps = 100
    vel_mag = 0.5
    vel_mag_increment = 0.05
    min_vel_mag = 0.
    max_vel_mag = 1.
    dt = 0.2
    noise = 0.03
    
    
    # TODO want to update this to show the uncertainty propagations (MC and UT),
    # can show propagated terminal Gaussians and see how they compare
    

    
    p = torch.tensor([5., 1.0])
    direct = torch.tensor([1., 0.])

    actual_obj = ax.scatter([], [], s=100, color='red')
    objs = ax.scatter([], [], s=100, color='indigo', alpha=0.4)
    paths = [ax.plot([], [], lw=1, alpha=0.2, color='indigo')[0] for _ in range(n_samples)]
    
    ellipses = None
    gauss = Gaussian()
    
    def update():
        v = vel_mag * direct
        actual_obj.set_offsets(p)

        x = torch.cat([p, v]).unsqueeze(0)
        xs = env.rollout(x.repeat(n_samples, 1), n_steps)

        # Draw terminal samples and associated Gaussian distribution
        pts_T = xs[:,-1,:2].numpy()
        objs.set_offsets(pts_T)
        gauss.fit(pts_T)
        if gauss.artists:
            gauss.draw_update(ax)
        else:
            gauss.draw(ax, cmap='Purples')

        for i in range(n_samples):
            paths[i].set_data(xs[i,:,:2].T)
        fig.canvas.draw_idle()
    
    def on_button_press(event):
        global direct, p
        if event.xdata is not None and event.ydata is not None:
            end_pt = torch.tensor([event.xdata, event.ydata])
            # TODO this is kind of a hacked interaction, if you click below the obj
            # in y-axis it sets the x coordinate of the obj to where you clicked,
            # above in y-axis sets the target direction
            if end_pt[1] <= p[1]:
                p[0] = end_pt[0]
            else:
                diff = end_pt - p
                direct = diff / torch.linalg.norm(diff)
            update()
        
    def on_scroll(event):
        global vel_mag
        if event.button == 'up':
            vel_mag += vel_mag_increment
        elif event.button == 'down':
            vel_mag -= vel_mag_increment
        vel_mag = max(min(vel_mag, max_vel_mag), min_vel_mag)
        update()

    plt.connect('button_press_event', on_button_press)
    plt.connect('scroll_event', on_scroll)
    
    plt.show()
