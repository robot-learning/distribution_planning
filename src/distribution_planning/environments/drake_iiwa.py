import os
import os.path as osp
import sys
import time
import numpy as np

from pydrake.common import FindResourceOrThrow
from pydrake.common.eigen_geometry import Quaternion as DrakeQuaternion
from pydrake.math import RigidTransform, RotationMatrix, RollPitchYaw
from pydrake.autodiffutils import AutoDiffXd, ExtractValue
from pydrake.geometry import (
    Box, Cylinder, Sphere, MeshcatVisualizerCpp, Meshcat, MeshcatVisualizerParams
)
from pydrake.geometry.render import (
    MakeRenderEngineVtk, RenderEngineVtkParams, RenderCameraCore,
    ClippingRange, DepthRenderCamera, DepthRange
)
from pydrake.perception import DepthImageToPointCloud, BaseField
from pydrake.multibody.plant import AddMultibodyPlantSceneGraph
from pydrake.multibody.parsing import Parser
from pydrake.multibody.inverse_kinematics import InverseKinematics
from pydrake.systems.framework import DiagramBuilder
from pydrake.systems.analysis import Simulator
from pydrake.systems.sensors import CameraInfo, RgbdSensor
import pydrake.solvers.mathematicalprogram as mp

from manipulation.scenarios import AddIiwa, AddShape, AddMultibodyTriad
from manipulation.utils import FindResource

import distribution_planning
from distribution_planning.util import drake_util
from distribution_planning.environments.drake_object import DrakeObject
from ll4ma_util import file_util, func_util, vis_util

CONFIG_ROOT = osp.join(osp.dirname(osp.dirname(osp.abspath(__file__))), "config")
DEFAULT_CONFIG = osp.join(CONFIG_ROOT, "drake_iiwa.yaml")


# TODO currently this only runs through Jupyter notebooks, I haven't been able to
# figure out a good way to run it otherwise (e.g. through a script). I think it
# makes communicating with the running sim challenging so for now just stick
# with the notebook

class DrakeSim:
    
    def __init__(self, config=DEFAULT_CONFIG, collisions=False, headless=False,
                 no_cache=False, obj_data=None, add_camera=True,
                 meshcat_pos=None, meshcat_zoom=None):
        if isinstance(config, str):
            self.config = file_util.load_yaml(config)
        else:
            self.config = config
        self.headless = headless
        self.no_cache = no_cache
        self.obj_data = obj_data

        self.meshcat = None
        if not headless:
            self.meshcat = Meshcat()
            for key, cfg in self.config['meshcat'].items():
                for prop, value in cfg.items():
                    if 'color' in prop:
                        value = vis_util.get_color(value)
                    self.meshcat.SetProperty(key, prop, value)
            # Override meshcat view if provided
            if meshcat_pos is not None:
                self.meshcat.SetProperty('/Cameras/default/rotated/<object>',
                                         'position', meshcat_pos)
            if meshcat_zoom is not None:
                self.meshcat.SetProperty('/Cameras/default/rotated/<object>',
                                         'zoom', meshcat_zoom)
            print(f"Meshcat URL: {self.meshcat.web_url()}")
            
        self.builder = DiagramBuilder()
        self.n_joints = 7
        self.ee_frame = 'palm_link'
        
        self._add_robot(collisions)
        self._add_objects()
        if add_camera:
            self._add_camera()
        self._finalize()
        
        if 'start_thetas' in self.config:
            self.set_joint_positions(np.array(self.config['start_thetas']))

        self._set_plant_ad()
            
    def clear_sim(self):
        if not self.headless:
            self.meshcat.Delete()
        
    def fk(self, q, frame=None, ref_frame=None):
        tf = self.fk_ad(q, frame, ref_frame)
        return RigidTransform(R=RotationMatrix(ExtractValue(tf.rotation().matrix())),
                              p=ExtractValue(tf.translation()))
        
    def fk_ad(self, q, frame=None, ref_frame=None):
        if frame is None:
            frame = self.ee_frame
        if ref_frame is None:
            ref_frame = self.plant_ad.world_frame()
        # TODO doing this for allegro, can make nicer
        q = np.concatenate([q, np.zeros(16)])
        self.plant_ad.SetPositions(self.plant_ad_context, q)
        tf = self.plant_ad.CalcRelativeTransform(self.plant_ad_context, ref_frame,
                                                 self.plant_ad.GetFrameByName(frame))
        return tf

    def ik(self, tf, theta_bound=0., n_attempts=10):
        # Setting theta_bound higher will give more relaxed solutions
        ik = InverseKinematics(self.plant, self.plant_context)
        ee_frame = self.plant.GetFrameByName(self.ee_frame)
        world_frame = self.plant.world_frame()
        ik.AddPositionConstraint(ee_frame, [0, 0, 0], world_frame, 
                                 tf.translation(), tf.translation())
        ik.AddOrientationConstraint(ee_frame, RotationMatrix(), world_frame, 
                                    tf.rotation(), theta_bound)
        prog = ik.get_mutable_prog()
        q = ik.q()
        for _ in range(n_attempts):
            result = mp.Solve(ik.prog())
            if result.is_success():
                return result.GetSolution()
            else:
                # Do a random re-seed of the initial guess
                q0 = self._random_joints()
                prog.SetInitialGuess(q, q0)
        # print(f"IK failure after {n_attempts} re-seed attempts")
        return None

    def pose_reachable(self, pos, quat, theta_bound=0., n_attempts=10):
        # TODO can make more general, but currently assumes pos/quat are input just as list-like
        tf = RigidTransform(quaternion=DrakeQuaternion(quat), p=pos)
        ik_soln = self.ik(tf, theta_bound, n_attempts)
        return ik_soln is not None
        
    def test_joints(self):
        thetas = []
        for j in np.linspace(0, 1.57, 1000):
            thetas.append(np.full(7, j))
        for j in np.linspace(1.57, 0, 1000):
            thetas.append(np.full(7, j))
        self.execute_kinematic_trajectory(np.array(thetas))
            
    def execute_kinematic_trajectory(self, thetas, dt=0.001):
        # thetas (T, 7)
        for t in range(len(thetas)):
            start = time.time()
            self.set_joint_positions(thetas[t,:])
            diff = dt - (time.time() - start)
            if diff > 0:
                time.sleep(diff)

    def get_trajectory(self, init_thetas, d_thetas):
        """
        Computes joint and EE trajectory given start theta and sequence of d_thetas
        """
        thetas = [init_thetas]
        tfs = [self.fk(init_thetas)]
        for t in range(len(d_thetas)):
            new_thetas = thetas[-1] + d_thetas[t]
            thetas.append(new_thetas)
            tfs.append(self.fk(new_thetas))
        return thetas, tfs
        
    def set_joint_positions(self, thetas):
        # if len(thetas) != self.plant.num_positions():
        #     raise ValueError(f"Expected size {self.plant.num_positions()} for setting "
        #                      f"joint positions but got size {len(thetas)}")
        
        # TODO can make this nicer, doing this since allegro is attached and
        # considered part of the joint space even though I'm not using fingers
        thetas = np.concatenate([thetas, np.zeros(16)])
        self.plant.SetPositions(self.plant_context, thetas)
        self.diagram.Publish(self.context)

    def get_rgb_image(self):
        self.diagram.Publish(self.context)
        return self.diagram.GetOutputPort("color_image").Eval(self.context)

    def delete(self):
        if not self.headless:
            self.meshcat.Delete()
        
    def _finalize(self):
        self.plant.Finalize()
        if not self.headless:
            self.visualizer = MeshcatVisualizerCpp.AddToBuilder(self.builder, self.scene_graph,
                                                                self.meshcat)
        self.diagram = self.builder.Build()
        self.simulator = Simulator(self.diagram)
        self.context = self.diagram.CreateDefaultContext()
        self.plant_context = self.plant.GetMyContextFromRoot(self.context)
        self.diagram.Publish(self.context)

    def _add_robot(self, collisions):
        self.plant, self.scene_graph = AddMultibodyPlantSceneGraph(self.builder, time_step=1e-4)
        self.parser = Parser(self.plant, self.scene_graph)        
        robot_cfg = self.config['robot']
        robot_sdf = FindResourceOrThrow(robot_cfg['path'])
        self.parser.AddModelFromFile(robot_sdf)
        robot_base = self.plant.GetFrameByName(robot_cfg['base_link'])
        self.plant.WeldFrames(self.plant.world_frame(), robot_base)
        if collisions:
            # TODO can improve this to more accurately represent arm
            # collisions (more and smaller spheres)
            for i in range(1,7):
                sphere_i = AddShape(self.plant, Sphere(0.12), f"sphere_{i}")
                parent_frame = self.plant.GetFrameByName(f"iiwa_link_{i}")
                child_frame = self.plant.GetFrameByName(f"sphere_{i}", sphere_i)
                tf = RigidTransform([0., 0., 0.])
                self.plant.WeldFrames(parent_frame, child_frame, tf)

        ee_cfg = self.config['end_effector']
        ee_sdf = FindResourceOrThrow(ee_cfg['path'])
        robot_ee_tf = drake_util.get_tf(np.array(ee_cfg['position']))
        self.parser.AddModelFromFile(ee_sdf)
        self.plant.WeldFrames(self.plant.GetFrameByName('iiwa_link_7'),
                              self.plant.GetFrameByName(ee_cfg['base_link']),
                              robot_ee_tf)

        # There wasn't a good frame so I'm adding one to serve as palm link,
        # adding a sphere only because I don't know how to manually add a frame
        AddShape(self.plant, Sphere(0.001), 'palm_link')
        palm_frame = self.plant.GetFrameByName('palm_link')
        palm_tf = drake_util.get_tf(np.array([0.01, 0., 0.1]),
                                    rotation=np.array([[ 0,0,1],
                                                       [ 0,1,0],
                                                       [-1,0,0]]))
        self.plant.WeldFrames(self.plant.GetFrameByName(ee_cfg['base_link']),
                              palm_frame, palm_tf)
        drake_util.draw_frame(palm_frame, self.plant, self.scene_graph,
                              length=0.1, radius=0.005)

    def _add_camera(self):
        # TODO can move this info into config
        renderer = "camera"
        self.scene_graph.AddRenderer(renderer, MakeRenderEngineVtk(RenderEngineVtkParams()))
        rot = RollPitchYaw(0., -0.8, 0.).ToRotationMatrix()
        rot = rot.multiply(RollPitchYaw(-np.pi/2.0, 0, np.pi/2.0).ToRotationMatrix())
        X_Camera = RigidTransform(rot, [1.4, 0., 1.0])
        camera_instance = self.parser.AddModelFromFile(FindResource("models/camera_box.sdf"))
        camera = self.plant.GetBodyByName("base", camera_instance)    
        self.plant.WeldFrames(self.plant.world_frame(), camera.body_frame(), X_Camera)
        AddMultibodyTriad(camera.body_frame(), self.scene_graph, length=.1, radius=0.005)

        core = RenderCameraCore(renderer,
                                CameraInfo(width=1024, height=768, fov_y=np.pi / 4.0),
                                ClippingRange(near=0.1, far=10.0), RigidTransform())
        depth_camera = DepthRenderCamera(core, DepthRange(0.1, 10.0))
        rgbd_sensor = RgbdSensor(parent_id=self.plant.GetBodyFrameIdOrThrow(camera.index()),
                                 X_PB=RigidTransform(), depth_camera=depth_camera,
                                 show_window=False)
        camera = self.builder.AddSystem(rgbd_sensor)
        camera.set_name("rgbd_sensor")
        self.builder.Connect(self.scene_graph.get_query_output_port(),
                             camera.query_object_input_port())

        # Export the camera outputs
        self.builder.ExportOutput(camera.color_image_output_port(), "color_image")
        self.builder.ExportOutput(camera.depth_image_32F_output_port(), "depth_image")

        # Add a system to convert the camera output into a point cloud
        to_point_cloud = self.builder.AddSystem(
            DepthImageToPointCloud(camera_info=camera.depth_camera_info(),
                                   fields=BaseField.kXYZs | BaseField.kRGBs))
        self.builder.Connect(camera.depth_image_32F_output_port(),
                             to_point_cloud.depth_image_input_port())
        self.builder.Connect(camera.color_image_output_port(),
                             to_point_cloud.color_image_input_port())

        # Export the point cloud output.
        self.builder.ExportOutput(to_point_cloud.point_cloud_output_port(), "point_cloud")
        
    def _add_objects(self):
        self.objects = {}
        if 'objects' in self.config:
            world_frame = self.plant.world_frame()
            self.objects = {}
            for obj_name, cfg in self.config['objects'].items():
                link_name = f"base_link_{obj_name}"
                if cfg['type'] == 'object':
                    if self.obj_data and obj_name in self.obj_data:
                        obj_data = self.obj_data[obj_name]
                    else:
                        obj_data = None
                    obj = DrakeObject(cfg, obj_name, self.no_cache, obj_data)
                    obj.add_to_parser(self.parser, self.plant)
                    if cfg['draw_components']['active']:
                        obj.draw_components(self.plant, self.scene_graph)
                    if cfg['draw_reachable']['active']:
                        obj.draw_reachable(self.plant)
                    self.objects[obj_name] = obj
                elif cfg['type'] in ['cylinder', 'box']:
                    if cfg['type'] == 'cylinder':
                        shape = Cylinder(cfg['radius'], cfg['length'])    
                    elif cfg['type'] == 'box':
                        shape = Box(cfg['x_extent'], cfg['y_extent'], cfg['z_extent'])
                    color = self._get_color(cfg['color'])
                    AddShape(self.plant, shape, link_name, color=color)
                    tf = self._get_tf(cfg)
                    obj_frame = self.plant.GetFrameByName(link_name)                    
                    self.plant.WeldFrames(world_frame, obj_frame, tf)
                else:
                    raise ValueError(f"Unkown collision shape type: {cfg['type']}")
                
    def _set_plant_ad(self):
        self.plant_ad = self.plant.ToScalarType[AutoDiffXd]()
        self.plant_ad_context = self.plant_ad.CreateDefaultContext()

    def _random_joints(self):
        bounds = zip(self.config['min_thetas'], self.config['max_thetas'])
        joints = np.array([np.random.uniform(*b) for b in bounds])
        joints = np.concatenate([joints, np.zeros(16)])  # Adding allegro
        return joints
        
    def _get_color(self, color):
        # TODO can add more options for getting colors, probably
        # just use a utility function from ll4ma_util
        r, g, b, a = color
        return [r/255., g/255., b/255., a]
    
    def _get_tf(self, cfg):
        # TODO can support other rotations like quaternion, euler, etc.
        rotation = cfg['rotation'] if 'rotation' in cfg else None
        return drake_util.get_tf(cfg['position'], rotation)
