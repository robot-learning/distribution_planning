import sys
import torch
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.animation import FuncAnimation
from copy import deepcopy

from distribution_planning.distributions import Gaussian, DiracDelta, get_cov
from distribution_planning.util.env_util import (
    di_dynamics, batch_di_dynamics, dubins_dynamics, batch_dubins_dynamics
)

from ll4ma_util import file_util

# TODO for batch distributions, can integrate better with Gaussian from this pkg
from torch.distributions import MultivariateNormal as MVN


class PlanarNavigationEnvironment:

    def __init__(self, config=None):
        if isinstance(config, str):
            self.config = file_util.load_yaml(config)
        elif isinstance(config, dict):
            self.config = config
        else:
            raise ValueError(f"Unknown type for config: {type(config)}")

        self.obstacles = {}
        self.patches = {}
        self.terrains = {}
        
        self.sdfs = {}
        self.torch_sdfs = {}
        self.terrain_funcs = {}
        
        self.time = 0
        self.states = [np.array(self.config['start_point'])]
        if 'goal_point' in self.config:
            self.goal_states = [np.array(self.config['goal_point'])]  # For tracking dynamic goal
        else:
            self.goal_states = []
        self.acts = []
                
        if 'obstacles' in self.config:
            for name, cfg in self.config['obstacles'].items():
                self.obstacles[name] = cfg
                self.sdfs[name] = self._sdf_factory(cfg)
                self.torch_sdfs[name] = self._torch_sdf_factory(cfg)
        if 'patches' in self.config:
            for name, cfg in self.config['patches'].items():
                self.patches[name] = cfg
        if 'terrains' in self.config:
            for name, cfg in self.config['terrains'].items():
                self.terrains[name] = cfg
                self.terrain_funcs[name] = self._terrain_factory(cfg)

    def current_state(self):
        return self.states[-1].copy()

    def current_goal(self):
        return self.goal_states[-1].copy()
            
    def apply_action(self, u):
        self.acts.append(u.copy())
        state = self.dynamics(self.current_state(), u, noise=self.dynamics_noise())
        state = state.clip(self.config['min_x'], self.config['max_x'])
        self.states.append(state)
        self.time += 1

    def step_goal(self, u=None):        
        # TODO just making up a policy for the goal movement that will make it go side
        # to side along the environment
        next_goal, self.goal_u = self.goal_dynamics(self.goal_states[-1], u)
        self.goal_states.append(next_goal)

    def observe(self):
        if self.config['observation'] == 'linear':
            current_state = self.current_state()
            Q = self.observation_noise(current_state)
            C = self.linear_observation_matrix()
            mvn = Gaussian(np.zeros(self.state_size()), Q)
            noise = mvn.sample()
            z = C.dot(current_state) + noise
        else:
            raise ValueError(f"Unknown observation type: {self.config['observation']}")
        return z

    def linear_observation_matrix(self, state=None):
        # TODO currently this is over-simplified to just be identity, can add options
        return np.eye(self.state_size())

    def goal_dynamics(self, x, u=None):
        # TODO setting these configurations for what I need right now,
        # can make this more configurable later on
        if u is None:
            goal_u = np.zeros(2)
            px, py, vx, vy = x
            if px < 1.2 and vx <= 0.8:
                goal_u[0] = 0.3
            # elif px > 8.5 and vx >= -0.8:
            #     goal_u[0] = -0.3
        else:
            goal_u = u

        next_x = di_dynamics(x, goal_u, self.config['dt'])
        return next_x, goal_u  # Returning act since it might have been computed here

    def get_goal_trajectory(self, x0=None, n_steps=10):
        """
        Returns the sequence of goal states and actions that would result if the goal
        were to follow its default motion policy from x0.
        """
        xs = []
        us = []
        x = x0 if x0 is not None else self.current_goal()
        for _ in range(n_steps):
            next_x, next_u = self.goal_dynamics(x)
            xs.append(next_x)
            us.append(next_u)
            x = next_x
        return xs, us
        
    def dynamics(self, x, u, dt=None, noise=None):
        dt = dt if dt is not None else self.config['dt']
        if self.config['dynamics'] in ['di', 'double_integrator']:
            if isinstance(x, torch.Tensor):
                next_x = batch_di_dynamics(x, u, dt)
            else:
                next_x = di_dynamics(x, u, dt)
        elif self.config['dynamics'] == 'dubins':
            if isinstance(x, torch.Tensor):
                next_x = batch_dubins_dynamics(x, u, dt)
            else:
                next_x = dubins_dynamics(x, u, dt)
        else:
            raise ValueError(f"Unknown dynamics type: {self.config['dynamics']}")

        if noise is not None:
            # TODO for now just doing additive noise, not sure how multiplicative will work
            # in the filters and UT dynamics.
            if isinstance(x, torch.Tensor):
                mvn = MVN(torch.zeros_like(x), noise)
                sampled_noise = mvn.sample()
                next_x += torch.randn_like(next_x) * sampled_noise
            else:
                # TODO need to address this diagonal assumption similar to above
                next_x += np.random.randn(len(next_x)) * np.diag(noise)
        return next_x
        
    def dynamics_noise(self, state=None, cast_type=False):
        # TODO can make dependent on other aspects of state than pos
        if isinstance(state, torch.Tensor):
            # TODO this is a simple but silly way to batch things, but need to
            # make the 'in_patch' function more intelligent to handle batches
            return torch.tensor(np.array([self._dynamics_noise(s, cast_type) for s in state]))
        else:
            return self._dynamics_noise(state, cast_type)
        
    def _dynamics_noise(self, state=None, cast_type=False):
        # Get the base dynamics noise specified in config
        R = self._get_cov(self.config['dynamics_noise'])
        if cast_type:
            R = R.astype(state.dtype)
        # Add in additional noise from the terrain functions
        for name, cfg in self.terrains.items():            
            coef = self.terrain_funcs[name](state[:2])
            cov = self._get_cov(cfg['noise'])
            if cast_type:
                cov = cov.astype(state.dtype)
            R += coef * cov
        return R
    
    def observation_noise(self, state=None):
        # TODO can make dependent on other aspects of state than pos
        for name, cfg in self.patches.items():
            if self.in_patch(state[:2], cfg):
                return self._get_cov(cfg['observation_noise'])
        return self._get_cov(self.config['observation_noise'])

    def get_rollouts(self, x, u, n_rollouts=1):
        # Assuming x comes in a vector (N_state,) and u size (H, N_act)
        u = torch.tensor(u).unsqueeze(0).repeat(n_rollouts, 1, 1)
        _, T, N_act = u.size()

        xs = torch.zeros(n_rollouts, T+1, len(x)).double()
        xs[:,0,:] = torch.tensor(x).unsqueeze(0).repeat(n_rollouts, 1)
        for t in range(T):
            R = self.dynamics_noise(xs[:,t,:])
            xs[:,t+1,:] = self.dynamics(xs[:,t,:], u[:,t,:], noise=R)
        return xs
            
    def state_size(self):
        return len(self.config['start_point'])
    
    def in_patch(self, pos, cfg):
        if cfg['type'] == 'rectangle':
            x, y = pos
            x_min, y_min, x_max, y_max = cfg['bounds']
            # TODO this is currently only axis-aligned
            return x >= x_min and x <= x_max and y >= y_min and y <= y_max
        else:
            raise ValueError(f"Unknown patch type for containment computation: {cfg['type']}")
            
    def draw_obstacles(self, ax):
        for name, cfg in self.obstacles.items():
            self._draw_patch(cfg, ax)
            
    def draw_patches(self, ax):
        for name, cfg in self.patches.items():
            self._draw_patch(cfg, ax)

    def draw_terrain(self, ax, min_xy, max_xy, res=100):
        min_x, min_y = min_xy
        max_x, max_y = max_xy
        x = np.linspace(min_x, max_x, res)
        y = np.linspace(min_y, max_y, res)
        xx, yy = np.meshgrid(x,y)
        img = np.zeros((res,res))
        for name, func in self.terrain_funcs.items():
            for i in range(res):
                for j in range(res):
                    img[i,j] += func(np.array([x[i], y[j]]))
        ax.imshow(img.T, interpolation='bilinear', origin='lower',
                  cmap=self.config['terrain_cmap'],
                  extent=[min_x, max_x, min_y, max_y])        
            
    def draw_scene(self, show=False, min_xy=[0,0], max_xy=[10,10], size=(9, 9)):
        fig, ax = plt.subplots(1, 1)
        if 'patches' in self.config:
            self.draw_patches(ax)
        if 'obstacles' in self.config:
            self.draw_obstacles(ax)
        if 'terrains' in self.config:
            self.draw_terrain(ax, min_xy, max_xy)
        if 'viz_patches' in self.config:
            for name, cfg in self.config['viz_patches'].items():
                self._draw_patch(cfg, ax)
        fig.set_size_inches(*size)
        ax.set_xlim(min_xy[0], max_xy[0])
        ax.set_ylim(min_xy[1], max_xy[1])

        plt.tight_layout()
        plt.tick_params(axis='both', which='both', bottom=False, left=False,
                        labelbottom=False, labelleft=False)
        if show:
            plt.show()
        return fig, ax

    def visualize_execution(self, agent_acts, goal_acts=None):
        """
        This will show the agent taking a path with the specified actions, and will
        show the goal moving if the goal actions are specified
        """
        fig, ax = self.draw_scene()

        T = len(agent_acts)
        
        for t in range(T):
            self.apply_action(agent_acts[t])
        agent_states = np.array(self.states)
        agent_path = ax.plot([], [], lw=3, color='indianred', marker='o')[0]

        if goal_acts is not None:
            for t in range(T):
                self.step_goal(goal_acts[t])
                goal_states = np.array(self.goal_states)
                goal_path = ax.plot([], [], lw=3, color='dodgerblue', marker='o')[0]
        
        def animate(t):
            agent_path.set_data(agent_states[:t,0], agent_states[:t,1])
            if goal_acts is not None:
                goal_path.set_data(goal_states[:t,0], goal_states[:t,1])

        return FuncAnimation(fig, animate, frames=T, blit=False, interval=50)
        

    def _sdf_factory(self, cfg):
        if cfg['type'] == 'sphere':
            origin = np.array(cfg['origin'])
            return lambda x: np.array([np.square(x - origin).sum() - cfg['radius']**2])
        else:
            raise ValueError(f"Unsupported type for SDF: {cfg['type']}")

    def _torch_sdf_factory(self, cfg):
        # Assuming batched input (B, N)
        if cfg['type'] == 'sphere':
            origin = torch.tensor(cfg['origin'])
            return lambda x: (x - origin).square().sum(dim=-1) - cfg['radius']**2

    def _terrain_factory(self, cfg):
        if cfg['type'] == 'gaussian':
            mvn = Gaussian(mean=np.array(cfg['coef_mean']), cov=self._get_cov(cfg['coef_cov'], 2))
            return lambda x: mvn.pdf(x)
        else:
            raise ValueError(f"Unsupported type for terrain: {cfg['type']}")

    def _get_cov(self, val, ndims=None):
        if ndims is None:
            ndims = self.state_size()
        return get_cov(val, ndims)
        
    def _draw_patch(self, cfg, ax):
        fc = cfg['fc'] if 'fc' in cfg else 'xkcd:dark'
        ec = cfg['ec'] if 'ec' in cfg else None
        lw = cfg['lw'] if 'lw' in cfg else 3
        hatch = cfg['hatch'] if 'hatch' in cfg else None
        alpha = cfg['alpha'] if 'alpha' in cfg else 1.
        if cfg['type'] == 'rectangle':
            x_min, y_min, x_max, y_max = cfg['bounds']
            patch = patches.Rectangle((x_min, y_min), x_max - x_min, y_max - y_min,
                                      hatch=hatch, fc=fc, ec=ec, lw=lw, alpha=alpha)
        elif cfg['type'] == 'sphere':
            patch = patches.Circle(cfg['origin'], cfg['radius'], fc=fc, ec=ec,
                                   hatch=hatch, lw=lw, alpha=alpha)
        else:
            raise ValueError(f"Unknown obstacle type: {cfg['type']}")
        if 'zorder' in cfg:
            patch.set_zorder(cfg['zorder'])
        ax.add_patch(patch)



if __name__ == '__main__':
    import os.path as osp
    config_fn = osp.join(file_util.dir_of_file(__file__, 1), "config", "di_dynamic_goal.yaml")

    env = PlanarNavigationEnvironment(config_fn)

    agent_acts = np.zeros((50, 2))
    agent_acts[:3,1] = 1.

    goal_acts = np.zeros((50, 2))
    goal_acts[:3,0] = 1.

    anim = env.visualize_execution(agent_acts, goal_acts)
    plt.show()
