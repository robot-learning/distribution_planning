import sys
import os.path as osp
import numpy as np
from pyquaternion import Quaternion
from pybingham import Bingham

from pydrake.math import RigidTransform
from pydrake.geometry import Sphere
from manipulation.scenarios import AddShape, SetColor

from distribution_planning.util import drake_util, math_util
from distribution_planning.distributions import (
    Gaussian, PoseDistribution, PoseMM, get_cov
)
from ll4ma_util import file_util, vis_util


class DrakeObject:

    def __init__(self, cfg, name='obj', no_cache=False, obj_data=None):
        self.cfg = cfg
        self.name = name
        self.no_cache = no_cache
        self.obj_data = obj_data
        
        self.link_name = f"base_link_{name}"
        self.set_pose()

    def set_pose(self):
        if self.obj_data:
            # print("Setting object pose from cached data")
            self.pos = self.obj_data['pos']
            self.quat = self.obj_data['quat']
        elif 'sample_axis' in self.cfg:
            pos, quat = math_util.random_planar_pose(self.cfg['min_pos'],
                                                     self.cfg['max_pos'],
                                                     self.cfg['sample_axis'])
            tf_quat = Quaternion(quat)
            start_quat = Quaternion(matrix=np.array(self.cfg['rotation']))
            new_quat = start_quat * tf_quat
            self.pos = pos
            self.quat = new_quat.elements
        else:
            self.pos = np.array(self.cfg['position'])
            self.quat = Quaternion(matrix=np.array(self.cfg['rotation'])).elements
        self._set_distribution()
        
    def add_to_parser(self, parser, plant):
        parser.AddModelFromFile(self.cfg['path'], self.name)
        obj_frame = plant.GetFrameByName(self.link_name)
        tf = drake_util.get_tf(self.pos, quaternion=self.quat)
        plant.WeldFrames(plant.world_frame(), obj_frame, tf)

    def draw_components(self, plant, scene_graph):
        """
        Draws mixture model components around object mesh, optionally showing modes,
        samples, and the object reference frame.
        """
        cfg = self.cfg['draw_components']
        # Draw component frames
        if 'components' in self.cfg:
            obj_frame = plant.GetFrameByName(self.link_name)
            for frame_name, frame_cfg in self.cfg['components'].items():
                tf = drake_util.get_tf(frame_cfg['position'], frame_cfg['rotation'])
                if cfg['draw_modes']:
                    drake_util.draw_frame(obj_frame, plant, scene_graph,
                                          tf, name=f"{self.name}_{frame_name}",
                                          length=cfg['component_frame_length'],
                                          radius=cfg['component_frame_radius'])
            if cfg['draw_obj_frame']:
                drake_util.draw_frame(obj_frame, plant, scene_graph)

        if cfg['draw_samples']:
            samples = self.distribution.sample(100)
            for i, sample in enumerate(samples):
                pos = sample[:3]
                quat = sample[3:]
                tf = drake_util.get_tf(pos, quaternion=quat)
                drake_util.draw_frame(plant.world_frame(), plant, scene_graph,
                                      tf, name=f"{self.name}_{frame_name}_sample_{i}",
                                      length=cfg['sample_frame_length'],
                                      radius=cfg['sample_frame_radius'],
                                      alpha=cfg['sample_frame_alpha'])

    def draw_reachable(self, plant):
        """
        Draws reachable/unreachable poses as colored spheres around object mesh.
        """
        if self.obj_data:
            cfg = self.cfg['draw_reachable']
            samples = self.obj_data['samples']
            for i, sample in enumerate(samples):
                pos = sample[:3]
                if self.obj_data['samples_reachable'][i]:
                    color = cfg['reachable_color']
                else:
                    color = cfg['unreachable_color']
                self._draw_sphere(plant, cfg['radius'], pos, f"sphere_{i}",
                                  color=color, alpha=cfg['alpha'])

    def _draw_sphere(self, plant, radius, position=[0,0,0], link_name='sphere',
                     ref_frame=None, color='blue', alpha=1.):
        obj = AddShape(plant, Sphere(radius), link_name, color=vis_util.get_color(color, alpha))
        tf = RigidTransform(position)
        obj_frame = plant.GetFrameByName(link_name)
        if ref_frame is None:
            ref_frame = plant.world_frame()
        plant.WeldFrames(ref_frame, obj_frame, tf)
        return obj
        
    def _set_distribution(self):
        """
        Sets PoseMM distribution from params saved in file if they exist, otherwise
        will create them from samples and then save params to cache filename
        """
        tf_W_O = drake_util.get_tf(self.pos, quaternion=self.quat)
        cache_fn = None
        if 'cache_filename' in self.cfg:
            cache_fn = osp.expanduser(self.cfg['cache_filename']) 
        if cache_fn and osp.exists(cache_fn) and not self.no_cache:
            # print(f"Using cached PoseMM params from: {cache_fn}")
            params = file_util.load_pickle(cache_fn)
        else:
            # print("Initializing pose mixture model (might take a minute)...")
            params = {
                'position': self.cfg['position'],
                'rotation': self.cfg['rotation'],
                'components': {}
            }
            n_components = len(self.cfg['components'].keys())
            weights = [1./n_components] * n_components
            for i, (frame_name, frame_cfg) in enumerate(self.cfg['components'].items()):
                tf_O_F = drake_util.get_tf(frame_cfg['position'], frame_cfg['rotation'])
                q_O_F = tf_O_F.rotation().ToQuaternion().wxyz()
                R_O_F = tf_O_F.rotation().matrix()

                p_O_F = tf_O_F.translation()
                cov_F = get_cov(frame_cfg['position_cov'], 3)
                cov_O = R_O_F @ cov_F @ R_O_F.T
                gauss = Gaussian(p_O_F, cov_O)

                # TODO this is the best way I have currently to get the params for a
                # bingham, which is take a nominal quaternion pose and generate a bunch
                # of random samples that are within a small distance to that nominal
                # pose, then fit a Bingham.            
                qs = math_util.random_quats(n_samples=20, dist_threshold=0.1, origin=q_O_F)
                bingham = Bingham()
                bingham.fit(qs)
                params['components'][frame_name] = {
                    'weight': weights[i], 'rot_mode': bingham.mode,
                    'v1': bingham.V[0], 'v2': bingham.V[1], 'v3': bingham.V[2],
                    'z1': bingham.Z[0], 'z2': bingham.Z[1], 'z3': bingham.Z[2],
                    'pos_mean': gauss.get_mean(), 'pos_cov': gauss.get_cov()
                }
    
        self.distribution = PoseMM()
        self.distribution.init_from_params(params, tf_W_O)
        if cache_fn and not osp.exists(cache_fn) and not self.no_cache:
            self.distribution.save_params(cache_fn)
        # print("Mixture model initialized.")
            

if __name__ == '__main__':
    import distribution_planning
    from ll4ma_util import file_util, func_util
    CONFIG_ROOT = osp.join(func_util.get_module_path(distribution_planning), "config")
    config_fn = osp.join(CONFIG_ROOT, "drake_iiwa.yaml")
    cfg = file_util.load_yaml(config_fn)
    obj = DrakeObject(cfg['objects']['sugar'], 'sugar')
    
