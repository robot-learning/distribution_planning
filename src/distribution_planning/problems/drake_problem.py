import pydrake.solvers.mathematicalprogram as mp

import warnings; warnings.filterwarnings(action='ignore', module='ll4ma_opt')
from ll4ma_opt.solvers import SolverReturn


class DrakeProblem:

    def __init__(self):
        self.setup()
    
    def setup(self):
        self.prog = mp.MathematicalProgram()
        self.create_decision_vars()
        self.add_costs()
        self.add_constraints()
        self.set_initial_guess()
        
    def create_decision_vars(self):
        raise NotImplementedError("Need to override create_decision_vars")

    def set_initial_guess(self):
        pass
    
    def add_costs(self):
        pass

    def add_constraints(self):
        pass

    def save_data(self, *args, **kwargs):
        pass

    def get_ll4ma_result(self, result, xs):
        ll4ma_result = SolverReturn()
        ll4ma_result.iterates = xs
        return ll4ma_result

    def visualize_grad_optimization(self, result):
        pass
