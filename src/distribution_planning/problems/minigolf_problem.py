import sys
import numpy as np
import torch
from torch.distributions import MultivariateNormal as MVN
from matplotlib.animation import FuncAnimation
from matplotlib.patches import Ellipse

from distribution_planning.environments import MiniGolfEnvironment
from distribution_planning.distributions import (
    DiracDelta, Gaussian, get_goal_distribution, distribution_math as dist_math,
    kl_divergence
)
from distribution_planning.util.math_util import (
    unscented_transform as UT, batch_unscented_transform as batch_UT
)

from distribution_planning.util import math_util, vis_util, drake_util
from distribution_planning.problems import DrakeProblem

from ll4ma_opt.problems import Problem
from ll4ma_opt.solvers import SolverReturn
from ll4ma_util import torch_util, ui_util, file_util


class MiniGolfProblem(Problem):

    def __init__(self, config, horizon=100, beta=2., goal_cost='ce_iproj'):
        self.config = config
        self.horizon = horizon
        self.beta = beta
        self.env = MiniGolfEnvironment(config)
        
        self.estimate_state()
        self.pG = get_goal_distribution(self.config)
        self._goal_cost = goal_cost

        p0_bounds = self.config['start_region']['bounds']
        min_p = np.array([p0_bounds[0], p0_bounds[1]])
        max_p = np.array([p0_bounds[2], p0_bounds[3]])
        min_v = np.array(self.config['min_x'][2:])
        max_v = np.array(self.config['max_x'][2:])
        self.min_bounds = np.concatenate([min_p, min_v]).reshape(-1, 1)
        self.max_bounds = np.concatenate([max_p, max_v]).reshape(-1, 1)

    def get_initial_guess(self):
        # guess = ((self.max_bounds - self.min_bounds) / 2.) + self.min_bounds
        # guess = np.zeros(4)
        guess = np.random.uniform(self.min_bounds, self.max_bounds)
        return guess
        
    def estimate_state(self):
        # TODO need to integrate better with the DistributionProblem, ideally that will be
        # a more generic problem for distribution planning, but currently it has a lot of
        # stuff for the Nav2d stuff, so need to break that out into a class that extends
        # distribution problem and then this one can do the same and inherit stuff like this
        if self.config['state_estimation'] == 'deterministic':
            self.p0 = DiracDelta(self.env.current_state())
        elif self.env.time == 0 or self.config['state_estimation'] == 'fixed_gaussian':
            mean = self.env.current_state()
            cov = self.config['gaussian_state_cov'] * np.eye(len(mean))
            self.p0 = Gaussian(mean, cov)
        else:
            raise ValueError(f"Unknown state estimation type: {self.config['state_estimation']}")

    def estimate_goal(self):
        # TODO
        pass
        
    def batch_cost(self, u):

        
        if self.config['deterministic']:
            xs = self.env.rollout(u, self.horizon, use_noise=False)
            goal = torch.tensor(self.pG.get_mean()).unsqueeze(0).repeat(len(u), 1)
            costs = torch.linalg.norm(goal - xs[:,-1,:], dim=-1)
        else:
            if self.config['uncertainty_propagation'] == 'monte_carlo':
                means, covs = self.mc_uncertainty_propagation(u)
            elif self.config['uncertainty_propagation'] == 'unscented':
                means, covs = self.ut_uncertainty_propagation(u)
            else:
                raise ValueError(f"Unknown uncertainty propagation method: "
                                 f"{self.config['uncertainty_propagation']}")
            # TODO doing silly batch cost since I don't have batch goal costs defined yet
            costs = torch.zeros(len(u)).double()
            for i in range(len(costs)):
                pT = Gaussian(means[i].numpy(), covs[i].numpy())
                costs[i] = self.goal_cost(pT)
                
        return costs

    def goal_cost(self, pT):
        return dist_math.goal_cost(self.pG, pT, self._goal_cost)

    def mc_uncertainty_propagation(self, x, n_samples=100):
        B = len(x)
        x = x.unsqueeze(1).repeat(1, n_samples, 1).reshape(B * n_samples, -1)        
        xs = self.env.rollout(x, self.horizon).reshape(B, n_samples, self.horizon+1, -1)
        points_T = xs[:,:,-1,:]
        mu_T = points_T.mean(dim=1)
        Sigma_T = math_util.batch_cov(points_T)
        return mu_T, Sigma_T

    def ut_uncertainty_propagation(self, mu0, L0=None):
        B, N = mu0.size()
        if L0 is None:
            # Use initial cov across all samples
            L0 = torch_util.make_batch(self.p0.get_cov_tril(), B)
        x = torch.cat([mu0, L0], dim=-1)

        xs = [x]
        for i in range(self.horizon):
            R = self.env.dynamics_noise(xs[-1][:,:N])
            xs.append(batch_UT(xs[-1], lambda x: self.env.batch_dynamics(x, False, 0.), R=R))
        mu_T = xs[-1][:,:N]
        L_T = xs[-1][:,N:]
        L_T = math_util.batch_vec2tril(L_T)
        Sigma_T = math_util.batch_tril2cov(L_T)
        return mu_T, Sigma_T
        
    def size(self):
        return 4   # init x,y pos/vel of ball

    def state_size(self):
        return 4 # x,y pos/vel of ball

    def visualize_env(self):
        return self.env.draw_scene()
    
    def visualize_start_goal(self, ax):
        self.visualize_start(ax)
        self.visualize_goal(ax)

    def visualize_start(self, ax, s=400, color='tomato', ec='maroon', zorder=2000):
        ax.scatter(*self.p0.get_mean()[:2], s=s, color=color, zorder=zorder, edgecolors=ec)

    def visualize_goal(self, ax, cmap='Blues'):
        self.pG.draw(ax, [0,1], mean_s=100, cmap=cmap)

    def visualize_grad_optimization(self, result):
        mus = np.array(result.mu_iterates)
        Ls = np.array(result.L_iterates)
        
        fig, ax = self.env.draw_scene(size=(10,10))
        self.pG.draw(ax, [0, 1])        
        path = ax.plot([], [], lw=6, color='goldenrod', zorder=1000)[0]
        ellipses = []
        for t in range(self.horizon):
            ellipse = Ellipse([0,0], 0, 0, fc='indigo', ec='black', alpha=0.2)
            ellipses.append(ellipse)
            ax.add_artist(ellipse)
        
        def animate(i):
            ax.set_title(f"Iteration {i}")
            path.set_data(mus[i,:,0], mus[i,:,1])
            for t in range(self.horizon):
                L_t = math_util.vec2tril(Ls[i,t,:])
                sigma = np.dot(L_t, L_t.T)[:2,:2].astype(float)
                vis_util.update_ellipse(ellipses[t], mus[i,t,:2].astype(float), sigma, self.beta)
            return [path]
        
        return FuncAnimation(fig, animate, frames=len(mus), blit=False)
        
    def visualize_sampling_optimization(self, result):
        iterates = torch.tensor(result.iterates).squeeze(-1).unsqueeze(1)
        sample_iterates = torch.tensor(result.sample_iterates)
        n_samples = sample_iterates.size(1)

        mean_trajs = [self.env.rollout(i, self.horizon).squeeze(0) for i in iterates]
        sample_trajs = [self.env.rollout(si, self.horizon) for si in sample_iterates]

        fig, ax = self.env.draw_scene(size=(10,10))
        self.pG.draw(ax, [0, 1])        
        path = ax.plot([], [], lw=6, color='goldenrod', zorder=1000)[0]
        sample_paths = [ax.plot([], [], lw=1, alpha=0.5, color='g')[0] for _ in range(n_samples)]

        def animate(i):
            ax.set_title(f"Iteration {i}")
            path.set_data(mean_trajs[i][:,0], mean_trajs[i][:,1])
            for j in range(n_samples):
                sample_paths[j].set_data(sample_trajs[i][j][:,0], sample_trajs[i][j][:,1])
            return sample_paths + [path]
        
        return FuncAnimation(fig, animate, frames=len(sample_iterates), blit=False)

    def visualize_solution(self, ax, result, n_rollouts=1000, n_visualize=30,
                           mean_c='goldenrod', mean_lw=5, mean_ls='solid',
                           cov_c='indigo', cov_alpha=0.1, horizon=None):
        # TODO can clean up this logic once you have the full distribution visualized from grad
        
        if hasattr(result, 'mu_soln') and hasattr(result, 'L_soln'):
            # This is populated if solved by grad solver
            self.p0.set_mean(result.mu_soln[0,:])
            mu = result.mu_soln
            L = result.L_soln
            path = ax.plot(mu[:,0], mu[:,1], lw=3, color='indigo')
            for t in range(len(mu)):
                ellipse = Ellipse([0,0], 0, 0, fc=cov_c, ec='black', alpha=cov_alpha)
                ax.add_artist(ellipse)
                L_t = math_util.vec2tril(L[t,:])
                sigma = np.dot(L_t, L_t.T)[:2,:2].astype(float)
                vis_util.update_ellipse(ellipse, mu[t,:2].astype(float), sigma, self.beta)
        else:
            x = torch.tensor(result.iterates[-1]).squeeze().unsqueeze(0).repeat(n_rollouts, 1)
            if horizon is None:
                horizon = self.horizon
            xs = self.env.rollout(x, horizon)
            ps_T = xs[:,-1]
            vis_idxs = torch.randperm(len(xs))[:n_visualize]
            objs = ax.scatter(ps_T[vis_idxs,0], ps_T[vis_idxs,1],
                              s=100, color='indigo', alpha=0.4, zorder=1000)

            self.p0.set_mean(x[0,:].numpy())
            self.visualize_start(ax)
            
            for path in xs[vis_idxs]:
                ax.plot(path[:,0], path[:,1], lw=1, color='indigo', alpha=0.5)

            gauss = Gaussian()
            gauss.fit(ps_T.numpy())
            gauss.draw(ax, [0,1], cmap='Greys')

            print("  KL divergence", kl_divergence(gauss, self.pG))
        return xs

    def save_data(self, ll4ma_result, args, save_fn):
        data = {
            'args': vars(args),
            'result': ll4ma_result,
            'cfg': self.config
        }
        file_util.save_pickle(data, save_fn)
            

class DrakeMiniGolfProblem(MiniGolfProblem, DrakeProblem):

    def __init__(self, config, horizon, beta, goal_cost):
        MiniGolfProblem.__init__(self, config, horizon, beta, goal_cost)
        DrakeProblem.__init__(self)

    def create_decision_vars(self):
        N = self.state_size()
        self.mu = self.prog.NewContinuousVariables(N, self.horizon, "mu")
        self.L = self.prog.NewContinuousVariables(int(N*(N+1)/2.), self.horizon, "L")

    def get_decision_vars(self):
        return self.mu, self.L

    def set_initial_guess(self, init_to_goal=True):
        # Set mean guess as either straight line to goal or straight line to random point
        min_x = np.array(self.config['min_x'])
        max_x = np.array(self.config['max_x'])
        goal_point = self.pG.get_mean()
        p0_min_x, p0_min_y, p0_max_x, p0_max_y = self.config['start_region']['bounds']
        p0 = np.array([p0_min_x + ((p0_max_x - p0_min_x) / 2.),
                       p0_min_y + ((p0_max_y - p0_min_y) / 2.)])
        pt = goal_point if init_to_goal else np.random.uniform(min_x[0], max_x[0], size=2)
        direct = (pt[:2] - p0) / np.linalg.norm(pt[:2] - p0)
        vel0 = np.linalg.norm(max_x[2:]) * direct
        # init_mu = np.stack([np.linspace(p0[0], pt[0], self.horizon),
        #                     np.linspace(p0[1], pt[1], self.horizon),
        #                     np.linspace(vel0[0], pt[2], self.horizon),
        #                     np.linspace(vel0[1], pt[3], self.horizon)])
        init_mu = np.stack([np.random.randn(self.horizon),
                            np.random.randn(self.horizon),
                            np.random.randn(self.horizon),
                            np.random.randn(self.horizon)])

                            # np.random.randn(self.horizon),
                            # np.random.randn(self.horizon)])
        L0 = self.p0.get_cov_tril()
        
        self.prog.SetInitialGuess(self.mu, init_mu)
        self.prog.SetInitialGuess(self.L, np.repeat(np.expand_dims(L0, 1), self.horizon, axis=1))

    def add_costs(self):
        drake_util.add_cost(self.prog, self._drake_goal_cost,
                            np.concatenate([self.mu[:,-1], self.L[:,-1]]), "goal")

    def add_constraints(self):
        min_x = np.array(self.config['min_x'])
        max_x = np.array(self.config['max_x'])
        min_L = self.config['min_L']
        max_L = self.config['max_L']
        L0 = self.p0.get_cov_tril()
        N = self.state_size()
        M = N + int(N*(N+1)/2.) # Mean and tril sigma

        # mu0 = self.p0.get_mean()

        # Action is choice of start pos/vel where pos is constrained to designated region
        # and vel is within the set bounds
        p0_min_x, p0_min_y, p0_max_x, p0_max_y = self.config['start_region']['bounds']
        mu0_min = np.array([p0_min_x, p0_min_y, min_x[2], min_x[3]])
        mu0_max = np.array([p0_max_x, p0_max_y, max_x[2], max_x[3]])
        drake_util.add_bb_constraint(self.prog, mu0_min, mu0_max, self.mu[:,0], f"mu0 EQ mu init")
        drake_util.add_bb_constraint(self.prog, L0, L0, self.L[:,0], f"L0 EQ L init")

        for t in range(self.horizon-1):
            drake_util.add_bb_constraint(self.prog, min_x, max_x, self.mu[:,t], f"BB mu{t}")
            drake_util.add_bb_constraint(self.prog, min_L, max_L, self.L[:,t], f"BB L{t}")
            
            drake_util.add_constraint(self.prog, self._drake_ut_constraint,
                                      np.zeros(M), np.zeros(M),
                                      np.concatenate([self.mu[:,t], self.L[:,t],
                                                      self.mu[:,t+1], self.L[:,t+1]]),
                                      f"UT from t={t} to t={t+1}")
                    
    def get_ll4ma_result(self, drake_result, xs):
        result = SolverReturn()
        result.iterates = xs
        result.mu_iterates = [drake_util.get_decision_var(self.prog, x, self.mu).T for x in xs]
        result.L_iterates = [drake_util.get_decision_var(self.prog, x, self.L).T for x in xs]
        result.mu_soln = result.mu_iterates[-1]
        result.L_soln = result.L_iterates[-1]
        return result        
    
    def _drake_goal_cost(self, z):
        N = self.state_size()
        pT = Gaussian(z[:N], cov_tril=math_util.vec2tril(z[N:]))
        return dist_math.goal_cost(self.pG, pT, self._goal_cost)

    # def _drake_dyn_constraint(self, z):
    #     N = self.state_size()
    #     x1 = z[:N]
    #     x2 = z[N:]
    #     constraint = self.env.dynamics(x1) - x2
    #     return constraint

    def _drake_ut_constraint(self, z):
        N = self.state_size()
        M = N + int(N*(N+1)/2.) # Mean and tril sigma
        x1 = z[:M]
        x2 = z[M:2*M]
        R = self.env.dynamics_noise(x1, cast_type=True)
        constraint = UT(x1, N, self.env.dynamics, R=R) - x2
        return constraint

