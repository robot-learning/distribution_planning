import os.path as osp
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.patches import Ellipse
from copy import deepcopy

import pydrake.solvers.mathematicalprogram as mp

from distribution_planning.distributions import (
    Gaussian, DiracDelta, get_goal_distribution, distribution_math as dist_math
)
from distribution_planning.environments import PlanarNavigationEnvironment
from distribution_planning.util.math_util import (
    unscented_transform as UT, batch_unscented_transform as batch_UT, vec2tril
)
from distribution_planning.util.env_util import (
    di_kalman_filter as di_KF, unscented_kalman_filter as UKF,
    di_dynamics, dubins_dynamics, kalman_filter
)
from distribution_planning.util import math_util, vis_util, drake_util

import torch
import warnings; warnings.filterwarnings(action='ignore', module='ll4ma_opt')
from ll4ma_opt.problems import Problem
from ll4ma_opt.solvers import SolverReturn

from ll4ma_util import file_util


class PlanarNavigationProblem(Problem):
    
    def __init__(self, config, horizon=50, beta=2., goal_cost='kl_iproj'):
        self.config = config
        self.env = PlanarNavigationEnvironment(config)
        self.estimate_state()
        self.pG = get_goal_distribution(self.config)
        self.projected_pG = None
        self.horizon = horizon
        self.beta = beta
        self._goal_cost = goal_cost
            
        self.min_bounds = np.tile([self.config['min_u']], horizon).reshape(-1, 1)
        self.max_bounds = np.tile([self.config['max_u']], horizon).reshape(-1, 1)

    def estimate_state(self):
        if self.config['state_estimation'] == 'deterministic':
            self.p0 = DiracDelta(self.env.current_state())
        elif self.env.time == 0 or self.config['state_estimation'] == 'fixed_gaussian':
            mean = self.env.current_state()
            cov = self.config['gaussian_state_cov'] * np.eye(len(mean))
            self.p0 = Gaussian(mean, cov)
        elif self.config['state_estimation'] == 'kalman_filter':
            R = self.env.dynamics_noise(self.env.states[-2]) # Want from state before applied act
            Q = self.env.observation_noise()
            C = self.env.linear_observation_matrix()
            z = self.env.observe()
            u = self.env.acts[-1]
            mu = self.p0.get_mean()
            Sigma = self.p0.get_cov()
            dt = self.config['dt']
            if self.config['dynamics'] in ['di', 'double_integrator']:
                mu_new, Sigma_new = di_KF(mu, Sigma, u, R, z, C, Q, dt)
                self.p0.mean = mu_new
                self.p0.cov = Sigma_new
            else:
                raise NotImplementedError(f"Unknown dynamics for KF: {self.config['dynamics']}")
        elif self.config['state_estimation'] in ['unscented_kalman_filter', 'ukf']:
            R = self.env.dynamics_noise(self.env.states[-2]) # Want from state before applied act
            Q = self.env.observation_noise()
            h = lambda x: x  # TODO don't yet have a meaningful observation function
            g = self.env.dynamics
            z = self.env.observe()
            u = self.env.acts[-1]
            mu = self.p0.get_mean()
            Sigma = self.p0.get_cov()
            dt = self.config['dt']
            mu_new, Sigma_new = UKF(mu, Sigma, u, z, g, R, h, Q, dt)
            self.p0.mean = mu_new
            self.p0.cov = Sigma_new
        else:
            raise ValueError(f"Unknown state estimation type: {self.config['state_estimation']}")

    def estimate_goal(self):
        if self.config['goal_estimation'] == 'fixed':
            pass
        elif self.config['goal_estimation'] == 'kalman_filter':            
            mu = self.pG.get_mean()
            Sigma = self.pG.get_cov()
            dt = self.config['dt']
            actual_goal = self.env.current_goal()
            
            # TODO need to set R to something, it will essentially be the noise model
            # the agent assumes for the goal. I'm letting the goal move deterministically
            # so this can be a discrepancy between what's modeled and what's actually
            # happening.
            u = self.env.goal_u
            # R = np.zeros(self.state_size())
            R = 0.1 * np.eye(self.state_size())

            # TODO trying identity, this will then just give noisy estimate of actual goal
            C = np.eye(self.state_size())
            Q = self.env._get_cov(self.config['goal_observation_noise'])

            current_state = self.env.current_state()
            dist_to_goal = np.linalg.norm(actual_goal - current_state)
            alpha = 0.1  # TODO may need to adjust
            factor = 1. - np.exp(-alpha * dist_to_goal)
            Q *= factor

            delta = Gaussian(np.zeros(self.state_size()), Q).sample()
            z = C @ actual_goal + delta
            
            mu_new, Sigma_new = di_KF(mu, Sigma, u, R, z, C, Q, dt)
            self.pG.mean = mu_new
            self.pG.cov = Sigma_new
        else:
            raise ValueError(f"Unknown goal estimation type: {self.config['goal_estimation']}")
        
    def get_initial_guess(self):
        return None
        
    def batch_cost(self, u):
        B = u.size(0)
        N = self.p0.size()
        dt = self.config['dt']
        u = u.view(B, self.horizon, self.act_size())
        costs = torch.zeros(B)
        x0 = torch.cat([torch.tensor(self.p0.get_mean()), torch.tensor(self.p0.get_cov_tril())])
        x = self.get_trajectory(x0, u) # (B, H+1, M)

        # Apply Kalman filter over horizon to predict what the goal distribution will be
        if self.config['goal_moving']:
            goal_xs, goal_us = self.env.get_goal_trajectory(n_steps=self.horizon)
            
            goal_mus = [deepcopy(self.pG.get_mean())]
            goal_Sigmas = [deepcopy(self.pG.get_cov())]
            for t, (goal_x, goal_u) in enumerate(zip(goal_xs, goal_us)):
                # TODO need to set R to something, it will essentially be the noise model
                # the agent assumes for the goal. I'm letting the goal move deterministically
                # so this can be a discrepancy between what's modeled and what's actually
                # happening.
                # R = np.zeros(self.state_size())
                R = 0.1 * np.eye(self.state_size())
                
                # TODO trying identity, this will then just give noisy estimate of actual goal
                C = np.eye(self.state_size())
                Q = self.env._get_cov(self.config['goal_observation_noise'])
                
                
                # projected_state = x[i,t,:N].numpy()
                state = self.p0.get_mean()
                dist_to_goal = np.linalg.norm(goal_x - state)
                alpha = 0.3  # TODO may need to adjust
                factor = 1. - np.exp(-alpha * dist_to_goal)
                Q *= factor
        
                delta = Gaussian(np.zeros(self.state_size()), Q).sample()
                z = C @ goal_x + delta
        
                goal_mu_new, goal_Sigma_new = di_KF(goal_mus[-1], goal_Sigmas[-1],
                                                    goal_u, R, z, C, Q, dt)
                goal_mus.append(goal_mu_new)
                goal_Sigmas.append(goal_Sigma_new)

            # TODO trying only pos
            self.projected_pG = Gaussian(goal_mus[-1][:2], goal_Sigmas[-1][:2,:2])

        # TODO this is still an inefficiency, can compute KL in batch but you'll need
        # to first batchify the distributions and KL computation, should speed things up
        for i in range(B):
            # Get the projected state distributions over the horizon
            ps = []
            for t in range(self.horizon + 1):
                mu = x[i,t,:N].numpy()
                cov_tril = vec2tril(x[i,t,N:].numpy())
                # TODO trying only pos
                mu = mu[:2]
                cov_tril = cov_tril[:2,:2]
                                    
                ps.append(Gaussian(mu, cov_tril=cov_tril))


            pG = self.projected_pG if self.config['goal_moving'] else self.pG
            costs[i] = self.goal_cost(ps[-1], pG)

            costs[i] += self.collision_cost(ps)
                
        # TODO I don't know if you want this or not
        # costs += self.action_cost(u)
        
        return costs
    
    def goal_cost(self, pT, pG=None):
        if pG is None:
            pG = self.pG
        return dist_math.goal_cost(pG, pT, self._goal_cost)
            
    def collision_cost(self, ps):
        '''
        Adds a collision cost for all sigma points (including mean) given input 
        Gaussian state distributions.
        '''
        collision = 0
        # Note doing unique here since if there are non-position dims you get duplicates
        sps = [torch.tensor(np.unique(p.get_sigma_points(self.beta, True)[:,:2], axis=0))
               for p in ps]
        sps = torch.cat(sps, dim=0)
        for sdf in self.env.torch_sdfs.values():
            collision += torch.max(torch.zeros(len(sps)), -sdf(sps)).sum()
        collision *= 1000  # TODO trying amplifying, too weak relative to KL otherwise
        return collision

    def action_cost(self, u):
        # u (B, H, act_size)
        return u.square().sum(dim=(1,2))
    
    def get_trajectory(self, x0, u):
        '''
        Compute distribution trajectory (in batch) using unscented transform.

        Args:
            x0 (Tensor): Initial state distribution as concatenated mean and L
            u (Tensor): Sequence of actions (B, H, N) or (H, N) if not a batch.
        '''
        if u.ndim == 2:
            u = u.unsqueeze(0) # Add batch dim
        B, H, _ = u.size()
        x = torch.zeros(B, H+1, len(x0)).double()
        x[:,0,:] = x0.unsqueeze(0).repeat(B, 1)
        for t in range(u.size(1)):
            R = self.env.dynamics_noise(x[:,t,:])
            g = lambda z: self.env.dynamics(z, u[:,t,:])
            x[:,t+1,:] = batch_UT(x[:,t,:], g, self.beta, R)  # (B, M)        
        return x
    
    def get_deterministic_trajectory(self, x0, u):
        xs = [x0]
        for u_t in u:
            xs.append(self.env.dynamics(xs[-1], u_t))
        return np.array(xs)
            
    def act_size(self):
        return 2  # x,y acc for DI, lin_speed, turn_rate for dubins

    def state_size(self):
        return 4 if self.config['dynamics'] in ['di', 'double_integrator'] else 3
        
    def size(self):
        return self.horizon * self.act_size()

    def unflatten_act_seq(self, seq):
        return seq.reshape(self.horizon, self.act_size())

    def set_horizon(self, horizon):
        self.horizon = horizon
        self.min_bounds = np.tile([self.config['min_u']], horizon).reshape(-1, 1)
        self.max_bounds = np.tile([self.config['max_u']], horizon).reshape(-1, 1)
    
    def visualize_env(self):
        return self.env.draw_scene(min_xy=self.config['min_x'][:2],
                                   max_xy=self.config['max_x'][:2],
                                   size=self.config['max_x'][:2])

    def visualize_start_goal(self, ax):
        self.visualize_start(ax)
        self.visualize_goal(ax)

    def visualize_start(self, ax):
        ax.scatter(*self.config['start_point'][:2], s=300, color='tomato', zorder=2000,
                   edgecolors='maroon')

    def visualize_goal(self, ax):
        self.pG.draw(ax, [0,1])
        
    def visualize_solution(self, ax, result, mean_c='indigo', mean_lw=1, mean_ls='solid',
                           mean_marker='o', mean_ms=10, mean_marker_ec='none',
                           mean_marker_fc='indigo', cov_c='indigo', cov_alpha=0.2):
        if hasattr(result, 'mu_soln'):
            mu = result.mu_soln
            L = result.L_soln
            u = result.u_soln            
        else:
            u = torch.tensor(self.unflatten_act_seq(result.iterates[-1]))
            mu0 = self.p0.get_mean()
            L0 = self.p0.get_cov_tril()
            x0 = torch.tensor(np.concatenate([mu0, L0]))
            traj = self.get_trajectory(x0, u).squeeze(0).numpy()
            N = len(mu0)
            mu = traj[:,:N]
            L = traj[:,N:]
            
        path = ax.plot(mu[:,0], mu[:,1], lw=mean_lw, color=mean_c,
                       ls=mean_ls, marker=mean_marker, ms=mean_ms,
                       mec=mean_marker_ec, mfc=mean_marker_fc)[0]
        for t in range(len(mu)):
            ellipse = Ellipse([0,0], 0, 0, fc=cov_c, ec='black', alpha=cov_alpha)
            ax.add_artist(ellipse)
            L_t = math_util.vec2tril(L[t,:])
            sigma = np.dot(L_t, L_t.T)[:2,:2].astype(float)
            vis_util.update_ellipse(ellipse, mu[t,:2].astype(float), sigma, self.beta)        

    def visualize_grad_optimization(self, result):
        pass
            
    def visualize_sampling_optimization(self, result):
        iterates = [torch.tensor(i.reshape(self.horizon, self.act_size()))
                    for i in result.iterates]
        sample_iterates = [torch.tensor(s.reshape(-1, self.horizon, self.act_size()))
                           for s in result.sample_iterates]

        mean_trajs = [self.get_deterministic_trajectory(self.p0.get_mean(), i) for i in iterates]
        
        n_samples = sample_iterates[0].size(0)
        sample_trajs = []
        for iterate in sample_iterates:
            trajs = [self.get_deterministic_trajectory(self.p0.get_mean(), s) for s in iterate]
            sample_trajs.append(trajs)

        fig, ax = self.env.draw_scene(size=(10,10))
        self.pG.draw(ax, [0, 1])
        
        path = ax.plot([], [], lw=6, color='goldenrod', zorder=1000)[0]
        sample_paths = [ax.plot([], [], lw=1, alpha=0.5, color='g')[0] for _ in range(n_samples)]

        def animate(i):
            ax.set_title(f"Iteration {i}")
            for j in range(n_samples):
                sample_paths[j].set_data(sample_trajs[i][j][:,0], sample_trajs[i][j][:,1])
            path.set_data(mean_trajs[i][:,0], mean_trajs[i][:,1])
            return sample_paths + [path]
        
        return FuncAnimation(fig, animate, frames=len(sample_iterates), blit=False)
            
    def visualize_mpc(self, results, problem, n_std=5): # TODO this should probably match beta
        items = []
        for t, res in enumerate(results):
            for i, (iterate, sample_iterates) in enumerate(zip(res.iterates, res.sample_iterates)):
                # iterate = self.unflatten_act_seq(iterate)
                iterate = iterate.reshape(res.horizon, self.act_size())
                sample_iterates = sample_iterates.reshape(-1, res.horizon, self.act_size())
                mean_traj = self.get_deterministic_trajectory(res.p0.get_mean(), iterate)
                sample_trajs = [self.get_deterministic_trajectory(res.p0.get_mean(), s)
                                for s in sample_iterates]
                proj_pG = res.projected_pG if hasattr(res, 'projected_pG') else None
                items.append((t, i, mean_traj, sample_trajs, res.p0, res.pG, proj_pG))
                
        fig, ax = self.visualize_env()
        ax.set_facecolor('gainsboro')
        if self.config['goal_moving']:
            proj_goal_artists = self.pG.draw(ax, [0,1], n_std=n_std, cmap='Greens')
        init_artists = self.p0.draw(ax, [0,1], n_std=n_std, cmap='Purples', mean_s=0)
        goal_artists = self.pG.draw(ax, [0,1], n_std=n_std)
        
        mean_path = ax.plot([], [], lw=5, color='goldenrod', ls=':', zorder=1000)[0]
        actual_path = ax.plot([], [], lw=3, color='indianred', marker='o')[0]
        sample_paths = [ax.plot([], [], lw=1, alpha=0.5, color='indigo')[0]
                        for _ in range(len(sample_iterates))]
        goal_path = ax.plot([], [], lw=3, color='dodgerblue', marker='o')[0]
    
        states = np.array(self.env.states)
        goal_states = np.array(self.env.goal_states)

        def animate(i):
            t, i, mean_traj, sample_trajs, p0, pG, proj_pG = items[i]
            ax.set_title(f"t={t}, iter={i}")
                
            for j, sample_traj in enumerate(sample_trajs):
                sample_paths[j].set_data(sample_traj[:,0], sample_traj[:,1])
    
            mean_path.set_data(mean_traj[:,0], mean_traj[:,1])
            actual_path.set_data(states[:t,0], states[:t,1])
            goal_path.set_data(goal_states[:t+1,0], goal_states[:t+1,1])
            
            for i in range(n_std):
                if self.config['goal_moving']:
                    # Projected goal gaussian
                    w, h, theta = vis_util.get_cov_ellipse(proj_pG.get_cov()[:2,:2], n_std=i+1)
                    proj_goal_artists[f'g_std_{i+1}'].set_width(w)
                    proj_goal_artists[f'g_std_{i+1}'].set_height(h)
                    proj_goal_artists[f'g_std_{i+1}'].set_angle(theta)
                    proj_goal_artists[f'g_std_{i+1}'].set_center(proj_pG.get_mean()[:2])
                    if 'g_mean' in proj_goal_artists:
                        proj_goal_artists[f'g_mean'].set_offsets(proj_pG.get_mean()[:2])
                
                # Goal gaussian
                w, h, theta = vis_util.get_cov_ellipse(pG.get_cov()[:2,:2], n_std=i+1)
                goal_artists[f'g_std_{i+1}'].set_width(w)
                goal_artists[f'g_std_{i+1}'].set_height(h)
                goal_artists[f'g_std_{i+1}'].set_angle(theta)
                goal_artists[f'g_std_{i+1}'].set_center(pG.get_mean()[:2])
                if 'g_mean' in goal_artists:
                    goal_artists[f'g_mean'].set_offsets(pG.get_mean()[:2])

                # State gaussian
                w, h, theta = vis_util.get_cov_ellipse(p0.get_cov()[:2,:2], n_std=i+1)
                init_artists[f'g_std_{i+1}'].set_width(w)
                init_artists[f'g_std_{i+1}'].set_height(h)
                init_artists[f'g_std_{i+1}'].set_angle(theta)
                init_artists[f'g_std_{i+1}'].set_center(p0.get_mean()[:2])
                if 'g_mean' in init_artists:
                    init_artists[f'g_mean'].set_offsets(p0.get_mean()[:2])
    
            return [mean_path, actual_path] + sample_paths
            
        return FuncAnimation(fig, animate, frames=len(items), blit=False, interval=50)

    def visualize_test_execution(self, agent_acts, n_std=5):
        """
        This will show the agent taking a path with the specified actions, and 
        will show the goal moving if the goal actions are specified, and the 
        update uncertainty models.
        """
        fig, ax = self.env.draw_scene()

        T = len(agent_acts)
        
        for t in range(T):
            self.env.apply_action(agent_acts[t])
        agent_states = np.array(self.env.states)
        agent_path = ax.plot([], [], lw=3, color='indianred', marker='o')[0]

        pGs = [deepcopy(self.pG)]
        for t in range(T):
            self.env.step_goal()
            pGs.append(deepcopy(self.pG))
            self.estimate_goal()
        goal_states = np.array(self.env.goal_states)
        goal_path = ax.plot([], [], lw=3, color='dodgerblue', marker='o')[0]
                
        goal_artists = self.pG.draw(ax, [0,1], n_std=n_std)
        
        def animate(t):
            # agent_path.set_data(agent_states[:t,0], agent_states[:t,1])
            goal_path.set_data(goal_states[t,0], goal_states[t,1])

            for i in range(n_std):
                w, h, theta = vis_util.get_cov_ellipse(pGs[t].get_cov()[:2,:2], n_std=i+1)
                goal_artists[f'g_std_{i+1}'].set_width(w)
                goal_artists[f'g_std_{i+1}'].set_height(h)
                goal_artists[f'g_std_{i+1}'].set_angle(theta)
                goal_artists[f'g_std_{i+1}'].set_center(pGs[t].get_mean()[:2])
                if 'g_mean' in goal_artists:
                    goal_artists[f'g_mean'].set_offsets(pGs[t].get_mean()[:2])

        return FuncAnimation(fig, animate, frames=T, blit=False, interval=50)

    
class DrakePlanarNavigationProblem(PlanarNavigationProblem):

    def __init__(self, config, horizon, beta, goal_cost, init_to_goal=False):
        super().__init__(config, horizon, beta, goal_cost)
        self.prog = mp.MathematicalProgram()
        self.create_decision_vars()
        self.set_initial_guess(init_to_goal)
        self.add_costs()
        self.add_constraints()

    def create_decision_vars(self):
        N = self.state_size()                
        self.u = self.prog.NewContinuousVariables(2, self.horizon-1, "u")
        self.mu = self.prog.NewContinuousVariables(N, self.horizon, "mu")
        self.L = self.prog.NewContinuousVariables(int(N*(N+1)/2.), self.horizon, "L")

    def get_decision_vars(self):
        return self.u, self.mu, self.L
        
    def set_initial_guess(self, init_to_goal=True):
        mu0 = self.p0.get_mean()
        L0 = self.p0.get_cov_tril()

        # Set mean guess as either straight line to goal or straight line to random point
        min_x = np.array(self.config['min_x'])
        max_x = np.array(self.config['max_x'])            
        pt = self.pG.get_mean() if init_to_goal else np.random.uniform(min_x[0], max_x[0], size=2) 
        init_mu = np.stack([np.linspace(mu0[0], pt[0], self.horizon),
                            np.linspace(mu0[1], pt[1], self.horizon),
                            np.random.randn(self.horizon)])
        # init_mu = np.stack([np.random.randn(self.horizon),
        #                     np.random.randn(self.horizon),
        #                     np.random.randn(self.horizon)])

        if self.config['dynamics'] in ['di', 'double_integrator']:
            init_mu = np.concatenate([init_mu, np.random.randn(1, self.horizon)], axis=0)

        self.prog.SetInitialGuess(self.u, np.random.randn(2, self.horizon-1))
        self.prog.SetInitialGuess(self.mu, init_mu)
        self.prog.SetInitialGuess(self.L, np.repeat(np.expand_dims(L0, 1), self.horizon, axis=1))
        
    def add_costs(self):
        drake_util.add_cost(self.prog, self._drake_goal_cost,
                            np.concatenate([self.mu[:,self.horizon-1], self.L[:,self.horizon-1]]),
                            f"Goal")
    
    def add_constraints(self):
        min_x = np.array(self.config['min_x'])
        max_x = np.array(self.config['max_x'])
        min_u = np.array(self.config['min_u'])
        max_u = np.array(self.config['max_u'])
        min_L = self.config['min_L']
        max_L = self.config['max_L']
        min_xdiff = np.array(self.config['min_xdiff']) if 'min_xdiff' in self.config else None
        max_xdiff = np.array(self.config['max_xdiff']) if 'max_xdiff' in self.config else None
        min_udiff = np.array(self.config['min_udiff']) if 'min_udiff' in self.config else None
        max_udiff = np.array(self.config['max_udiff']) if 'max_udiff' in self.config else None
        mu0 = self.p0.get_mean()
        L0 = self.p0.get_cov_tril()
        N = self.state_size()
        M = N + int(N*(N+1)/2.) # Mean and tril sigma
        
        drake_util.add_bb_constraint(self.prog, mu0, mu0, self.mu[:,0], f"mu0 EQ to mu init")
        drake_util.add_bb_constraint(self.prog, L0, L0, self.L[:,0], f"L0 EQ to L init")
        # Ensure linear speed starts and ends at zero for dubins
        if self.config['dynamics'] == 'dubins':
            drake_util.add_bb_constraint(self.prog, np.zeros(1), np.zeros(1),
                                         self.u[0,0], "u00 EQ to zero")
            drake_util.add_bb_constraint(self.prog, np.zeros(1), np.zeros(1),
                                         self.u[0,-1], "uT0 EQ to zero")

        for t in range(self.horizon-1):
            drake_util.add_bb_constraint(self.prog, min_u, max_u, self.u[:,t], f"BB u{t}")
            drake_util.add_bb_constraint(self.prog, min_x, max_x, self.mu[:,t], f"BB mu{t}")
            drake_util.add_bb_constraint(self.prog, min_L, max_L, self.L[:,t], f"BB L{t}")
        
            drake_util.add_constraint(self.prog, self._drake_ut_constraint,
                                      np.zeros(M), np.zeros(M),
                                      np.concatenate([self.mu[:,t], self.L[:,t],
                                                      self.mu[:,t+1], self.L[:,t+1], self.u[:,t]]),
                                      f"UT from t={t} to t={t+1}")            
        
            if min_xdiff is not None and max_xdiff is not None:
                drake_util.add_constraint(self.prog, self._drake_x_diff_constraint,
                                          min_xdiff, max_xdiff,
                                          np.concatenate([self.mu[:,t], self.mu[:,t+1]]),
                                          f"XDIFF from t={t} to t={t+1}")
            if min_udiff is not None and max_udiff is not None and t < self.horizon - 2:
                drake_util.add_constraint(self.prog, self._drake_u_diff_constraint,
                                          min_udiff, max_udiff,
                                          np.concatenate([self.u[:,t], self.u[:,t+1]]),
                                          f"UDIFF from t={t} to t={t+1}")
        
            for sdf_name, sdf in self.env.sdfs.items():
                drake_util.add_constraint(self.prog, sdf, [0], [np.inf], self.mu[:2,t],
                                          f"SDF collision mu{t}")
                for i in range(N):
                    drake_util.add_constraint(
                        self.prog, lambda z, sdf=sdf, i=i: self._drake_sp_collision(z, sdf, i, 1),
                        [0], [np.inf], np.concatenate([self.mu[:,t], self.L[:,t]]),
                        f"SDF {sdf_name} SP+ {i}, t={t}")
                    drake_util.add_constraint(
                        self.prog, lambda z, sdf=sdf, i=i: self._drake_sp_collision(z, sdf, i, -1),
                        [0], [np.inf], np.concatenate([self.mu[:,t], self.L[:,t]]),
                        f"SDF {sdf_name} SP- {i}, t={t}")

    def get_ll4ma_result(self, drake_result, xs):
        result = SolverReturn()
        result.iterates = xs
        result.u_soln = drake_util.get_decision_var(self.prog, xs[-1], self.u).T
        result.mu_soln = drake_util.get_decision_var(self.prog, xs[-1], self.mu).T
        result.L_soln = drake_util.get_decision_var(self.prog, xs[-1], self.L).T
        return result
                    
    def save_data(self, xs, ll4ma_result, save_fn, args):
        save_prefix = osp.splitext(osp.basename(save_fn))[0]
        data = {
            'args': vars(args),
            'iterates': xs[:],
            'u_iterates': [drake_util.get_decision_var(self.prog, x, self.u).T for x in xs],
            'mu_iterates': [drake_util.get_decision_var(self.prog, x, self.mu).T for x in xs],
            'L_iterates': [drake_util.get_decision_var(self.prog, x, self.L).T for x in xs],
            'save_prefix': save_prefix,
            'result': ll4ma_result
        }
        file_util.save_pickle(data, save_fn)
                    
    def _drake_goal_cost(self, z):
        N = self.state_size()
        pT = Gaussian(z[:N], cov_tril=math_util.vec2tril(z[N:]))
        return dist_math.goal_cost(self.pG, pT, self._goal_cost)

    def _drake_ut_constraint(self, z):
        dt = self.config['dt']
        N = self.state_size()
        M = N + int(N*(N+1)/2.) # Mean and tril sigma
        x1 = z[:M]
        x2 = z[M:2*M]
        u = z[2*M:]
        R = self.env.dynamics_noise(x1[:2], cast_type=True) # TODO handle indices more generally
        if self.config['dynamics'] in ['di', 'double_integrator']:
            constraint = UT(x1, N, lambda y: di_dynamics(y, u, dt), R=R) - x2
        elif self.config['dynamics'] == 'dubins':
            constraint = UT(x1, N, lambda y: dubins_dynamics(y, u, dt), R=R) - x2
        else:
            raise ValueError(f"Unknown dynamics: {self.config['dynamics']}")
        return constraint

    def _drake_sp_collision(self, z, sdf, i, sign=1):
        N = self.state_size()
        mu_sp = z[:N]
        L_sp = math_util.vec2tril(z[N:])
        sp = mu_sp + (sign * self.beta * L_sp[i,:])
        # TODO should make indexing more robust, pos not always first 2 indices
        return sdf(sp[:2])
    
    def _drake_x_diff_constraint(self, z):
        '''
        Used only on Dubins since the state doesn't contain velocity, need to limit
        state diff (which is implicitly velocity because there is a fixed dt).
        '''
        N = self.state_size()
        return z[N:] - z[:N]

    def _drake_u_diff_constraint(self, z):
        N = self.act_size()
        return z[N:] - z[:N]


if __name__ == '__main__':
    import os.path as osp
    cfg_fn = osp.join(file_util.dir_of_file(__file__, 1), "config", "di_dynamic_goal.yaml")
    cfg = file_util.load_yaml(cfg_fn)
    
    problem = PlanarNavigationProblem(cfg)

    agent_acts = np.zeros((1000, 2))
    agent_acts[:3,1] = 1.

    # goal_acts = np.zeros((50, 2))
    # goal_acts[:3,0] = 1.

    anim = problem.visualize_test_execution(agent_acts)
    plt.show()
