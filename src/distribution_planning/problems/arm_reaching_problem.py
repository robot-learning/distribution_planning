import sys
import os
import os.path as osp
import numpy as np
import matplotlib.pyplot as plt
import torch
from random import shuffle

from pydrake.autodiffutils import AutoDiffXd, ExtractValue
from pydrake.common.eigen_geometry import Quaternion as DrakeQuaternion
from pydrake.math import RigidTransform

import pybingham
from pybingham import Bingham

from distribution_planning.problems import DrakeProblem
from distribution_planning.environments import DrakeSim
from distribution_planning.util import drake_util, math_util
from distribution_planning.distributions import Gaussian, PoseDistribution, kl_divergence

from ll4ma_opt.problems import Problem
from ll4ma_util import ui_util, file_util


class ArmReachingProblem(Problem):

    def __init__(self, config, horizon=None, headless=False, no_cache=False, obj_data=None):
        self.env = DrakeSim(config, headless=headless, no_cache=no_cache, obj_data=obj_data)
        self.config = self.env.config
        self.no_cache = no_cache
        self.point_based = self.config['point_based']
        self.horizon = horizon if horizon is not None else self.config['horizon']

        self.init_thetas = np.array(self.config['start_thetas'])
        init_tf = self.env.fk(self.init_thetas, 'iiwa_link_7')
        self.init_ee_pos = init_tf.translation()
        self.init_ee_rot = init_tf.rotation().matrix()
        self.init_ee_quat = init_tf.rotation().ToQuaternion().wxyz()

        self.min_thetas = self._get_joints_vector(self.config['min_thetas'])
        self.max_thetas = self._get_joints_vector(self.config['max_thetas'])
        self.min_d_thetas = self._get_joints_vector(self.config['min_d_thetas'])
        self.max_d_thetas = self._get_joints_vector(self.config['max_d_thetas'])
        self.min_ee_pos = np.array(self.config['min_ee_pos'])
        self.max_ee_pos = np.array(self.config['max_ee_pos'])

        self.min_bounds = np.tile(self.min_d_thetas, self.horizon).reshape(-1, 1)
        self.max_bounds = np.tile(self.max_d_thetas, self.horizon).reshape(-1, 1)

        # Faking an init orientation distribution by generating samples
        # about nominal pose and fitting a Bingham to it. Loading cached if exists
        cache_fn = None
        if 'ee_cache_filename' in self.config:
            cache_fn = osp.expanduser(self.config['ee_cache_filename'])
        if cache_fn and osp.exists(cache_fn) and not self.no_cache:
            # print(f"Using cached EE bingham params: {cache_fn}")
            params = file_util.load_pickle(cache_fn)
            self.init_bingham = Bingham(params['v1'], params['v2'], params['v3'],
                                        params['z1'], params['z2'], params['z3'])
        else:
            # print("Generating EE bingham distribution...")
            qs = math_util.random_quats(n_samples=100, dist_threshold=0.2,
                                        origin=self.init_ee_quat)
            self.init_bingham = Bingham()
            self.init_bingham.fit(qs)
            if cache_fn and not self.no_cache:
                params = {
                    'v1': self.init_bingham.V[0],
                    'v2': self.init_bingham.V[1],
                    'v3': self.init_bingham.V[2],
                    'z1': self.init_bingham.Z[0],
                    'z2': self.init_bingham.Z[1],
                    'z3': self.init_bingham.Z[2],
                }
                file_util.save_pickle(params, cache_fn)
                # print(f"EE bingham params cached: {cache_fn}")
        self.init_v1 = DrakeQuaternion(self.init_bingham.V[0])
        self.init_v2 = DrakeQuaternion(self.init_bingham.V[1])
        self.init_v3 = DrakeQuaternion(self.init_bingham.V[2])
        self.init_z1 = self.init_bingham.Z[0]
        self.init_z2 = self.init_bingham.Z[1]
        self.init_z3 = self.init_bingham.Z[2]

        if self.point_based:
            # Find a reachable component
            pG = self.env.objects['sugar'].distribution  # TODO hard-coded
            self.target_pos = None
            self.target_rot = None
            cs = pG.components[:]
            shuffle(cs)
            for c in cs:
                pos = c.gauss.get_mean()
                quat = c.bingham.mode
                if self.env.pose_reachable(pos, quat):
                    self.target_pos = pos
                    self.target_rot = DrakeQuaternion(quat).rotation()
                    print(f"Selected point-based goal: {c.name}")
                    break
            if self.target_pos is None and self.target_rot is None:
                raise ValueError("Could not find reachable component for point-based")

    def batch_cost(self, u):
        B = u.size(0)
        d_thetas = u.view(B, self.horizon, self.act_size())

        costs = torch.zeros(B)
        for i, sample in enumerate(d_thetas):
            thetas, tfs = self.env.get_trajectory(self.init_thetas, sample.numpy())
            tf_T = tfs[-1]
            pos_T = tf_T.translation()
            rot_T = tf_T.rotation().matrix()
            quat_T = tf_T.rotation().ToQuaternion()

            if self.point_based:
                pos_error = np.linalg.norm(self.target_pos - pos_T)
                pos_error *= 3.  # TODO trying to amplify pos
                rot_error = math_util.orientation_error(self.target_rot, rot_T)
                costs[i] = pos_error + rot_error
            else:
                cov = 0.0001 * np.eye(3)  # Just making up a tight cov
                cov = rot_T @ cov @ rot_T.T
                gauss = Gaussian(pos_T, cov)

                v1 = quat_T.multiply(self.init_v1).wxyz()
                v2 = quat_T.multiply(self.init_v2).wxyz()
                v3 = quat_T.multiply(self.init_v3).wxyz()

                bingham = Bingham(v1, v2, v3, self.init_z1, self.init_z2, self.init_z3)
                bingham.compute_stats()
                
                p_T = PoseDistribution(gauss, bingham)
                p_G = self.env.objects['sugar'].distribution
                kl = kl_divergence(p_T, p_G)
                costs[i] = kl

        # TODO trying cost on actions
        act_cost = torch.zeros(B)
        for t in range(1, self.horizon):
            act_cost += (d_thetas[:,t,:] - d_thetas[:,t-1,:]).square().sum(dim=-1)
        costs += act_cost
        
        return costs
        
    def size(self):
        return self.act_size() * self.horizon
        
    def act_size(self):
        return 7  # TODO can make env n joints
    
    def visualize_env(self):
        # TODO
        fig, ax = plt.subplots(1, 1)
        return fig, ax

    def visualize_solution(self, result, dt=0.001):
        thetas = result.thetas
        # d_thetas = result.d_thetas
        # ee_pos = result.ee_pos
        self.env.execute_kinematic_trajectory(thetas, dt)        

    def visualize_start_goal(self, ax):
        # TODO
        pass

    def save_result(self, result, filename):
        save_dir = osp.dirname(filename)
        os.makedirs(save_dir, exist_ok=True)
        data = {}
        data['d_thetas'] = result.iterates[-1].reshape(self.horizon, -1)
        thetas, tfs = self.env.get_trajectory(self.init_thetas, data['d_thetas'])
        data['thetas'] = np.array(thetas)
        data['ee_pos'] = np.array([tf.translation() for tf in tfs])
        data['ee_quat'] = np.array([tf.rotation().ToQuaternion().wxyz() for tf in tfs])
        data['objects'] = {}
        for obj_name, obj in self.env.objects.items():
            data['objects'][obj_name] = {
                'cfg': obj.cfg,
                'link_name': obj.link_name,
                'position': obj.pos,
                'quaternion': obj.quat,
                'distribution_params': obj.distribution._orig_params
            }

            # Determine which component it reached to as min pose distance from EE to component
            mode_dists = {}
            ee_pos = data['ee_pos'][-1]
            ee_rot = tfs[-1].rotation().matrix()
            for c in obj.distribution.components:
                mode_pos = c.gauss.get_mean()
                mode_quat = c.bingham.mode
                mode_tf = RigidTransform(quaternion=DrakeQuaternion(mode_quat), p=mode_pos)
                mode_rot = mode_tf.rotation().matrix()
                mode_dists[c.name] = math_util.pose_error(mode_pos, mode_rot, ee_pos, ee_rot)
                                
            data['objects'][obj_name]['mode_dists'] = mode_dists
            data['objects'][obj_name]['reached'] = min(mode_dists, key=mode_dists.get)
            
        file_util.save_pickle(data, filename)

    def _get_joints_vector(self, val):        
        return np.array(val) if isinstance(val, list) else np.full(self.env.n_joints, val)


class DrakeArmReachingProblem(ArmReachingProblem, DrakeProblem):

    def __init__(self, config, horizon=None):
        ArmReachingProblem.__init__(self, config, horizon)
        DrakeProblem.__init__(self)

        self.target_comp = None
        self.target_pos = None
        self.target_rot = None
        self.target_tf = None
        
    def setup(self):
        # TODO this is hacked for testing
        obj = 'sugar'
        tf_W_O = self.env._get_tf(self.config['objects'][obj])
        self.components = {}
        for name, cfg in self.config['objects'][obj]['components'].items():
            tf_O_C = self.env._get_tf(cfg)
            tf_W_C = tf_W_O.multiply(tf_O_C)
            self.components[name] = {'pos': tf_W_C.translation(),
                                     'rot': tf_W_C.rotation().matrix(),
                                     'quat': tf_W_C.rotation().ToQuaternion(),
                                     'tf': tf_W_C}

        names = list(self.components.keys())
        np.random.shuffle(names)
        target_found = False
        while names:
            self.target_comp = names.pop(0)
            self.target_pos = self.components[self.target_comp]['pos']
            self.target_rot = self.components[self.target_comp]['rot']
            self.target_quat = self.components[self.target_comp]['quat']
            self.target_tf = self.components[self.target_comp]['tf']
            if self.env.ik(self.target_tf, 0.5) is None:
                ui_util.print_warning(f"Component unreachable: {self.target_comp}")
                continue
            else:
                target_found = True
                print("TARGET:", self.target_comp)
                super().setup()
                break
        if not target_found:
            ui_util.print_error("No IK solution could be found for any of the component "
                                "target poses. Maybe object is out of reach?")
        # Reset joints since they would have moved to compute FK
        self.env.set_joint_positions(self.config['start_thetas'])
        
    def create_decision_vars(self):
        self.thetas = self.prog.NewContinuousVariables(7, self.horizon, "thetas")
        self.d_thetas = self.prog.NewContinuousVariables(7, self.horizon-1, "d_thetas")
        self.ee_pos = self.prog.NewContinuousVariables(3, self.horizon, "ee_pos")
        self.ee_quat = self.prog.NewContinuousVariables(4, self.horizon, "ee_quat")
        # self.ee_rot = self.prog.NewContinuousVariables(9, self.horizon, "ee_rot")
        
    def set_initial_guess(self):
        thetas_T = self.env.ik(self.target_tf, 0.5)
        if thetas_T is None:
            ui_util.print_error("IK solution not found, that pose is probably "
                                "not reachable. Exiting without optimizing")
            sys.exit()

        thetas = math_util.interpolate_vectors(self.init_thetas, thetas_T, self.horizon).T
        d_thetas = thetas[:,1:] - thetas[:,:-1]
        pos = []
        # rot = []
        quat = []
        for t in range(self.horizon):
            tf = self.env.fk(thetas[:,t])
            pos.append(tf.translation())
            quat.append(tf.rotation().ToQuaternion().wxyz())
        pos = np.array(pos).T
        quat = np.array(quat).T
        
        self.prog.SetInitialGuess(self.thetas, thetas)
        self.prog.SetInitialGuess(self.d_thetas, d_thetas)
        self.prog.SetInitialGuess(self.ee_pos, pos)
        self.prog.SetInitialGuess(self.ee_quat, quat)
    
    def add_costs(self):
        if self.point_based:
            drake_util.add_cost(self.prog, self._drake_ee_pos_cost,
                                self.ee_pos[:,-1], "EE pos")
            drake_util.add_cost(self.prog, self._drake_ee_rot_cost,
                                self.ee_quat[:,-1], "EE rot")
        else:
            drake_util.add_cost(self.prog, self._drake_kl_cost,
                                np.concatenate([self.ee_pos[:,-1], self.ee_quat[:,-1]]),
                                "EE KL")
            
    def add_constraints(self):
        drake_util.add_bb_constraint(self.prog, self.init_thetas, self.init_thetas,
                                     self.thetas[:,0], f"thetas_0 EQ init")
        drake_util.add_bb_constraint(self.prog, self.init_ee_pos, self.init_ee_pos,
                                     self.ee_pos[:,0], f"ee_pos_0 EQ init")
        drake_util.add_bb_constraint(self.prog, self.init_ee_quat.flatten(),
                                     self.init_ee_quat.flatten(),
                                     self.ee_quat[:,0], f"ee_quat_0 EQ init")                    
        for t in range(self.horizon):
            drake_util.add_bb_constraint(self.prog, self.min_thetas, self.max_thetas,
                                         self.thetas[:,t], f"BB thetas_{t}")

            drake_util.add_bb_constraint(self.prog, self.min_ee_pos, self.max_ee_pos,
                                         self.ee_pos[:,t], f"BB ee_pos_{t}")
            drake_util.add_bb_constraint(self.prog, np.full(4, -1.), np.full(4, 1.),
                                         self.ee_quat[:,t], f"BB ee_quat_{t}")
            drake_util.add_constraint(self.prog, self._drake_quat_constraint, [1.], [1.],
                                      self.ee_quat[:,t], f"EE unit quat t={t}")
            # drake_util.add_constraint(self.prog, self._drake_ee_rot_constraint,
            #                           np.zeros(9), np.zeros(9), self.ee_rot[:,t],
            #                           f"EE rot valid rotation t={t}")
        for t in range(self.horizon-1):
            drake_util.add_bb_constraint(self.prog, self.min_d_thetas, self.max_d_thetas,
                                         self.d_thetas[:,t], f"BB d_thetas_{t}")
            M = self.env.n_joints + 3 + 4  # N joints + 3D pos + 4D quat
            drake_util.add_constraint(
                self.prog, self._drake_dynamics_constraint, np.zeros(M), np.zeros(M),
                np.concatenate([self.thetas[:,t], self.ee_pos[:,t], self.ee_quat[:,t],
                                self.thetas[:,t+1], self.ee_pos[:,t+1], self.ee_quat[:,t+1],
                                self.d_thetas[:,t]]),
                f"DYN from t={t} to t={t+1}")
                
    def get_ll4ma_result(self, result, xs):
        ll4ma_result = super().get_ll4ma_result(result, xs)
        ll4ma_result.thetas = drake_util.get_decision_var(self.prog, xs[-1], self.thetas).T
        ll4ma_result.d_thetas = drake_util.get_decision_var(self.prog, xs[-1], self.d_thetas).T
        ll4ma_result.ee_pos = drake_util.get_decision_var(self.prog, xs[-1], self.ee_pos).T
        ll4ma_result.ee_quat = drake_util.get_decision_var(self.prog, xs[-1], self.ee_quat).T
        return ll4ma_result
    
    def _drake_ee_pos_cost(self, z):
        return np.linalg.norm(self.target_pos - z)

    def _drake_ee_rot_cost(self, z):
        # TODO I think this is approximate error and you should look at maybe the CDMP
        # paper to get full error
        z /= np.linalg.norm(z)
        q = DrakeQuaternion(ExtractValue(z))
        q_c = q.conjugate()
        diff = self.target_quat.multiply(q_c)
        error = AutoDiffXd(np.square(diff.xyz()).sum())
        return error

    def _drake_quat_constraint(self, z):
        return [np.linalg.norm(z)]
    
    def _drake_kl_cost(self, z):
        pos = z[:3]
        cov = 0.0001 * np.eye(3)  # Just making up a tight cov
        gauss = Gaussian(pos, cov)


        # TODO I think in general you want relative quat from init
        
        quat = ExtractValue(z[3:]).squeeze()
        quat /= np.linalg.norm(quat)
        quat = DrakeQuaternion(quat)
        # quat = Quaternion(np.array([0.,0.,1.,0.]))

        # TODO which multiply direction do you need?
        v1 = self.init_v1.multiply(quat).wxyz()
        v2 = self.init_v2.multiply(quat).wxyz()
        v3 = self.init_v3.multiply(quat).wxyz()
        bingham = Bingham(v1, v2, v3, self.init_z1, self.init_z2, self.init_z3)
        bingham.compute_stats()
                
        p_T = PoseDistribution(gauss, bingham)
        p_G = self.env.objects['sugar'].distribution
        kl = kl_divergence(p_T, p_G)
        
        return kl
            
    # def _drake_ee_rot_constraint(self, z):
    #     R = z.reshape((3,3))
    #     return (R.T @ R).flatten() - np.eye(3).flatten()
    
    def _drake_dynamics_constraint(self, z):
        n_joints = self.env.n_joints
        M = n_joints + 3 + 4  # N joints + 3D pos + 4D quat
        x1 = z[:M]
        x2 = z[M:2*M]
        d_thetas = z[2*M:]
        thetas = x1[:n_joints]
        next_thetas = thetas + d_thetas
        if isinstance(z[0], AutoDiffXd):
            next_pose = self.env.fk_ad(next_thetas)
        else:
            next_pose = self.env.fk(next_thetas)
        next_pos = next_pose.translation()
        next_quat = next_pose.rotation().ToQuaternion().wxyz()
        next_x = np.concatenate([next_thetas, next_pos, next_quat])
        return next_x - x2
