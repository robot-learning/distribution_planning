import sys
import torch

from ll4ma_opt.solvers.samplers import GaussianSampler


class ArmReachingSampler(GaussianSampler):

    def __init__(self, problem, start_iso_var=None, end_iso_var=None, n_steps=100):
        super().__init__(problem, start_iso_var=start_iso_var, end_iso_var=end_iso_var,
                         n_steps=n_steps)

    def sample(self, n_samples):
        samples = super().sample(n_samples)
        samples = samples.view(n_samples, self.problem.horizon, self.problem.act_size())

        # Want to ensure the delta joints don't push the arm outside
        # its joint limits when they're accumulated
        upper = torch.tensor(self.problem.max_thetas).view(1, -1).repeat(n_samples, 1)
        lower = torch.tensor(self.problem.min_thetas).view(1, -1).repeat(n_samples, 1)
        thetas = torch.tensor(self.problem.init_thetas).view(1, -1).repeat(n_samples, 1)
        zeros = torch.zeros(n_samples, self.problem.act_size()).double()
        for t in range(self.problem.horizon):
            thetas += samples[:,t,:]
            # TODO for now just zeroing out deltas that push it over the limit. You could
            # take a different strategy and limit diff so it goes right up against limit
            # but I'm not convinced that will give you very different solutions
            samples[:,t,:] = samples[:,t,:].where(thetas < upper, zeros)
            samples[:,t,:] = samples[:,t,:].where(thetas > lower, zeros)
            
        samples = samples.view(n_samples, self.problem.horizon * self.problem.act_size())

        return samples
        
