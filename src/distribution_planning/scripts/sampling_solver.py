#!/usr/bin/env python
import os
import os.path as osp
import sys
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, FFMpegWriter
# TODO ideally you can use conda ffmpeg but codecs for MP4 saving weren't working
# plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'
from copy import deepcopy
from tqdm import tqdm

from distribution_planning.problems import PlanarNavigationProblem, MiniGolfProblem
from distribution_planning.util import vis_util, math_util
from distribution_planning.distributions import kl_divergence, Gaussian

import torch
import warnings; warnings.filterwarnings(action='ignore', module='ll4ma_opt') # Ignore pybullet warn
from ll4ma_opt.solvers import MPPI, CEM
from ll4ma_opt.solvers.samplers import GaussianSampler

from ll4ma_util import file_util, ui_util


CONFIG_ROOT = osp.join(osp.dirname(osp.dirname(osp.abspath(__file__))), "config")

            
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_root', type=str, default=CONFIG_ROOT)
    parser.add_argument('--config', type=str, default='gaussian_example.yaml')
    parser.add_argument('--save_dir', type=str, default='~/tro_results')
    parser.add_argument('--solver', type=str, default='mppi', choices=['mppi', 'cem'])
    parser.add_argument('--horizon', type=int, default=None)
    parser.add_argument('--max_iters', type=int, default=10)
    parser.add_argument('--n_samples', type=int, default=100)
    parser.add_argument('--n_elite', type=int, default=10,
                        help="Only used in CEM, number of elite samples to fit to")
    parser.add_argument('--iso_var', type=float, default=0.5,
                        help="Used in both CEM and MPPI, isotropic variance for sampler")
    parser.add_argument('--end_iso_var', type=float, default=None,
                        help="Only used in MPPI. Isotropic variance attenuates to this value")
    parser.add_argument('--alpha', type=float, default=0.001,
                        help="Only used in MPPI, factor on exponentiated cost")
    parser.add_argument('--beta', type=float, default=2.)
    parser.add_argument('--goal_cost', type=str, default='ce_iproj',
                        choices=['kl_iproj', 'kl_mproj', 'ce_iproj', 'ce_mproj',
                                 'mean_l2', 'log_goal_pdf', 'log_terminal_pdf'])
    parser.add_argument('--mpc', action='store_true')
    parser.add_argument('--mpc_iters', type=int, default=40)
    parser.add_argument('--test_env', action='store_true')
    parser.add_argument('--vis_optimization', action='store_true')
    parser.add_argument('--seed', type=int, default=None)
    parser.add_argument('--no_show', action='store_true')
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
    
    save_dir = osp.expanduser(args.save_dir)
    img_dir = osp.join(save_dir, "imgs", "dynamic_goal")
    video_dir = osp.join(save_dir, "videos", "dynamic_goal")
    data_dir = osp.join(save_dir, "data", "dynamic_goal")
    config_fn = osp.join(args.config_root, args.config)
    file_util.check_path_exists(config_fn, "YAML config file")
    file_util.create_dir(img_dir)
    file_util.create_dir(video_dir)
    file_util.create_dir(data_dir)
    save_fn = args.config.replace('.yaml', '')
    config = file_util.load_yaml(config_fn)
    horizon = args.horizon if args.horizon is not None else config['horizon']
    
    if config['problem'] == 'planar_nav':
        problem = PlanarNavigationProblem(config, horizon, args.beta, args.goal_cost)
    elif config['problem'] == 'minigolf':
        problem = MiniGolfProblem(config, horizon, args.beta, args.goal_cost)
    else:
        raise ValueError(f"Unknown problem type: {config['problem']}")
    
    if args.solver == 'cem':
        sampler = GaussianSampler(problem, start_iso_var=args.iso_var, n_steps=args.max_iters)
        solver = CEM(problem, sampler, args.n_samples, args.n_elite)
    elif args.solver == 'mppi':
        sampler = GaussianSampler(problem, start_iso_var=args.iso_var,
                                  end_iso_var=args.end_iso_var, n_steps=args.max_iters)
        solver = MPPI(problem, sampler, n_samples=args.n_samples, alpha=args.alpha)
    else:
        raise ValueError(f"Unknown solver type: {args.solver}")

    if args.test_env:
        fig, ax = problem.visualize_env()
        problem.visualize_start_goal(ax)
        plt.show()
        sys.exit()
    
    if args.mpc:
        x0 = None
        results = []
        # TODO trying this
        min_horizon = 3
        max_horizon = 25
        
        for mpc_iter in tqdm(range(args.mpc_iters), desc='MPC'):
            if problem.projected_pG is not None:
                current = Gaussian(problem.p0.get_mean()[:2], problem.p0.get_cov()[:2,:2])
                kl = kl_divergence(current, problem.projected_pG)
                current_horizon = int((kl / 600.) * max_horizon) # TODO can make more robust
                # print("HORIZON", current_horizon, "KL", kl)
            else:
                current_horizon = max_horizon            
            current_horizon = max(current_horizon, min_horizon)
            current_horizon = min(current_horizon, max_horizon)

            # # Shift action distribution to initialize as previously optimized distribution
            # if len(results) > 0:
            #     x0 = problem.unflatten_act_seq(results[-1].iterates[-1])
            #     x0[:-1,:] = x0[1:,:]  # Shift one back in time
            #     x0[-1,:] = x0[-2,:]   # Duplicate last action
            #     x0 = x0.flatten()

            solver.reset()

            # TODO trying receding horizon
            # if len(results) > 0 and current_horizon != problem.horizon:
            #     x0 = problem.unflatten_act_seq(results[-1].iterates[-1])
            #     x0 = x0[:-1,:]
            #     x0 = x0.flatten()
            x0 = None
            
            problem.set_horizon(current_horizon)
            
            problem.env.step_goal()
            problem.estimate_state()
            problem.estimate_goal()

            # TODO re-creating because of receding horizon
            sampler = GaussianSampler(problem, start_iso_var=args.iso_var,
                                      end_iso_var=args.end_iso_var, n_steps=args.max_iters)
            solver = MPPI(problem, sampler, n_samples=args.n_samples, alpha=args.alpha)
            
            result = solver.optimize(x0, args.max_iters, verbose=False)
            result.p0 = deepcopy(problem.p0)
            result.pG = deepcopy(problem.pG)
            result.horizon = current_horizon
            result.states = np.array(problem.env.states)
            result.goal_states = np.array(problem.env.goal_states)
            if hasattr(problem, 'projected_pG'):
                result.projected_pG = deepcopy(problem.projected_pG)
            results.append(result)
            
            # Execute the action in the environment (first act in last mean traj)
            u = result.iterates[-1].reshape(current_horizon, -1)[0,:]
            problem.env.apply_action(u)
            
            # TODO add convergence check
        ui_util.print_happy(f"\n  MPC run with {args.solver} solver complete\n")

        save_prefix = f'{save_fn}_{args.solver}_{args.goal_cost}_mpc'

        file_util.save_pickle(results, osp.join(data_dir, f'{save_prefix}.pickle'))
        
        print("Creating visualization...")
        anim = problem.visualize_mpc(results, problem)        
        anim.save(osp.join(video_dir, f'{save_prefix}.mp4'))
        print("Visualization complete")
        if not args.no_show:
            plt.show()
    else:
        problem.estimate_state()
        problem.estimate_goal()

        x0 = problem.get_initial_guess()
        
        result = solver.optimize(x0, args.max_iters)
        result.display(show_solution=False)

        save_prefix = f'{save_fn}_{args.solver}_{args.goal_cost}'

        if args.vis_optimization:
            anim = problem.visualize_sampling_optimization(result)
            anim.save(osp.join(save_dir, f'{save_prefix}.mp4'))

        fig, ax = problem.visualize_env()
        problem.visualize_goal(ax)
        problem.visualize_solution(ax, result)
        plt.savefig(osp.join(save_dir, f'{save_prefix}.pdf'), bbox_inches='tight')
        if not args.no_show:
            plt.show()

