#!/usr/bin/env python
import os.path as osp
import argparse
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from distribution_planning.distributions import Gaussian


if __name__ == '__main__':
    """
    This script creates the upper row of the cover figure for the paper. Note this only
    generates the base PNG, and then I import that image into Google Drawings to overlay
    some text and icons and such.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--img_dir', type=str, default="~/tro_results/imgs")
    parser.add_argument('--save_fn', type=str, default="cover_dynamic_goal.png")
    parser.add_argument('--no_save', action='store_true')
    args = parser.parse_args()

    fig_dims = (12, 6)
    ax_l_dims = (4, 6)
    ax_r_dims = (8, 6)
    mean_s = 20
    n_std = 5
    
    goal_cmap = 'pink_r'
    goal_pred_cmap = 'binary' # 'inferno_r'
    agent_cmap = 'bone_r'
    agent_pred_cmap = 'cividis'
    ax_l_fc = 'gainsboro'
    ax_r_fc = 'xkcd:prussian blue'
    ax_r_alpha = 0.5

    goal_mus = np.array([
        [1., 5.5],
        [2.2, 5.5],
        [3.5, 5.4]
    ])
    goal_covs = np.array([
        [[0.05, 0.0], [0.0, 0.02]],
        [[0.05, -0.006], [-0.006, 0.015]],
        [[0.04, -0.006], [-0.006, 0.01]]
    ])
    goal_pred_mus = np.array([
        [3.5,  5.4],
        [4.5,  5.25],
        [5.5,  4.95],
        [6.5,  4.65],
        [7.5,  4.3],
        [8.5,  3.9],
        [9.5,  3.5],
        [10.5, 3.0],
    ])
    goal_pred_covs = np.array([
        [[0.04, -0.006], [-0.006, 0.01]],
        [[0.05, -0.006], [-0.006, 0.012]],
        [[0.05, -0.006], [-0.006, 0.02]],
        [[0.05, -0.006], [-0.006, 0.03]],
        [[0.06, -0.006], [-0.006, 0.04]],
        [[0.07, -0.006], [-0.006, 0.05]],
        [[0.08, -0.006], [-0.006, 0.06]],
        [[0.09, -0.006], [-0.006, 0.07]],
    ])
    
    
    agent_mus = np.array([
        [0.5, 0.5],
        [2., 0.55],
        [3.5, 0.65]
    ])
    agent_covs = np.array([
        [[0.008, 0.0], [0.0, 0.008]],
        [[0.008, 0.0], [0.0, 0.008]],
        [[0.008, 0.0], [0.0, 0.008]]        
    ])
    agent_pred_mus = np.array([
        [3.6,  0.7],
        [4.5,  0.8],
        [5.5,  0.9],
        [6.5,  1.05],
        [7.5,  1.3],
        [8.5,  1.65],
        [9.5,  2.2],
        [10.5, 3.0],
    ])
    agent_pred_covs = np.array([
        [[0.010, 0.000], [0.000, 0.010]],
        [[0.010, 0.000], [0.000, 0.010]],
        [[0.015, 0.001], [0.001, 0.015]],
        [[0.020, 0.002], [0.002, 0.020]],
        [[0.025, 0.002], [0.002, 0.025]],
        [[0.030, 0.003], [0.003, 0.030]],
        [[0.035, 0.003], [0.003, 0.035]],
        [[0.040, 0.004], [0.004, 0.040]],
    ])


    
    gs = gridspec.GridSpec(1, 2, width_ratios=[1,2])
        
    fig = plt.figure()
    fig.set_size_inches(*fig_dims)
    ax_l = plt.subplot(gs[0])
    ax_r = plt.subplot(gs[1])

    ax_l.patch.set_facecolor(ax_l_fc)
    ax_r.patch.set_facecolor(ax_r_fc)
    ax_r.patch.set_alpha(ax_r_alpha)
    ax_l.set_xlim(0, ax_l_dims[0])
    ax_l.set_ylim(0, ax_l_dims[1])
    ax_r.set_xlim(ax_l_dims[0], ax_l_dims[0] + ax_r_dims[0])
    ax_r.set_ylim(0, ax_r_dims[1])

    for t in range(len(goal_mus)):
        gauss = Gaussian(goal_mus[t], goal_covs[t])
        gauss.draw(ax_l, mean_s=mean_s, cmap=goal_cmap, n_std=n_std, mean_c='whitesmoke')

    for t in range(len(agent_mus)):
        gauss = Gaussian(agent_mus[t], agent_covs[t])
        gauss.draw(ax_l, mean_s=mean_s, cmap=agent_cmap, n_std=n_std, mean_c='white')

    for goal_mu, agent_mu in zip(goal_mus, agent_mus):
        ax_l.plot([agent_mu[0], goal_mu[0]], [agent_mu[1], goal_mu[1]],
                  ls=':', lw=4, c='slategray')

    for t in range(len(goal_pred_mus)-1):
        gauss = Gaussian(goal_pred_mus[t], goal_pred_covs[t])
        gauss.draw(ax_r, mean_s=-1, cmap=goal_pred_cmap, n_std=5, show_only_std=5, ec=None)
    goal_gauss = Gaussian(goal_pred_mus[-1], goal_pred_covs[-1])
    goal_gauss.draw(ax_r, mean_s=-1, n_std=5, cmap=goal_pred_cmap, ec='darkslategray')

    for t in range(len(agent_pred_mus)-1):
        gauss = Gaussian(agent_pred_mus[t], agent_pred_covs[t])
        gauss.draw(ax_r, mean_s=-1, cmap=agent_pred_cmap, n_std=5, show_only_std=5, ec=None)
    agent_gauss = Gaussian(agent_pred_mus[-1], agent_pred_covs[-1])
    agent_gauss.draw(ax_r, mean_s=70, n_std=5, cmap=agent_pred_cmap, ec='silver',
                     mean_c='darkgoldenrod')
        
        

    
        
    
    plt.tight_layout()
    ax_l.tick_params(axis='both', which='both', bottom=False, left=False,
                     labelbottom=False, labelleft=False)
    ax_r.tick_params(axis='both', which='both', bottom=False, left=False,
                     labelbottom=False, labelleft=False)
    plt.subplots_adjust(wspace=0)
    if not args.no_save:
        plt.savefig(osp.expanduser(osp.join(args.img_dir, args.save_fn)), dpi=2000,
                    bbox_inches='tight')
    plt.show()
        
