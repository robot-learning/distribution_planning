#!/usr/bin/env python
import os.path as osp
import argparse
import matplotlib.pyplot as plt

from ll4ma_util import file_util, ui_util

from distribution_planning.problems import PlanarNavigationProblem

CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")


def get_items(res, problem):
    for i, (iterate, sample_iterates) in enumerate(zip(res.iterates, res.sample_iterates)):
        # iterate = self.unflatten_act_seq(iterate)
        iterate = iterate.reshape(res.horizon, problem.act_size())
        sample_iterates = sample_iterates.reshape(-1, res.horizon, problem.act_size())
        mean_traj = problem.get_deterministic_trajectory(res.p0.get_mean(), iterate)
        sample_trajs = [problem.get_deterministic_trajectory(res.p0.get_mean(), s)
                        for s in sample_iterates]
        proj_pG = res.projected_pG if hasattr(res, 'projected_pG') else None
    return mean_traj, sample_trajs, res.p0, res.pG, proj_pG


if __name__ == '__main__':
    """
    This script creates the dynamic goal figure in the experiments section.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default="~/tro_results/data/dynamic_goal")
    parser.add_argument('--data_fn', type=str, default="di_dynamic_goal_mppi_ce_iproj_mpc.pickle")
    parser.add_argument('--save_dir', type=str,
                        default="~/multisensory_learning_paper/goal_distribution_planning/imgs")
    parser.add_argument('--idxs', type=int, nargs='+', default=[0, 35, 69])
    args = parser.parse_args()

    data_dir = osp.expanduser(args.data_dir)
    save_dir = osp.expanduser(args.save_dir)
    data_fn = osp.join(data_dir, args.data_fn)
    cfg_fn = osp.join(CONFIG_ROOT, "di_dynamic_goal.yaml")
    file_util.check_path_exists(data_fn, "Data file")
    file_util.check_path_exists(cfg_fn, "YAML config")
    file_util.create_dir(save_dir)
    
    results = file_util.load_pickle(data_fn)
    cfg = file_util.load_yaml(cfg_fn)
    
    for idx in args.idxs:
        if idx > len(results) - 1:
            ui_util.print_error_exit(f"Invalid idx: {idx}. Must be in range [0,{len(results)})")

    problem = PlanarNavigationProblem(cfg)
    
    min_xy = cfg['min_x'][:2]
    max_xy = cfg['max_x'][:2]

    lw = 2
    ms = 3

    n_per_row = 3
    n_rows = 2
    fig_size_factor = 0.25
    fig_width = n_per_row * max_xy[0] * fig_size_factor
    fig_height = n_rows * max_xy[1] * fig_size_factor
    
    fig, axes = plt.subplots(n_rows, n_per_row)
    fig.set_size_inches(fig_width, fig_height)

    print("AXES", axes.shape)
    axes = axes.flatten()
    for ax_idx, idx in enumerate(args.idxs):
        ax = axes[ax_idx]
        ax.set_xlim(min_xy[0], max_xy[0])
        ax.set_ylim(min_xy[1], max_xy[1])
        ax.tick_params(axis='both', which='both', bottom=False, left=False,
                       labelbottom=False, labelleft=False)
        
        problem.env.draw_obstacles(ax)

        res = results[idx]
        mean_traj, sample_trajs, p0, pG, proj_pG = get_items(res, problem)

        proj_pG.draw(ax, [0,1], mean_s=10, cmap='Greens')
        p0.draw(ax, [0,1], mean_s=-1)
        pG.draw(ax, [0,1], mean_s=10)
        
        
        ax.plot(res.goal_states[:,0], res.goal_states[:,1], lw=lw,
                color='dodgerblue', marker='o', ms=ms)
        ax.plot(res.states[:,0], res.states[:,1], lw=lw,
                color='indianred', marker='o', ms=ms)
        ax.plot(mean_traj[:,0], mean_traj[:,1], lw=2, color='slategrey', ls=':')
        ax.text(10.5, 0.2, f't = {idx}', fontsize=18)

        
    plt.tight_layout()
    plt.subplots_adjust(wspace=0, hspace=0)
    plt.savefig(osp.join(save_dir, "dynamic_goal.pdf"))
    plt.show()
