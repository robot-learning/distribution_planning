#!/usr/bin/env python
import os.path as osp
import argparse
import numpy as np
import matplotlib.pyplot as plt
import random
import torch
from tqdm import tqdm

from distribution_planning.problems import MiniGolfProblem

from ll4ma_opt.solvers import CEM
from ll4ma_opt.solvers.samplers import GaussianSampler

from ll4ma_util import file_util, ui_util


CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")


if __name__ == '__main__':
    """
    This script generates all the plans for the minigolf (ball-rolling) environment.
    This is done separately from creating the figure since this can take a long
    time to run.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default='minigolf_noise_amplifier.yaml')
    parser.add_argument('--data_dir', type=str,
                        default='~/tro_results/data/minigolf')
    parser.add_argument('--img_dir', type=str,
                        default='~/tro_results/imgs/minigolf')
    parser.add_argument('--horizon', type=int, default=100)
    parser.add_argument('--max_iters', type=int, default=50)
    parser.add_argument('--n_samples', type=int, default=500)
    parser.add_argument('--std', type=float, default=0.8)
    parser.add_argument('--n_elite', type=int, default=20)
    parser.add_argument('--beta', type=float, default=2.)
    parser.add_argument('--seed', type=int, default=3)
    parser.add_argument('--show', action='store_true')
    parser.add_argument('--goal_costs', type=str, nargs='+', 
                        default=['kl_iproj', 'kl_mproj', 'ce_iproj'])
    parser.add_argument('--n_point_plans', type=int, default=50)
    args = parser.parse_args()

    data_dir = osp.expanduser(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)
    file_util.safe_create_dir(data_dir)
    file_util.safe_create_dir(img_dir)
    
    cfg_fn = osp.join(CONFIG_ROOT, args.config)
    file_util.check_path_exists(cfg_fn, "YAML config file")
    cfg = file_util.load_yaml(cfg_fn)
    
    save_prefix = args.config.replace('.yaml', '')
    
    for goal_cost in args.goal_costs:
        print(f"Solving for cost {goal_cost}...")

        save_prefix_i = f"{save_prefix}_{goal_cost}"
        
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        
        problem = MiniGolfProblem(cfg, args.horizon, args.beta, goal_cost)
        sigma0 = np.eye(problem.size()) * args.std
        sampler = GaussianSampler(problem, sigma=sigma0)
        solver = CEM(problem, sampler, args.n_samples, args.n_elite)

        x0 = problem.get_initial_guess()
        result = solver.optimize(x0, args.max_iters)
        problem.save_data(result, args, osp.join(data_dir, f"{save_prefix_i}.pickle"))
               
        # anim = problem.visualize_sampling_optimization(result)
        # anim.save(osp.join(img_dir, f'{save_prefix_i}.mp4')) 

        fig, ax = problem.visualize_env()
        problem.visualize_goal(ax)
        problem.visualize_solution(ax, result)
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.pdf'), bbox_inches='tight')
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.png'), bbox_inches='tight')
        if args.show:
            plt.show()
        plt.close('all')

    pG = problem.pG
        
    # Generate samples from goal distribution and plan to each point with log PDF of terminal
    print(f"Solving for {args.n_point_plans} samples from goal "
          f"distribution using logPDF of terminal...")
    for i in tqdm(range(args.n_point_plans)):
        save_prefix_i = f"{save_prefix}_log-terminal-pdf_sample_{i+1}"

        cfg['deterministic'] = False
        cfg['goal_point'] = pG.sample()
        problem = MiniGolfProblem(cfg, args.horizon, args.beta, goal_cost)
        
        sampler = GaussianSampler(problem, start_iso_var=args.std)
        solver = CEM(problem, sampler, 500, args.n_elite)

        x0 = problem.get_initial_guess()
        result = solver.optimize(x0, 20, verbose=False)

        problem.save_data(result, args, osp.join(data_dir, f"{save_prefix_i}.pickle"))
               
        fig, ax = problem.visualize_env()
        problem.visualize_goal(ax)
        problem.visualize_solution(ax, result)
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.pdf'), bbox_inches='tight')
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.png'), bbox_inches='tight')
        plt.close('all')

        
        # Do deterministic
        save_prefix_i = f"{save_prefix}_det_sample_{i+1}"
        cfg['deterministic'] = True
        problem = MiniGolfProblem(cfg, args.horizon, args.beta, goal_cost)
        
        sampler = GaussianSampler(problem, start_iso_var=args.std)
        solver = CEM(problem, sampler, 500, args.n_elite)

        x0 = problem.get_initial_guess()
        result = solver.optimize(x0, 20, verbose=False)

        problem.save_data(result, args, osp.join(data_dir, f"{save_prefix_i}.pickle"))
               
        fig, ax = problem.visualize_env()
        problem.visualize_goal(ax)
        problem.visualize_solution(ax, result)
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.pdf'), bbox_inches='tight')
        plt.savefig(osp.join(img_dir, f'{save_prefix_i}.png'), bbox_inches='tight')
        plt.close('all')


    ui_util.print_happy("\n  Data collection complete for minigolf\n")
