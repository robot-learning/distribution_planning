#!/usr/bin/env python
import sys
import os.path as osp
import argparse
import torch
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from tqdm import tqdm

from distribution_planning.distributions import Gaussian, kl_divergence
from distribution_planning.problems import MiniGolfProblem

from ll4ma_util import file_util, ui_util


def plot_paths(fns, img_fn, video_fn, pG=None, no_save_img=False, no_save_video=False,
               path_color='darkslategray'):
    cfg = file_util.load_pickle(fns[0])['cfg']
    cfg['terrain_cmap'] = 'binary'
    problem = MiniGolfProblem(cfg)
    fig, ax = problem.visualize_env()

    if pG is not None:
        pG.draw(ax, [0,1])
    
    pts = None
    rollouts = []
    for fn in tqdm(fns):        
        data = file_util.load_pickle(fn)
        result = data['result']
        cfg = data['cfg']
        cfg['terrain_cmap'] = 'binary'
        problem = MiniGolfProblem(cfg)

        x = torch.tensor(result.iterates[-1]).squeeze().unsqueeze(0)

        # Get noisy rollouts to see the actual terminal distribution
        x0 = x.repeat(10, 1)
        xs = problem.env.rollout(x0, problem.horizon, use_noise=True)
        rollouts.append(xs)
        pts = xs[:,-1,:] if pts is None else torch.cat([pts, xs[:,-1,:]], dim=0)

        # Do deterministic rollout to see the planned path
        xs = problem.env.rollout(x, problem.horizon, use_noise=False).squeeze()
        path = xs[:,:2]
        plt.plot(path[:,0], path[:,1], lw=2, alpha=0.8, color=path_color)

        goal = problem.pG.get_mean()
        plt.scatter(goal[0], goal[1], color='dodgerblue', zorder=2000, s=100, alpha=0.8)

        problem.p0.set_mean(xs[0,:].numpy())
        problem.visualize_start(ax, s=200)
        
    gauss = Gaussian()
    gauss.fit(pts.numpy())
    gauss.draw(ax, [0,1], cmap='Greys')

    print("  KL", kl_divergence(gauss, pG))
    if not no_save_img:
        plt.savefig(img_fn, bbox_inches='tight')
    plt.show()

    # # Need to get original cfg so goal distribution looks correct
    # fn = osp.join(data_dir, 'minigolf_noise_amplifier_ce_iproj.pickle')
    # cfg = file_util.load_pickle(fn)['cfg']
    # cfg['terrain_cmap'] = 'binary'
    problem.pG = pG
    # problem = MiniGolfProblem(cfg)
    anim = animate_rollouts(torch.cat(rollouts, dim=0), problem)
    if not no_save_video:
        anim.save(video_fn)
    plt.show()


def vis_result(fn, img_fn, video_fn, no_save_img, no_save_video):
    data = file_util.load_pickle(fn)
    result = data['result']
    cfg = data['cfg']
    cfg['terrain_cmap'] = 'binary'
    problem = MiniGolfProblem(cfg)
    
    fig, ax = problem.visualize_env()
    problem.visualize_goal(ax)
    xs = problem.visualize_solution(ax, result, n_rollouts=500)
    if not no_save_img:
        plt.savefig(img_fn, bbox_inches='tight')
    plt.show()

    # Animation
    anim = animate_rollouts(xs, problem)
    if not no_save_video:
        anim.save(video_fn)
    plt.show()


def animate_rollouts(xs, problem):
    fig, ax = problem.visualize_env()
    problem.visualize_goal(ax)
    paths = [ax.plot([], [], lw=1, color='indigo', alpha=0.05)[0]
             for _ in range(len(xs))]
    objs = ax.scatter([], [], s=100, color='indigo', alpha=0.4)

    n_pts = 1000
    old_T = np.linspace(0, 1, xs.size(1))
    new_T = np.linspace(0, 1, n_pts)
    new_x = np.stack([np.interp(new_T, old_T, xs[i,:,0]) for i in range(xs.size(0))])
    new_y = np.stack([np.interp(new_T, old_T, xs[i,:,1]) for i in range(xs.size(0))])
    # Repeating end states so you get a stable image to pause on, otherwise there's
    # some jitter in the dynamics due to low-velocity states continuing to be simulated
    freeze_idx = 900
    new_x[:,freeze_idx:] = np.expand_dims(new_x[:,freeze_idx], -1)
    new_y[:,freeze_idx:] = np.expand_dims(new_y[:,freeze_idx], -1)
    new_xy = np.stack([new_x, new_y], axis=-1)
    
    def animate(i):
        objs.set_offsets(new_xy[:,i])
        for path_idx, path in enumerate(paths):
            path.set_data(new_x[path_idx,:i+1], new_y[path_idx,:i+1])
        # if i >= new_x.shape[-1] - 20:
        #     gauss = Gaussian()
        #     gauss.fit(xs[:,-1,:2].numpy())
        #     g_artists = list(gauss.draw(ax, [0,1], cmap='Greys', alpha=0.5).values())
        # else:
        #     g_artists = []
            
        return [objs] + paths # + g_artists

    anim = FuncAnimation(fig, animate, frames=new_x.shape[-1], interval=10.,
                         blit=True, repeat_delay=6000)
    return anim
    

if __name__ == '__main__':
    """
    This script creates the images for the paper for the ball-rolling experiments. It
    also creates the animations that go into the paper video. You can disable saving
    either of these with the --no_save_img or --no_save_video flag.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str,
                        default='~/tro_results/data/minigolf')
    parser.add_argument('--img_dir', type=str,
                        default="~/multisensory_learning_paper/goal_distribution_planning/imgs")
    parser.add_argument('--video_dir', type=str,
                        default='~/tro_results/videos/minigolf')
    parser.add_argument('-e', '--examples', type=str, nargs='+',
                        default=['log_pdf', 'det', 'ce_iproj', 'kl_iproj'],
                        choices=['log_pdf', 'det', 'ce_iproj', 'kl_iproj'])
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--no_save_img', action='store_true')
    parser.add_argument('--no_save_video', action='store_true')
    args = parser.parse_args()

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    data_dir = osp.expanduser(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)
    video_dir = osp.expanduser(args.video_dir)
    
    file_util.check_path_exists(data_dir)
    file_util.create_dir(img_dir)
    file_util.create_dir(video_dir)

    fns = file_util.list_dir(data_dir, '.pickle')
    pdf_fns = [fn for fn in fns if 'sample' in fn and 'terminal' in fn]
    det_fns = [fn for fn in fns if 'sample' in fn and 'det' in fn]

    ce_iproj_fn = osp.join(data_dir, 'minigolf_noise_amplifier_ce_iproj.pickle')
    kl_iproj_fn = osp.join(data_dir, 'minigolf_noise_amplifier_kl_iproj.pickle')
    
    data = file_util.load_pickle(ce_iproj_fn)
    problem = MiniGolfProblem(data['cfg'])
    pG = problem.pG

    if 'log_pdf' in args.examples:
        print("Log terminal PDF")
        img_fn = osp.join(img_dir, 'minigolf_log_terminal.pdf')
        video_fn = osp.join(video_dir, 'minigolf_log_terminal.mp4')
        plot_paths(pdf_fns, img_fn, video_fn, pG, args.no_save_img, args.no_save_video)

    if 'det' in args.examples:
        print("Determinisitic")
        img_fn = osp.join(img_dir, 'minigolf_det.pdf')
        video_fn = osp.join(video_dir, 'minigolf_det.mp4')
        plot_paths(det_fns, img_fn, video_fn, pG, args.no_save_img, args.no_save_video)

    if 'ce_iproj' in args.examples:
        print("CE iproj")
        img_fn = osp.join(img_dir, 'minigolf_ce_iproj.pdf')
        video_fn = osp.join(video_dir, 'minigolf_ce_iproj.mp4')
        vis_result(ce_iproj_fn, img_fn, video_fn, args.no_save_img, args.no_save_video)

    if 'kl_iproj' in args.examples:
        print("KL iproj")
        img_fn = osp.join(img_dir, 'minigolf_kl_iproj.pdf')
        video_fn = osp.join(video_dir, 'minigolf_kl_iproj.mp4')
        vis_result(kl_iproj_fn, img_fn, video_fn, args.no_save_img, args.no_save_video)
