#!/usr/bin/env python
import os.path as osp
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from ll4ma_util import file_util, ui_util


CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")


if __name__ == '__main__':
    """
    This script deletes images and data associated with dubins 
    point plans that did not reach the goal position region.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default="~/tro_results/data/dubins_point_plans")
    parser.add_argument('--img_dir', type=str, default="~/tro_results/imgs/dubins_point_plans")
    parser.add_argument('--dry_run', action='store_true')
    args = parser.parse_args()
    
    fns = file_util.list_dir(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)

    # Get cfg so you can read out bounds (they're all the same)
    data = file_util.load_pickle(fns[0])
    cfg = file_util.load_yaml(osp.join(CONFIG_ROOT, data['args']['config']))
    lb = np.array(cfg['goal_lb'])[:2] - 0.1  # Giving a little slack
    ub = np.array(cfg['goal_ub'])[:2] + 0.1

    n_removed = 0
    for fn in tqdm(fns):
        data = file_util.load_pickle(fn)
        end_pt = data['mu_iterates'][-1][-1][:2]
        in_goal_region = np.all(end_pt >= lb) and np.all(end_pt <= ub)
        if not in_goal_region:
            if not args.dry_run:
                img_fn = osp.join(img_dir, osp.basename(fn).replace('.pickle', '.png'))
                file_util.remove(img_fn)
                file_util.remove(fn)
            n_removed += 1

    if args.dry_run:
        print(f"\n  DRY RUN. No files removed. Would have removed {n_removed} instances\n")
    else:
        ui_util.print_happy(f"\n  File cleaning done. Removed {n_removed} instances\n")
