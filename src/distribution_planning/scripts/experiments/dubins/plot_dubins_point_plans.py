#!/usr/bin/env python
import os.path as osp
import sys
import argparse
import random
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from distribution_planning.problems import PlanarNavigationProblem

from ll4ma_util import file_util, ui_util


CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")


if __name__ == '__main__':
    """
    This script deletes images and data associated with dubins 
    point plans that did not reach the goal position region.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default="~/tro_results/data/dubins_point_plans")
    parser.add_argument('--img_dir', type=str,
                        default="~/multisensory_learning_paper/goal_distribution_planning/imgs")
    parser.add_argument('--n_display', type=int, default=40)
    parser.add_argument('--seed', type=int, default=16)  # I thought it looked nice
    parser.add_argument('--no_save', action='store_true')
    args = parser.parse_args()
    
    fns = file_util.list_dir(args.data_dir)

    img_dir = osp.expanduser(args.img_dir)

    data = file_util.load_pickle(fns[0])
    cfg = file_util.load_yaml(osp.join(CONFIG_ROOT, data['args']['config']))
    problem = PlanarNavigationProblem(cfg)

    fig, ax = problem.visualize_env()

    random.seed(args.seed)
    
    # There are a few I definitely want to include because they give interesting paths,
    # the rest can be random and just find some seed that looks nice

    
    beta02_fns = [fn for fn in fns if 'beta-0.2' in fn]
    beta1_fns = [fn for fn in fns if 'beta-1.0' in fn]
    beta2_fns = [fn for fn in fns if 'beta-2.0' in fn]

    disp_beta02_fns = [fn for fn in beta02_fns if '0071' in fn or '0111' in fn or '0198' in fn]
    disp_beta1_fns = [fn for fn in beta1_fns if '0090' in fn]

    disp_beta02_fns += random.sample([fn for fn in beta02_fns if fn not in disp_beta02_fns],
                                     args.n_display - len(disp_beta02_fns))
    disp_beta1_fns += random.sample([fn for fn in beta1_fns if fn not in disp_beta1_fns],
                                    args.n_display - len(disp_beta1_fns))
    disp_beta2_fns = random.sample(beta2_fns, args.n_display)
    
    for fn in sorted(fns):
        data = file_util.load_pickle(fn)
        path = data['result'].mu_soln

        lw = 3
        ls ='solid'
        alpha = 0.7
        if fn in disp_beta02_fns:
            color = 'firebrick'
            label = 'beta-0.2'
        elif fn in disp_beta1_fns:
            color = 'darkblue'
            label = 'beta-1.0'
        elif fn in disp_beta2_fns:
            color = 'goldenrod'
            label = 'beta-2.0'
        else:
            continue # Not one of the samples we're visualizing

        ax.plot(path[:,0], path[:,1], c=color, alpha=alpha, lw=lw, ls=ls,
                label=label)
        plt.scatter(path[-1,0], path[-1,1], c='dodgerblue', s=35, zorder=100)
        
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    leg = ax.legend(*zip(*unique), loc=3, prop={'size': 22}, markerscale=10,
                    bbox_to_anchor=(0.1, 0.0))
    for line in leg.get_lines():
        line.set_linewidth(12)
    
    if not args.no_save:
        save_fn = osp.join(img_dir, "dubins_point_plans.pdf")
        plt.savefig(save_fn, bbox_inches='tight')
        
    plt.show()

