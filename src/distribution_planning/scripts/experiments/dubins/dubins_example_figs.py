#!/usr/bin/env python
import sys
import os
import os.path as osp
import random
import torch
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.animation import FuncAnimation

from ll4ma_util import file_util, ui_util, vis_util
from ll4ma_opt.solvers import SolverReturn

from distribution_planning.problems import PlanarNavigationProblem


if __name__ == '__main__':
    """
    This script creates all the images for the Dubins figures in the paper (except the
    point-based plans which is done separately).
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default="~/tro_results/data")
    parser.add_argument('--img_dir', type=str,
                        default="~/multisensory_learning_paper/goal_distribution_planning/imgs")
    parser.add_argument('--video_dir', type=str,
                        default="~/tro_results/videos/dubins")
    parser.add_argument('-e', '--examples', type=str, nargs='+',
                        default=['uniform', 'gaussian', 'truncated_gaussian', 'dirac',
                                 'gmm', 'classifier'])
    parser.add_argument('--gmm_prefix', type=str, default="dubins_example_gmm")
    parser.add_argument('--gmm_names', type=str, nargs='+',
                        default=['kl_iproj_left', 'kl_iproj_right', 'kl_mproj'])
    parser.add_argument('--no_save_img', action='store_true')
    parser.add_argument('--no_save_video', action='store_true')
    parser.add_argument('--seed', type=int, default=1)
    args = parser.parse_args()

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    data_dir = osp.expanduser(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)
    video_dir = osp.expanduser(args.video_dir)
    file_util.check_path_exists(data_dir, "Data directory")
    os.makedirs(img_dir, exist_ok=True)
    os.makedirs(video_dir, exist_ok=True)

    mproj_mean_c = 'xkcd:dark orange'
    mproj_cov_c = 'xkcd:tangerine'
    mproj_mec = 'xkcd:dark orange'
    mproj_mfc = 'xkcd:pale peach'

    iproj_mean_c = 'xkcd:plum purple'
    iproj_cov_c = 'xkcd:deep lavender'
    iproj_mec = 'xkcd:plum purple'
    iproj_mfc = 'xkcd:pale lavender'

    if not args.no_save_img:
        print(f"\n  Images saved: {img_dir}")
    if not args.no_save_video:
        print(f"\n  Videos saved: {video_dir}")
    
    for example in args.examples:
        if example == 'uniform':
            fn = "dubins_example_uniform_ce_mproj.pickle"
        elif example == 'gaussian':
            fn = "dubins_example_gaussian_ce_iproj.pickle"
        elif example == 'truncated_gaussian':
            fn = "dubins_example_truncated_gaussian_ce_mproj.pickle"
        elif example == 'dirac':
            fn = "dubins_example_dirac_kl_mproj.pickle"
        elif example == 'classifier':
            fn = "dubins_example_classifier_ce_iproj.pickle"
        elif example == 'gmm':
            continue  # Visualized in separate block below
        else:
            ui_util.print_error_exit(f"Unknown example: {example}")
        data_fn = osp.join(data_dir, fn)
        file_util.check_path_exists(data_fn)
        data = file_util.load_pickle(data_fn)
        cfg = file_util.load_yaml(osp.join(data['args']['config_root'], data['args']['config']))
        problem = PlanarNavigationProblem(cfg)
        
        fig, ax = problem.visualize_env()
    
        mean_c = mproj_mean_c if 'mproj' in fn else iproj_mean_c
        cov_c = mproj_cov_c if 'mproj' in fn else iproj_cov_c
        mec = mproj_mec if 'mproj' in fn else iproj_mec
        mfc = mproj_mfc if 'mproj' in fn else iproj_mfc
        
        problem.visualize_solution(ax, data['result'], mean_c=mean_c, mean_lw=2, cov_c=cov_c,
                                   mean_marker_ec=mec, mean_marker_fc=mfc, cov_alpha=0.3)
        problem.visualize_start(ax)


        if 'truncated' in example:
            # Having to manually edit truncated since I don't know how to do it otherwise
            if not args.no_save_img:
                save_fn = osp.join(img_dir, f"{fn.split('.')[0]}_nogoal.png")
                plt.savefig(save_fn, bbox_inches='tight', dpi=1000)
            problem.visualize_goal(ax)
            if not args.no_save_img:
                save_fn = osp.join(img_dir, f"{fn.split('.')[0]}.png")
                plt.savefig(save_fn, bbox_inches='tight', dpi=1000)
        else:
            problem.visualize_goal(ax)
            if not args.no_save_img:
                save_fn = osp.join(img_dir, f"{fn.split('.')[0]}.pdf")
                plt.savefig(save_fn, bbox_inches='tight')

        # TODO trying to animate right over the plot
        res = data['result']
        u = res.u_soln
        x0 = problem.p0.get_mean()
        rollouts = problem.env.get_rollouts(x0, u, 20)

        colors = vis_util.random_colors(20)
        

        paths = [plt.plot([], [], lw=3, color=colors[idx], alpha=0.9)[0]
                 for idx, rollout in enumerate(rollouts)]
        pts = [plt.scatter([], [], s=120, color=colors[idx], alpha=0.7)
               for idx in range(len(rollouts))]
        
        def animate(i):
            for idx, path in enumerate(paths):
                path.set_data(rollouts[idx,:i+1,0], rollouts[idx,:i+1,1])
            for idx, pt in enumerate(pts):
                pt.set_offsets(rollouts[idx,i,:2])
            return paths + pts
        
        anim = FuncAnimation(fig, animate, frames=rollouts.size(1), interval=10.,
                             blit=True, repeat_delay=6000)
        if not args.no_save_video:
            anim.save(osp.join(video_dir, f"{fn.split('.')[0]}.mp4"))

        plt.show()

    # GMM fig
    if 'gmm' in args.examples:
        data = {}
        for name in args.gmm_names:
            fn = osp.join(data_dir, f"{args.gmm_prefix}_{name}.pickle")
            data[name] = file_util.load_pickle(fn)
        
        # They all have same config
        cfg_fn = osp.join(data[args.gmm_names[0]]['args']['config_root'],
                          data[args.gmm_names[0]]['args']['config'])
        cfg = file_util.load_yaml(cfg_fn)
        problem = PlanarNavigationProblem(cfg)
        fig, ax = problem.visualize_env()

        problem.visualize_solution(ax, data['kl_iproj_left']['result'],
                                   mean_c=iproj_mean_c, mean_lw=2, cov_c=iproj_cov_c,
                                   mean_marker_ec=iproj_mec, mean_marker_fc=iproj_mfc,
                                   cov_alpha=0.3)
        problem.visualize_solution(ax, data['kl_iproj_right']['result'],
                                   mean_c=iproj_mean_c, mean_lw=2, cov_c=iproj_cov_c,
                                   mean_marker_ec=iproj_mec, mean_marker_fc=iproj_mfc,
                                   cov_alpha=0.3)
        problem.visualize_solution(ax, data['kl_mproj']['result'],
                                   mean_c=mproj_mean_c, mean_lw=2, cov_c=mproj_cov_c,
                                   mean_marker_ec=mproj_mec, mean_marker_fc=mproj_mfc,
                                   cov_alpha=0.3)

        problem.visualize_start(ax)
        problem.visualize_goal(ax)
        if not args.no_save_img:
            save_fn = osp.join(img_dir, "dubins_gmm.pdf")
            plt.savefig(save_fn, bbox_inches='tight')
        plt.show()
