#!/usr/bin/env python
import sys
import os
import os.path as osp
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

import torch
from torch.distributions import Uniform

from ll4ma_util import file_util, ui_util
from ll4ma_opt.solvers import SolverReturn

from distribution_planning.problems import DrakePlanarNavigationProblem
from distribution_planning.util import drake_util


CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")


if __name__ == '__main__':
    """
    This script generates the plans for randomly selected points from the goal region.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default="~/tro_results/data/dubins_point_plans")
    parser.add_argument('--img_dir', type=str, default="~/tro_results/imgs/dubins_point_plans")
    parser.add_argument('--log_dir', type=str, default="~/tro_results/logs")
    parser.add_argument('--config', type=str, default="dubins_example_dirac.yaml")
    parser.add_argument('--beta', type=float, default=2.)
    parser.add_argument('--n_points', type=int, default=200)
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--show', action='store_true')
    args = parser.parse_args()

    data_dir = osp.expanduser(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)
    log_dir = osp.expanduser(args.log_dir)
    
    os.makedirs(data_dir, exist_ok=True)
    os.makedirs(img_dir, exist_ok=True)
    os.makedirs(log_dir, exist_ok=True)

    config_fn = osp.join(CONFIG_ROOT, args.config)
    file_util.check_path_exists(config_fn, "YAML config file")
    cfg = file_util.load_yaml(config_fn)

    horizon = cfg['horizon']
    goal_cost = cfg['goal_cost']
    max_iters = cfg['max_iters']

    save_prefix = args.config.replace('.yaml', '')
    save_prefix += f"_{goal_cost}"
    save_prefix += f"_beta-{args.beta}"
    snopt_filename = osp.join(log_dir, f"{save_prefix}_snopt.log")

    if args.seed is not None:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed) 

    solver_options = drake_util.get_snopt_options(max_iters, snopt_filename)

    uniform = Uniform(torch.tensor(cfg['goal_lb']), torch.tensor(cfg['goal_ub']))
    
    for run_idx in range(args.n_points):
        save_prefix_i = f"{save_prefix}_{run_idx+1:04d}"

        cfg['goal_point'] = uniform.sample().numpy()
        
        problem = DrakePlanarNavigationProblem(cfg, horizon, args.beta, goal_cost)
    
        result, xs = drake_util.solve(problem.prog, solver_options)
        ll4ma_result = problem.get_ll4ma_result(result, xs)
        problem.save_data(xs, ll4ma_result,
                          osp.join(data_dir, f"{save_prefix_i}.pickle"), args)

        # anim = problem.visualize_grad_optimization(ll4ma_result)
    
        fig, ax = problem.visualize_env()
        problem.visualize_solution(ax, ll4ma_result)
        problem.visualize_start_goal(ax)

        plt.savefig(osp.join(img_dir, f"{save_prefix_i}.png"), bbox_inches='tight')
        if args.show:
            plt.show()
        plt.close('all')

