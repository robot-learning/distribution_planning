#!/usr/bin/env python

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

"""
This script simply plots a colormap to show the range of values in the appendix figure.
"""

gradient = np.linspace(0, 1, 256)
gradient = np.vstack((gradient, gradient))

def plot_color_gradients(name):
    # Create figure and adjust figure height to number of colormaps
    fig, ax = plt.subplots()
    fig.set_size_inches(15, 4)
    ax.imshow(gradient, aspect='auto', cmap=plt.get_cmap(name))
    ax.set_axis_off()
    plt.tight_layout()

plot_color_gradients('viridis')
plt.show()
