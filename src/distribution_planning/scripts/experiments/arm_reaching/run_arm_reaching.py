#!/usr/bin/env python
import sys
import os.path as osp
import numpy as np
import argparse
import torch
import random
from tqdm import tqdm

from distribution_planning.problems import ArmReachingProblem, ArmReachingSampler
from distribution_planning.util import drake_util

import distribution_planning
from ll4ma_util import file_util

from ll4ma_opt.solvers import MPPI, CEM
from ll4ma_opt.solvers.samplers import GaussianSampler

CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 3), "config")
DEFAULT_CONFIG = osp.join(CONFIG_ROOT, "drake_iiwa.yaml")


def get_fn_idx(fn):
    return osp.basename(fn).split('.')[0].split('_')[-1]


if __name__ == '__main__':
    """
    This script runs the arm-reaching experiments. It's assumed the reachable/unreachable
    data has already been generated using the generate_reachable_data.py script. This
    will run the solver to generate the plans and determine if it reached to a reachable
    component.

    You can run this script to view numerical results for previously generated planning 
    results. Just pass the directory you want to see results for to --save_dir. Only if
    you additionally pass the --regenerate flag will it wipe out that data and re-run
    everything. Without that flag, it just computes the statistics and prints them.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--solver', type=str, default='mppi', choices=['mppi', 'cem'])
    parser.add_argument('--n_samples', type=int, default=500)
    parser.add_argument('--max_iters', type=int, default=30)
    parser.add_argument('--start_var', type=float, default=0.002)
    parser.add_argument('--end_var', type=float, default=0.0001)
    parser.add_argument('--alpha', type=float, default=0.001)
    parser.add_argument('--save_dir', type=str,
                        default="~/tro_results/data/arm_reaching/runs")
    parser.add_argument('--reachable_dir', type=str,
                        default="~/tro_results/data/arm_reaching/reachable")
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--seed', type=int, default=1234)
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)

    save_dir = f"{osp.expanduser(args.save_dir)}_seed-{args.seed}"
    reachable_dir = osp.expanduser(args.reachable_dir)
    file_util.check_path_exists(reachable_dir, "Reachable data directory")
    reachable_fns = file_util.list_dir(reachable_dir, '.pickle')
    
    if not osp.exists(save_dir) or args.regenerate:
        file_util.safe_create_dir(save_dir)
        print(f"Running arm reaching for {len(reachable_fns)} runs...")
        for reachable_fn in tqdm(reachable_fns):
            obj_data = file_util.load_pickle(reachable_fn)
            problem = ArmReachingProblem(DEFAULT_CONFIG, headless=True, obj_data=obj_data)
            if args.solver == 'mppi':
                sampler = ArmReachingSampler(problem, args.start_var, args.end_var, args.max_iters)
                solver = MPPI(problem, sampler, args.alpha, args.n_samples, cost_epsilon=1e-10)
            elif args.solver == 'cem':
                # TODO use arm reaching sampler
                sigma0 = args.std*np.eye(problem.size())
                sampler = GaussianSampler(problem, sigma=sigma0)
                solver = CEM(problem, sampler, n_samples=args.n_samples)

            # TODO what to init?
            # bounds = list(zip(list(problem.min_d_thetas), list(problem.max_d_thetas)))
            # x0 = np.array([np.array([np.random.uniform(*b) for b in bounds])
            #                for _ in range(problem.horizon)]).flatten()
            x0 = None
            
            result = solver.optimize(x0, max_iterations=args.max_iters, verbose=False)
            save_idx = get_fn_idx(reachable_fn)
            save_fn = osp.join(save_dir, f"arm_reaching_run_{save_idx}.pickle")
            problem.save_result(result, save_fn)
        print("Data collection complete.")

    # Run simple analysis to see how frequently it reached a reachable component
    reached_pcts = []
    fns = sorted(file_util.list_dir(save_dir))
    for fn in fns:
        # This data has all the info about component reachability
        idx = get_fn_idx(fn)
        reachable_fn = list(filter(lambda x: idx in x, reachable_fns))[0]
        reachable_data = file_util.load_pickle(reachable_fn)
        
        data = file_util.load_pickle(fn)
        reached = data['objects']['sugar']['reached']  # TODO hard-coded
        reached_pct = reachable_data['sugar']['components'][reached]['reachable_pct']

        options = [reachable_data['sugar']['components'][n]['reachable_pct']
                   for n in reachable_data['sugar']['components'].keys()]
        print(f"{idx}: {reached_pct} - {options}")

        reached_pcts.append(reached_pct)

    print(f"COUNT: {len([r for r in reached_pcts if r > 50])} / {len(fns)}")


    # Compute reachability statistics
    n_unreachable_samples = []
    n_unreachable_comps = []
    for fn in reachable_fns:
        data = file_util.load_pickle(fn)['sugar']
        # Figure out how many samples from full distribution were unreachable
        n_unreachable_samples.append(len([r for r in data['samples_reachable'] if not r]))
        # Figure out unreachable components
        n_unreachable_comps.append(len([v['reachable_pct'] for v in data['components'].values()
                                        if v['reachable_pct'] <= 50]))
                
    print(f"Unreachable samples: mean = {np.mean(n_unreachable_samples)}, "
          f"std = {np.std(n_unreachable_samples)}")
    print(f"Unreachable components: mean = {np.mean(n_unreachable_comps)}, "
          f"std = {np.std(n_unreachable_comps)}")
