#!/usr/bin/env python
import sys
import gc
import os.path as osp
import numpy as np
import argparse
import torch
import random
from tqdm import tqdm

import matplotlib; matplotlib.use('agg')  # GUI-less backend since I'm just saving images
import matplotlib.pyplot as plt

from distribution_planning.environments import DrakeSim

from ll4ma_util import file_util, ui_util


CONFIG_ROOT = osp.join(osp.dirname(osp.dirname(osp.dirname(osp.abspath(__file__)))), "config")
DEFAULT_CONFIG = osp.join(CONFIG_ROOT, "drake_iiwa.yaml")


if __name__ == '__main__':
    """
    This script spawns the object randomly around the table and generates samples
    from PMM distribution to check if reachable or not using IK. Saves the data as 
    well as an RGB image to the specified directories, where the image displays
    colored spheres for whether the samples were reachable or not.

    TODO: there's a memory leak somewhere but I couldn't figure it out, this will
    slowly build up memory until it maxes it out and kills the job (~135 instances).
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_poses', type=int, default=100)
    parser.add_argument('--n_samples', type=int, default=100)
    parser.add_argument('--data_dir', type=str,
                        default="~/tro_results/data/arm_reaching/reachable")
    parser.add_argument('--img_dir', type=str,
                        default="~/tro_results/imgs/arm_reaching/reachable")
    parser.add_argument('--save_prefix', type=str, default="reachable_data")
    parser.add_argument('--theta', type=float, default=0.3)
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--seed', type=int, default=None)
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)

    data_dir = osp.expanduser(args.data_dir)
    img_dir = osp.expanduser(args.img_dir)
    if osp.exists(data_dir) and not args.regenerate:
        fns = file_util.list_dir(data_dir, '.pickle')
        n_collected = len(fns)
        n_to_collect = args.n_poses - n_collected
        start_idx = n_collected
        ui_util.print_info(f"\n  Already collected {n_collected} instances. Collecting "
                           f"{n_to_collect} more instances to reach total {args.n_poses}")
    else:
        file_util.safe_create_dir(data_dir)
        file_util.safe_create_dir(img_dir)
        n_to_collect = args.n_poses
        start_idx = 0

    print(f"\nSpawning object in {n_to_collect} poses to collect "
          f"reachable data ({args.n_samples} per pose)...")
    for idx in tqdm(range(start_idx, args.n_poses)):
        env = DrakeSim(DEFAULT_CONFIG, headless=True)
        
        # Save the data
        data = {}
        for obj_name, obj in env.objects.items():
            samples = obj.distribution.sample(args.n_samples)
            reachable = [env.pose_reachable(s[:3], s[3:], args.theta) for s in samples]
            data[obj_name] = {
                'config': obj.cfg,
                'samples': samples,
                'samples_reachable': reachable,
                'pos': obj.pos,
                'quat': obj.quat,
                'components': {}
            }

            # Generating a bunch of samples from each component and checking reachability.
            for c in obj.distribution.components:
                mode_pos = c.gauss.get_mean()
                mode_quat = c.bingham.mode
                mode_reachable = env.pose_reachable(mode_pos, mode_quat, args.theta)
                
                reachable_pct = 0
                c_samples = c.sample(100)
                c_samples_reachable = []
                for c_sample in c_samples:
                    c_sample_pos = c_sample[:3]
                    c_sample_quat = c_sample[3:]
                    c_samples_reachable.append(env.pose_reachable(c_sample_pos,
                                                                  c_sample_quat,
                                                                  args.theta))
                data[obj_name]['components'][c.name] = {
                    'mode_reachable': mode_reachable,
                    'reachable_pct': len([s for s in c_samples_reachable if s]),
                    'samples_reachable': c_samples_reachable,
                    'samples': c_samples
                }
            
        data_fn = osp.join(data_dir, f"{args.save_prefix}_{idx+1:04d}.pickle")
        file_util.save_pickle(data, data_fn)
        
        # Respawning with the saved object data to create RGB image of reachable
        # scene. Note this must be done separately based on how I'm currently
        # handling the visualization, I can't use IK to color spheres until
        # plant is finalized, but then you can't change color after finalized.
        # At least, I haven't figured out how to do that.
        
        # Save the image
        env = DrakeSim(DEFAULT_CONFIG, headless=True, obj_data=data)
        rgb_image = env.get_rgb_image()
        fig, ax = plt.subplots()
        fig.set_size_inches(12, 9)
        plt.imshow(rgb_image.data)
        plt.tick_params(axis='both', which='both', bottom=False, left=False,
                        labelbottom=False, labelleft=False)
        plt.tight_layout()
        img_fn = osp.join(img_dir, f"{args.save_prefix}_{idx+1:04d}.png")
        plt.savefig(img_fn, bbox_inches='tight')
        plt.close(fig)
    
    ui_util.print_happy("\n  Data collection complete.")
    print(f"  Data saved: {data_dir}")
    print(f"  Images saved: {img_dir}\n")
