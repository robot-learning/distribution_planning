#!/usr/bin/env python
import sys
import os.path as osp
import argparse
import subprocess
from PIL import Image

from ll4ma_util import file_util


if __name__ == '__main__':
    """
    This script takes all of the recorded videos in the specified video directory,
    creates images using ffmpeg, and crops them all to the same region (I manually
    had to figure out the crop region). The images are saved in the same folder
    as the videos in a new folder named as the video file without its extension.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--video_dir', type=str, default="~/tro_results/videos/arm_reaching")
    args = parser.parse_args()

    crop_area = (700, 100, 1640, 920)  # Just tedious guess and check
    
    for fn in file_util.list_dir(args.video_dir, '.mp4'):
        img_dir = osp.splitext(fn)[0]
        img_fn = f"{img_dir}/img_%02d.png"
        file_util.create_dir(img_dir)
        subprocess.call(['ffmpeg', '-i', fn, '-vf', 'fps=2', img_fn])

        # Crop all images the same
        for img_fn in file_util.list_dir(img_dir, '.png'):
            img = Image.open(img_fn)
            cropped_img = img.crop(crop_area)
            # cropped_img.show(); sys.exit()  # Can uncomment to debug
            cropped_img.save(img_fn)  # Overwriting old image


