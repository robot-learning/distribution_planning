#!/usr/bin/env python
import os
import os.path as osp
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
# Importing these so you can seed them for randomization
import random
import torch

from distribution_planning.problems import (
    DrakePlanarNavigationProblem, DrakeMiniGolfProblem, DrakeArmReachingProblem
)
from distribution_planning.util import drake_util

from ll4ma_util import file_util


CONFIG_ROOT = osp.join(osp.dirname(osp.dirname(osp.abspath(__file__))), "config")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_root', type=str, default=CONFIG_ROOT)
    parser.add_argument('--config', type=str, default='dubins_example_gaussian.yaml')
    parser.add_argument('--save_dir', type=str, default='~/tro_results')
    parser.add_argument('--horizon', type=int, default=30)
    parser.add_argument('--max_iters', type=int, default=100)
    parser.add_argument('--beta', type=float, default=2.)
    parser.add_argument('--goal_cost', type=str, default='ce_iproj',
                        choices=['kl_iproj', 'kl_mproj', 'ce_iproj', 'ce_mproj', 'mean_l2',
                                 'log_goal_pdf', 'log_terminal_pdf', 'goal_pdf', 'terminal_pdf'])
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--init_to_goal', action='store_true')
    parser.add_argument('--test_env', action='store_true')
    parser.add_argument('--show', action='store_true')
    parser.add_argument('--no_show', dest='show', action='store_false')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--no_verbose', dest='verbose', action='store_false')
    parser.set_defaults(verbose=True, show=True)
    args = parser.parse_args()

    save_dir = osp.expanduser(args.save_dir)
    img_dir = osp.join(save_dir, "imgs")
    log_dir = osp.join(save_dir, "logs")
    data_dir = osp.join(save_dir, "data")
    config_fn = osp.join(args.config_root, args.config)
    file_util.check_path_exists(config_fn, "YAML config file")
    file_util.create_dir(img_dir)
    file_util.create_dir(log_dir)
    file_util.create_dir(data_dir)
    cfg = file_util.load_yaml(config_fn)

    seed = cfg['seed'] if 'seed' in cfg else args.seed
    horizon = cfg['horizon'] if 'horizon' in cfg else args.horizon
    goal_cost = cfg['goal_cost'] if 'goal_cost' in cfg else args.goal_cost
    max_iters = cfg['max_iters'] if 'max_iters' in cfg else args.max_iters

    save_prefix = args.config.replace('.yaml', '')
    save_prefix += f"_{goal_cost}"
    snopt_filename = osp.join(log_dir, f"{save_prefix}_snopt.log")

    if seed is not None:
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)  # Doing torch also since might be using learned model
    
    # np.seterr(all='raise')  # Uncomment to see what's throwing numpy warnings

    if cfg['problem'] == 'planar_nav':
        problem = DrakePlanarNavigationProblem(cfg, horizon, args.beta, goal_cost,
                                               args.init_to_goal)
    elif cfg['problem'] == 'minigolf':
        problem = DrakeMiniGolfProblem(cfg, horizon, args.beta, goal_cost)
    elif cfg['problem'] == 'arm_reaching':
        problem = DrakeArmReachingProblem(cfg, horizon)
    else:
        raise ValueError(f"Unknown problem type: {cfg['problem']}")
    
    if args.test_env:
        print(problem.prog)
        fig, ax = problem.visualize_env()
        problem.visualize_start_goal(ax)
        plt.show()
        sys.exit()
        
    solver_options = drake_util.get_snopt_options(max_iters, snopt_filename)
    result, xs = drake_util.solve(problem.prog, solver_options, args.verbose)
    ll4ma_result = problem.get_ll4ma_result(result, xs)
    problem.save_data(xs, ll4ma_result, osp.join(data_dir, f"{save_prefix}.pickle"), args)

    anim = problem.visualize_grad_optimization(ll4ma_result)
    
    fig, ax = problem.visualize_env()
    problem.visualize_solution(ax, ll4ma_result)
    problem.visualize_start_goal(ax)

    
    plt.savefig(osp.join(img_dir, f"{save_prefix}.pdf"), bbox_inches='tight')
    if args.show:
        plt.show()
