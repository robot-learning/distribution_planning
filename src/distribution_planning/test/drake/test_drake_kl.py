#!/usr/bin/env python
import numpy as np
from scipy.stats import invwishart as IW

import torch
from torch.distributions import MultivariateNormal as MVN
from torch.distributions import kl_divergence as KL_torch

from distribution_planning.util.drake_util import KL as KL_drake

            
if __name__ == '__main__':
    N = 5
    mu1 = np.random.rand(N)
    mu2 = np.random.rand(N)
    sigma1 = IW.rvs(N, np.eye(N))
    sigma2 = IW.rvs(N, np.eye(N))
    mvn1 = MVN(torch.tensor(mu1), torch.tensor(sigma1))
    mvn2 = MVN(torch.tensor(mu2), torch.tensor(sigma2))

    print("DRAKE KL", KL_drake(mu1, sigma1, mu2, sigma2))    
    print("TORCH KL", KL_torch(mvn1, mvn2).item())

        
