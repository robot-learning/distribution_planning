#!/usr/bin/env python
import sys
import numpy as np
import numpy.matlib

from pydrake.all import MathematicalProgram, Variable, Solve, eq

from distribution_planning.util.drake_util import unscented_transform


def f(x):
    r = np.sqrt(x[0]**2 + x[1]**2)
    theta = np.arctan2(x[1], x[0])
    return np.array([r, theta])


def g(x):
    return unscented_transform(x[:5], 2, f) - x[5:]
    

if __name__ == '__main__':
    '''
    Test example from wikipedia: https://en.wikipedia.org/wiki/Unscented_transform
    '''
    mu_sigma0 = np.array([12.3, 7.6, 1.44, 0.0, 2.89]) # tril sigma values
    init = np.zeros(2*(len(mu_sigma0)))

    prog = MathematicalProgram()
    mu_sigma1 = prog.NewContinuousVariables(5, "mu_sigma1")
    mu_sigma2 = prog.NewContinuousVariables(5, "mu_sigma2")

    prog.AddBoundingBoxConstraint(mu_sigma0, mu_sigma0, mu_sigma1)
    prog.AddConstraint(g, lb=np.zeros(5), ub=np.zeros(5),
                       vars=np.concatenate([mu_sigma1, mu_sigma2]))
    
    result = Solve(prog, init)

    np.set_printoptions(precision=3, suppress=True)
    print(f"\nSuccess? {result.is_success()}")
    print(f"Solver: {result.get_solver_id().name()}")
    print(f"Soln: {result.GetSolution()}\n")
