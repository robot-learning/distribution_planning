#!/usr/bin/env python
import sys
import numpy as np
import numpy.matlib

from pydrake.all import MathematicalProgram, Variable, Solve, eq

from distribution_planning.util.drake_util import unscented_transform


def f(x):
    return np.array([np.sin(x[0]) + x[1]**2, x[1]**0.5])
    

if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    
    mu_sigma0 = np.array([12.3, 7.6, 1.44, 0.0, 2.89]) # tril sigma values
    init = np.zeros(2*(len(mu_sigma0)))

    T = 4
    
    r = mu_sigma0
    for t in range(T):
        r = unscented_transform(r, 2, f)
        mu = r[:2].astype(float)
        sigma = r[2:].astype(float)
        # print("MU", mu, "SIGMA", sigma)
    
    
        


