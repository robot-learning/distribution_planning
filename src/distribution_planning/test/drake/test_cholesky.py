#!/usr/bin/env python
import numpy as np

from distribution_planning.util.drake_util import cholesky


if __name__ == '__main__':
    
    m1 = np.array([[25, 15, -5],
                   [15, 18,  0],
                   [-5,  0, 11]])
    print(cholesky(m1))
    print("NUMPY", np.linalg.cholesky(m1))
    print()
 
    m2 = np.array([[18, 22,  54,  42],
                   [22, 70,  86,  62],
                   [54, 86, 174, 134],
                   [42, 62, 134, 106]])
    print(cholesky(m2))
    print("NUMPY", np.linalg.cholesky(m2))
