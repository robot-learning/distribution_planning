#!/usr/bin/env python
import sys
import yaml
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


from pydrake.all import MathematicalProgram, Variable, Solve, eq


class PolygonEnvironment:

    def __init__(self):
        self.obstacles = {}
        self.sdfs = {}
        
    def add_obstacle(self, name, config):
        self.obstacles[name] = config
        self.sdfs[name] = self._sdf_factory(config)

    def draw_obstacles(self, ax):
        for name, config in self.obstacles.items():
            if config['type'] == 'rectangle':
                x_min, y_min, x_max, y_max = config['bounds']
                patch = patches.Rectangle((x_min, y_min), x_max - x_min, y_max - y_min)
            elif config['type'] == 'sphere':
                patch = patches.Circle(config['origin'], config['radius'])                
            else:
                raise ValueError(f"Unknown obstacle type: {config['type']}")
            ax.add_patch(patch)

    def draw_scene(self):
        """
        This is currently more for debugging purposes, should add some better visualization
        options e.g. having env config
        """
        fig, ax = plt.subplots(1, 1)
        self.draw_obstacles(ax)
        fig.set_size_inches(8, 8)
        ax.axis('equal')
        plt.tight_layout()
        plt.show()

    def _sdf_factory(self, config):
        if config['type'] == 'sphere':
            origin = np.array(config['origin'])
            return lambda x: np.array([np.square(x - origin).sum() - config['radius']**2])
        else:
            raise ValueError(f"Unsupported type for SDF: {config['type']}")


if __name__ == '__main__':
    fn = '/home/adam/ll4ma-opt-sandbox/src/ll4ma_opt/problems/config/double_integrator_spheres.yaml'
    with open(fn, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    
    env = PolygonEnvironment()
    for name, obs_config in config['obstacles'].items():
        env.add_obstacle(name, obs_config)
    
    N = 80
    dt = 0.1
    x0 = [0, -7, 0, 0]
    xG = [0, 7, 0, 0]
    init = np.random.randn(2*(N-1) + 4*N)
    
    A = np.mat([[1, 0, dt,  0],
                [0, 1,  0, dt],
                [0, 0,  1,  0],
                [0, 0,  0,  1]])
    B = np.mat([[ 0,  0],
                [ 0,  0],
                [dt,  0],
                [ 0, dt]])


    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(9, 9)
    env.draw_obstacles(ax)    
    path = ax.plot([], [], lw=8, color='goldenrod')[0]
    ax.axis('equal')
    ax.set_xlim(-10, 10)
    ax.set_ylim(-10, 10)
        
    iter_count = 0
    prog = MathematicalProgram()    
    u = prog.NewContinuousVariables(2, N-1, "u") # x, y acc
    x = prog.NewContinuousVariables(4, N, "x") # x, y pos/vel

    def update(x):
        global iter_count
        x = x.reshape(4, -1)[:,:-1] # I don't know why last timestep is zero
        path.set_data(x[0], x[1])
        ax.set_title(f'iteration {iter_count}')
        fig.canvas.draw()
        fig.canvas.flush_events()
        iter_count += 1
        plt.pause(0.2)
    prog.AddVisualizationCallback(update, x.flatten())

    prog.AddBoundingBoxConstraint(x0, x0, x[:,0])
    for n in range(N-1):
        prog.AddConstraint(eq(x[:,n+1], A.dot(x[:,n]) + B.dot(u[:,n])))
        prog.AddBoundingBoxConstraint(-1, 1, u[:,n])
        for sdf in env.sdfs.values():
            prog.AddConstraint(sdf, lb=[0], ub=[np.inf], vars=x[:2,n])
    prog.AddBoundingBoxConstraint(xG, xG, x[:,N-1])
    
    result = Solve(prog, init)
    print("Success?", result.is_success())
    print("Solver:", result.get_solver_id().name())

    infeasible_constraints = result.GetInfeasibleConstraints(prog)
    for c in infeasible_constraints:
        print(f"infeasible constraint: {c}")

    
    # x_sol = result.GetSolution(x)

    # fig, ax = plt.subplots(1, 1)
    # fig.set_size_inches(9, 9)
    # env.draw_obstacles(ax)    
    # ax.plot(x_sol[0], x_sol[1], lw=8, color='goldenrod')[0]
    # ax.axis('equal')
    # ax.set_xlim(-10, 10)
    # ax.set_ylim(-10, 10)
    # plt.tight_layout()
    # plt.show()
    

