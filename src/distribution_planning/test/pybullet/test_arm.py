#!/usr/bin/env python
import numpy as np

from ll4ma_opt.problems.resources import Panda

if __name__ == '__main__':
    panda = Panda()
    joints = np.stack([np.linspace(0, np.pi/2., 100) for _ in range(panda.num_joints)]).T
    panda.visualize_pybullet(joints)
