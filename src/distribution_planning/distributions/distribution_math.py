import sys
import numpy as np

import torch
from torch.distributions import (
    Distribution as TorchDistribution,
    kl_divergence as torch_kl_divergence
)

from distribution_planning.distributions import (
    Gaussian,
    TruncatedGaussian,
    DiracDelta,
    Uniform,
    GMM,
    UMM,
    PoseDistribution,
    PoseMM,
    Classifier
)
from distribution_planning.distributions.torch_distributions import (
    GaussianMixtureModel as BatchGaussianMixtureModel
)
from distribution_planning.util.math_util import det

from ll4ma_util import func_util

pydrake = func_util.silent_import('pydrake')
pybingham = func_util.silent_import('pybingham')


class NotDefinedError(Exception):
    
    def __init__(self, msg):
        super().__init__(msg)


def goal_cost(pG, pT, goal_cost):
    if goal_cost == 'kl_iproj':
        cost = kl_divergence(pT, pG)
    elif goal_cost == 'kl_mproj':
        cost = kl_divergence(pG, pT)
    elif goal_cost == 'ce_iproj':
        cost = cross_entropy(pT, pG)
    elif goal_cost == 'ce_mproj':
        cost = cross_entropy(pG, pT)
        # TODO these have been written assuming Gaussian, need to expand to other distributions
    elif goal_cost == 'mean_l2':
        cost = np.linalg.norm(pG.get_mean() - pT.get_mean())
    elif goal_cost == 'log_goal_pdf':
        cost = -pG.log_pdf(pT.get_mean())
    elif goal_cost == 'log_terminal_pdf':
        cost = -pT.log_pdf(pG.get_mean())
    elif goal_cost == 'goal_pdf':
        cost = -pG.pdf(pT.get_mean())
    elif goal_cost == 'terminal_pdf':
        cost = -pT.pdf(pG.get_mean())
    else:
        raise ValueError(f"Unknown goal cost type: {goal_cost}")
    return cost

        
def kl_divergence(p1, p2):
    '''
    Computes KL divergence between two probability distributions
    '''
    if isinstance(p1, TorchDistribution) and isinstance(p2, TorchDistribution):
        # Defer to torch's computations since they're batched
        kl = torch_kl_divergence(p1, p2)
    elif isinstance(p1, Gaussian) and isinstance(p2, Gaussian):
        kl = _kl_gauss_gauss(p1, p2)
    elif isinstance(p1, DiracDelta) and isinstance(p2, Gaussian):
        kl = _kl_dirac_gauss(p1, p2)
    elif isinstance(p2, DiracDelta):
        raise NotDefinedError("KL not defined for DISTRIBUTION || Dirac")
    elif isinstance(p2, Uniform):
        raise NotDefinedError("KL not defined for DISTRIBUTION || Uniform")
    elif isinstance(p1, Uniform) and isinstance(p2, Gaussian):
        kl = _kl_uniform_gauss(p1, p2)
    elif isinstance(p1, UMM):
        kl = _kl_umm_p(p1, p2)
    elif isinstance(p1, Gaussian) and isinstance(p2, GMM):
        p1_gmm = GMM([1], [p1.get_mean()], [p1.get_cov()])
        kl = _kl_gmm_gmm(p1_gmm, p2)
    elif isinstance(p1, GMM) and isinstance(p2, Gaussian):
        p2_gmm = GMM([1], [p2.get_mean()], [p2.get_cov()])
        kl = _kl_gmm_gmm(p1, p2_gmm)
    elif isinstance(p1, GMM) and isinstance(p2, GMM):
        kl = _kl_gmm_gmm(p1, p2)
    elif isinstance(p1, BatchGaussianMixtureModel) and isinstance(p2, BatchGaussianMixtureModel):
        kl = _kl_bgmm_bgmm(p1, p2)
    elif isinstance(p1, pybingham.Bingham) and isinstance(p2, pybingham.Bingham):
        kl = pybingham.bingham_kl_divergence(p1, p2)
    elif isinstance(p1, PoseDistribution) and isinstance(p2, PoseDistribution):
        kl = _kl_pose_pose(p1, p2)
    elif isinstance(p1, PoseDistribution) and isinstance(p2, PoseMM):
        # TODO I think you can use more generic MM KLs since they all
        # seem to be doing approximations in the same way
        kl = _kl_pose_pmm(p1, p2)
    else:
        raise NotImplementedError("KL currently only defined for:\n"
                                  "        Gaussian || Gaussian\n"
                                  "           Dirac || Gaussian\n"
                                  "         Uniform || Gaussian\n"
                                  "             UMM || DISTRIBUTION\n"
                                  "        Gaussian || GMM\n"
                                  "             GMM || Gaussian\n"
                                  "             GMM || GMM\n"
                                  "         Bingham || Bingham\n"
                                  "PoseDistribution || PoseDistribution\n"
                                  f"\nGot KL({type(p1)} || {type(p2)})")
    return kl


def _kl_gauss_gauss(g1, g2):
    mu1 = g1.get_mean()
    mu2 = g2.get_mean()
    mu_diff = mu2 - mu1
    Sigma1 = g1.get_cov()
    Sigma2 = g2.get_cov()
    Sigma2_inv = pydrake.math.inv(Sigma2)
    const = np.log(det(Sigma2) / det(Sigma1)) - len(mu1) + np.trace(np.dot(Sigma2_inv, Sigma1))
    kl = 0.5 * (const + np.dot(mu_diff, np.dot(Sigma2_inv, mu_diff)))
    return kl


def _kl_dirac_gauss(d, g):
    Sigma = g.get_cov()
    const = np.log(np.sqrt((2 * np.pi)**g.size() * det(Sigma)))
    diff = d.get_point() - g.get_mean()
    squared_mahalanobis = np.dot(diff, np.dot(pydrake.math.inv(Sigma), diff))
    kl = const + 0.5 * squared_mahalanobis
    return kl
    

def _kl_uniform_gauss(u, g):
    '''
    NOTE: this currently approximates whatever Gaussian you pass in as having
    diagonal cov because that's what I have code for. I'm blindly taking the 
    pytorch implementation [1]. Should do derivation to verify, can look at [2] 
    for reference. Can potentially relax this approximation if you can do
    derivation with full cov. This code also just computes per-dimension
    independently, so they get resolved to a single value through sum.

    TODO I don't know if summing is actually correct, need to look at derivation.

    [1] https://github.com/pytorch/pytorch/blob/master/torch/distributions/kl.py#L783-#L788
    [2] https://stats.stackexchange.com/questions/560848
    '''
    Sigma_diag = g.get_cov_diag()
    u_low = u.get_low()
    u_high = u.get_high()
    common_term = u_high - u_low
    t1 = np.log(np.sqrt(2 * np.pi) * np.sqrt(Sigma_diag) / common_term)
    t2 = (common_term**2) / 12.
    t3 = ((u_high + u_low - 2 * g.get_mean()) / 2.)**2
    kl = t1 + 0.5 * (t2 + t3) / Sigma_diag
    kl = kl.sum() # TODO not sure if this is correct thing to do yet    
    return kl


def _kl_gmm_gmm(m1, m2):
    '''
    This computes an approximate KL between two GMMs (no closed form solution
    exists). See [1] for different approximations, this one implements the
    unscented transformation (Sec. 3 in [1]).

    [1] Hershey, John R., and Peder A. Olsen. Approximating the Kullback 
        Leibler divergence between Gaussian mixture models. ICASSP 2007
    '''
    kl = 0
    for w, c in m1.get_components():
        sps = c.get_sigma_points()
        kl += sum([w * (c.log_pdf(sp) - m2.log_pdf(sp)) for sp in sps])
    kl /= len(sps)
    return kl


def _kl_bgmm_bgmm(p1, p2):
    if p1.batch_size != p2.batch_size:
        raise ValueError("Expected distributions to have same batch size but p1 "
                         f"has size {p1.batch_size} and p2 has size {p2.batch_size}")    
    kl = torch.zeros(p1.batch_size)
    sps = p1.sigma_points(beta=1)    # (B, n_sigma, D, K)
    sps = sps.transpose(1, 0)  # (n_sigma, B, D, K)
    n_sigma = sps.size(0)
    for comp_idx in range(p1.n_components):
        weight = p1.alpha[:,comp_idx]  # (B,)
        p1_comp_log_pdf = p1.mvns[comp_idx].log_prob(sps[:,:,:,comp_idx]) # (n_sigma, B)
        p2_log_pdf = p2.log_pdf(sps[:,:,:,comp_idx]) # (n_sigma_B)
        kl += weight * (p1_comp_log_pdf - p2_log_pdf).sum(dim=0)
    kl /= n_sigma
    return kl


def _kl_umm_p(umm, p):
    '''
    This computes an approximate KL M-projection between a mixture of 
    uniforms (UMM) and an arbitrary other distribution. This is a similar
    computation to the unscented approximation for GMMs, but we compute
    "sigma points" for a uniform to do it, which are symmetrically
    dispersed about the mean but taking the high/low values instead of
    a function of standard deviation as done for a Gaussian.
    '''
    kl = 0
    for w, c in umm.get_components():
        sps = c.get_sigma_points()
        kl += sum([w * (c.log_pdf(sp) - p.log_pdf(sp)) for sp in sps])
    kl /= len(sps)
    return kl


def _kl_pose_pose(p1, p2):
    '''
    Pose distributions are combined Gaussian for position and Bingham for
    orientation. KL is defined for both, seems sensible to just sum them
    for a combined KL but I haven't seen that actually done anywhere.
    '''
    g1 = p1.pos_gaussian
    g2 = p2.pos_gaussian
    b1 = p1.rot_bingham
    b2 = p2.rot_bingham
    g_kl = kl_divergence(g1, g2)
    b_kl = kl_divergence(b1, b2)
    
    # print("GAUSS KL", g_kl)
    # print(" BING KL", b_kl)
    
    kl = g_kl + b_kl
    return kl


def _kl_pose_pmm(p1, p2):
    '''
    Approximate KL between Gaussian and PoseDistribution mixture model.
    Uses sigma points from Gaussian and PoseMM pdf.

    TODO I think this can be integrated with a more generic mixture model
    approximation similar to UMM and GMM above.
    '''
    sps = p1.get_sigma_points()  # TODO these don't yet do SP for bingham
    kl = sum([(p1.log_pdf(sp) - p2.log_pdf(sp)) for sp in sps]) / len(sps)
    return kl

def entropy(p):
    if isinstance(p, Gaussian):
        H = _entropy_gauss(p)
    elif isinstance(p, Uniform):
        H = _entropy_uniform(p)
    else:
        raise NotImplementedError(f"Unknown distribution type for entropy: {type(p)}")
    return H


def _entropy_gauss(p):
    return 0.5 * p.size() * (1. + np.log(2.*np.pi)) + 0.5 * np.log(det(p.get_cov()))


def _entropy_uniform(p):
    # TODO I don't know how correct it is to do this sum
    return np.log(p.high - p.low).sum()


def cross_entropy(p1, p2):
    if isinstance(p1, TruncatedGaussian) and isinstance(p2, Gaussian):
        ce = _ce_sp_approx(p1, p2)    
    elif (isinstance(p1, Gaussian) or isinstance(p1, Uniform)) and \
       (isinstance(p2, Gaussian) or isinstance(p2, GMM)):
        ce = kl_divergence(p1, p2) + entropy(p1)
    elif isinstance(p1, Gaussian) and isinstance(p2, Classifier):
        # TODO I'm not 100% sure this makes sense yet, trying to just treat
        # the classifier (softmax) output as the PDF.
        ce = _ce_sp_approx(p1, p2)
    else:
        raise NotImplementedError(f"Unknown distribution types for cross-entropy: "
                                  f"{type(p1)}, {type(p2)}")
    return ce


def _ce_sp_approx(p1, p2):
    sps = p1.get_sigma_points(include_mean=True)
    ce = -sum([p1.pdf(sp) * p2.log_pdf(sp) for sp in sps])
    ce /= len(sps)
    return ce


if __name__ == '__main__':
    # Compare against pytorch implementations
    import torch
    from torch.distributions import MultivariateNormal as MVN
    from torch.distributions import kl_divergence as torch_KL
    from scipy.stats import invwishart as IW
    
    n_tests = 1000
    d = 10
    prec = 1e-8

    for _ in range(n_tests):
        mu1 = np.random.randn(d)
        mu2 = np.random.randn(d)
        Sigma1 = IW.rvs(d, 0.1 * np.eye(d))
        Sigma2 = IW.rvs(d, 0.1 * np.eye(d))
        low = np.min(np.stack([mu1, mu2]), axis=0) # Just using mus out of convenience
        high = np.max(np.stack([mu1, mu2]), axis=0)
        
        g1 = Gaussian(mu1, Sigma1)
        g2 = Gaussian(mu2, Sigma2)
        u = Uniform(low, high)
        
        torch_g1 = g1.as_torch()
        torch_g1_diag = g1.as_torch(diag=True)
        torch_g2 = g2.as_torch()
        torch_u = u.as_torch()
        
        kl_g1_g2_me = kl_divergence(g1, g2)
        kl_g1_g2_torch = torch_KL(torch_g1, torch_g2).item()
        assert np.allclose(kl_g1_g2_me, kl_g1_g2_torch, atol=prec), \
            f"Gauss || Gauss failed:\n\n    Expected={kl_g1_g2_torch}, Computed={kl_g1_g2_me}\n"
        
        kl_u_g1_me = kl_divergence(u, g1)
        kl_u_g1_torch = torch_KL(torch_u, torch_g1_diag).sum().numpy()
        assert np.allclose(kl_u_g1_me, kl_u_g1_torch, atol=prec), \
            f"Uniform || Gauss failed:\n\n    Expected={kl_u_g1_torch}, Computed={kl_u_g1_me}\n"

        # # See if 1-component GMM-GMM reduces to Gauss-Gauss
        # gmm1 = GMM([1], [mu1], [Sigma1])
        # gmm2 = GMM([1], [mu2], [Sigma2])
        # kl_gmm = kl_divergence(gmm1, gmm2)
        # assert np.allclose(kl_gmm, kl_g1_g2_me, atol=prec), \
        #     f"GMM || GMM failed: \n\n    Expected={kl_g1_g2_me}, Computed={kl_gmm}\n"
        # print("KL GMM", kl_gmm, "Expected", kl_g1_g2_me)

        entropy_me = entropy(g1)
        entropy_torch = torch_g1.entropy().item()
        assert np.allclose(entropy_me, entropy_torch, atol=prec), \
            f"Entropy H(Gauss) failed:\n\n    Expected={entropy_torch}, Computed={entropy_me}\n"

        
    print("\nAll tests passed!\n")
    
