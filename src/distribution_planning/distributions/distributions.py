import sys
import os.path as osp
import numpy as np
from pyquaternion import Quaternion
from copy import deepcopy
import torch
import torch.distributions as td
import matplotlib.pyplot as plt
from matplotlib import patches, cm
from tqdm import tqdm

from distribution_planning.util import vis_util, math_util
from ll4ma_util import file_util, func_util

pydrake = func_util.silent_import('pydrake')
pybingham = func_util.silent_import('pybingham')
    

class Distribution:

    def __init__(self, name=''):
        self.name = name
        self.artists = {}


class Gaussian(Distribution):

    def __init__(self, mean=None, cov=None, cov_tril=None, name='g'):
        super().__init__(name)
        
        self.mean = mean
        if cov is not None and cov_tril is not None:
            raise ValueError("Must specify exactly one of cov or cov_tril")
        self.cov = cov
        self.cov_tril = cov_tril
        self.name = name
        self._torch_distribution = None

    def log_pdf(self, x):
        Sigma = self.get_cov()
        Sigma_inv = pydrake.math.inv(Sigma)
        N = self.size()
        diff = x - self.get_mean()
        mahal = np.dot(diff, np.dot(Sigma_inv, diff))
        return -0.5 * (N * np.log(2*np.pi) + mahal + np.log(math_util.det(Sigma)))

    def pdf(self, x):
        return np.exp(self.log_pdf(x))
    
    def sample(self, n_samples=1):
        # TODO huge hack so I don't need to implement this right now
        return self.as_torch().sample((n_samples,)).numpy().squeeze()

    def fit(self, data):
        # Assumes data is (n_samples, size)
        self.mean = np.mean(data, axis=0)
        self.cov = np.cov(data.T)
    
    def get_mean(self):
        if self.mean is None:
            raise ValueError("Mean has not been set or computed yet")
        return self.mean
    
    def set_mean(self, point):
        self.mean = point

    def size(self):
        return len(self.mean)

    def get_cov(self):
        if self.cov is None and self.cov_tril is None:
            raise ValueError("Neither cov or cov_tril are defined")
        elif self.cov is None:
            self.cov = np.dot(self.cov_tril, self.cov_tril.T)            
        return self.cov
    
    def get_cov_tril(self, matrix=False):
        if self.cov_tril is None and self.cov is None:
            raise ValueError("Neither cov or cov_tril are defined")
        elif self.cov_tril is None:
            self.cov_tril = math_util.cholesky(self.cov)
        return self.cov_tril if matrix else self.cov_tril[np.tril_indices(self.size())].flatten()
        
    def get_cov_diag(self, matrix=False):
        diag = np.diag(self.get_cov())
        if matrix:
            diag = np.diag(diag)
        return diag

    def get_sigma_points(self, beta=2, include_mean=False):
        return math_util.sigma_points(self.get_mean(), L=self.get_cov_tril(True), beta=beta,
                                      include_mean=include_mean)

    def as_torch(self, diag=False, batch_size=0, clear_cached=False):
        """
        Returns the distribution as a PyTorch distribution. Will cache a version so it's
        not re-created on subsequent calls, can force recreation with clear_cached=True.
        """
        if clear_cached:
            self._torch_distribution = None
        if self._torch_distribution is None or batch_size != self._torch_distribution.loc.size(0):
            if diag:
                mean = torch.tensor(self.get_mean().copy())
                scale = torch.tensor(self.get_cov_diag().copy()).sqrt()
                if batch_size > 0:
                    mean = mean.unsqueeze(0).repeat(batch_size, 1)
                    scale = scale.unsqueeze(0).repeat(batch_size, 1)
                self._torch_distribution = td.Normal(mean, scale)
            else:
                mean = torch.tensor(self.get_mean().copy()).float()
                cov = torch.tensor(self.get_cov().copy()).float()
                if batch_size > 0:
                    mean = mean.unsqueeze(0).repeat(batch_size, 1)
                    cov = cov.unsqueeze(0).repeat(batch_size, 1, 1)
                self._torch_distribution = td.MultivariateNormal(mean, cov)
        return self._torch_distribution

    def draw(self, ax, idxs=None, n_std=5, cmap='Blues', alpha=0.7, mean_c='yellow', mean_s=80,
             ec='slategrey', show_only_std=None):
        mean = self.get_mean()
        cov = self.get_cov()
        if idxs is not None:
            mean = mean[idxs]
            cov = cov[np.ix_(idxs, idxs)]
        cmap = cm.get_cmap(cmap, n_std)
        colors = [cmap(v) for v in np.linspace(0, 1, n_std)]

        self.artists = {}
        if mean_s > 0:
            self.artists[f'{self.name}_mean'] = ax.scatter(*mean, c=mean_c, s=mean_s, zorder=100)
        for i, std in enumerate(range(n_std, 0, -1)):
            if show_only_std is not None and std != show_only_std:
                continue
            w, h, th = vis_util.get_cov_ellipse(cov, std)
            ellipse = patches.Ellipse(mean, w, h, th, fc=colors[i], ec=ec, alpha=alpha)
            ax.add_artist(ellipse)
            self.artists[f'{self.name}_std_{std}'] = ellipse
        return self.artists

    def draw_update(self, ax, n_std=5):
        mean = self.get_mean()
        cov = self.get_cov()
        self.artists[f'{self.name}_mean'].set_offsets(mean)
        for i, std in enumerate(range(n_std, 0, -1)):
            vis_util.update_ellipse(self.artists[f'{self.name}_std_{std}'], mean, cov, std)


class TruncatedGaussian(Gaussian):

    def __init__(self, low, high, mean=None, cov=None, cov_tril=None, name='g'):
        super().__init__(mean, cov, cov_tril, name)
        self.low = low
        self.high = high

    def pdf(self, x):
        low = self.get_low().astype(x.dtype)
        high = self.get_high().astype(x.dtype)
        in_region = np.prod(np.greater_equal(x, low) * np.less_equal(x, high))
        if isinstance(x[0], pydrake.autodiffutils.AutoDiffXd):
            in_region = pydrake.autodiffutils.AutoDiffXd(in_region)
        pdf = in_region * super().pdf(x)
        return pdf

    def get_low(self):
        return self.low

    def get_high(self):
        return self.high

    def draw(self, ax, idxs=None, n_std=5, cmap='Greens', alpha=0.7, mean_c='yellow', mean_s=80,
             ec='slategrey'):
        super().draw(ax, idxs, n_std, cmap, alpha, mean_c, mean_s, ec)

        
class DiracDelta(Distribution):

    def __init__(self, point, name=''):
        super().__init__(name)
        self.point = point

    def get_point(self):
        return self.point

    def get_mean(self):
        return self.point

    def set_mean(self, point):
        self.point = point

    def size(self):
        return len(self.point)

    def draw(self, ax, idxs=None, c='dodgerblue', s=300):
        point = self.get_point()
        if idxs is not None:
            point = point[idxs]
        return ax.scatter(*point, c=c, s=s, zorder=1000, edgecolors='xkcd:deep sea blue')
        
    
class Uniform(Distribution):

    def __init__(self, low, high, name=''):
        super().__init__(name)
        self.low = low
        self.high = high

    def log_pdf(self, x):
        # TODO probably shouldn't do this, most values will be infinite
        return np.log(self.pdf(x))
        
    def pdf(self, x):
        # TODO need to fix this, wasn't working for drake types
        low = self.get_low().astype(x.dtype)
        high = self.get_high().astype(x.dtype)
        in_region = np.prod(np.greater_equal(x, low) * np.less_equal(x, high))
        if isinstance(x[0], pydrake.autodiffutils.AutoDiffXd):
            in_region = pydrake.autodiffutils.AutoDiffXd(in_region)
        pdf = in_region / np.prod(high - low)
        return pdf

    def get_low(self):
        return self.low

    def get_high(self):
        return self.high

    def get_mean(self):
        return (self.get_low() + self.get_high()) / 2.

    def get_sigma_points(self, include_mean=True):
        N = self.size()
        N_sigma = 2*N + 1 if include_mean else 2*N
        pts = np.expand_dims(self.get_mean(), 0).repeat(N_sigma, axis=0)
        for i in range(N):
            pts[i,i] = self.low[i]
            pts[i+N,i] = self.high[i]
        return pts
        
    def size(self):
        return len(self.low)
                                                      
    def as_torch(self):
        return td.Uniform(torch.tensor(self.get_low().copy()),
                          torch.tensor(self.get_high().copy()))

    def draw(self, ax, idxs=None, fc='xkcd:bluish', ec='xkcd:light grey blue',
             hatch='\\', alpha=1):
        low = self.get_low()
        high = self.get_high()
        if idxs is not None:
            low = low[idxs]
            high = high[idxs]
        x_extent = high[0] - low[0]
        y_extent = high[1] - low[1]
        rect = patches.Rectangle(low, x_extent, y_extent, fc=fc, ec=ec, hatch=hatch, alpha=alpha,
                                 zorder=0)
        ax.add_patch(rect)
        return [rect]


class GMM(Distribution):

    def __init__(self, weights, means, covs=None, cov_trils=None, name=''):
        super().__init__(name)
        
        if sum(weights) != 1:
            raise ValueError(f"Weights must sum to 1, but they sum to {sum(weights)}")
        if (len(weights) != len(means) or
            covs is not None and len(weights) != len(covs) or
            cov_trils is not None and len(weights) != len(cov_trils)):
            raise ValueError(f"All parameter lists must have same length")
        
        self.weights = weights
        self.components = []
        if covs is not None:
            for m, c in zip(means, covs):
                self.components.append(Gaussian(m, cov=c))
        elif cov_trils is not None:
            for m, ct in zip(means, cov_trils):
                self.components.append(Gaussian(m, cov_tril=ct))
        else:
            raise ValueError("Must specify exactly one of covs or cov_trils")

    def log_pdf(self, x):
        c_vals = [c.log_pdf(x) + np.log(w) for w, c in self.get_components()]
        # sklearn does a logsumexp of these values, but I don't really see the point.
        # I have an issue with numerical precision when the PDF is too small, you'll
        # get infinity when you take the log. I think they do some numerical trickery
        # in their LSE function, so if you need that look at their implementation.
        # I'm just taking max for now - the values match well.
        
        # return np.log(sum([np.exp(c_val) for c_val in c_vals])) # LSE
        return max(c_vals)

    def get_components(self):
        return zip(self.weights, self.components)

    def draw(self, ax, idxs=None):
        # TODO can add other draw options to pass to Gaussian components
        artists = []
        for c in self.components:
            artists += c.draw(ax, idxs)
        return artists


class UMM(Distribution):
    '''
    Uniform Mixture Model, i.e. a mixture model of uniform distributions to
    probabilistically represent a collection of disjoint sets with associated
    weightings. Not sure if this has been looked at much in literature.
    '''
    def __init__(self, weights, lows, highs, name=''):
        super().__init__(name)

        if sum(weights) != 1:
            raise ValueError(f"Weights must sum to 1, but they sum to {sum(weights)}")
        if len(weights) != len(lows) or len(weights) != len(highs):
            raise ValueError(f"All parameter lists must have same length")

        self.weights = weights
        self.components = [Uniform(l, h) for l, h in zip(lows, highs)]

    def log_pdf(self, x):
        # TODO this is just mimicking the GMM code, not sure if antying else should be done
        c_vals = [c.log_pdf(x) + np.log(w) for w, c in self.get_components()]
        return max(c_vals)

    def get_components(self):
        return zip(self.weights, self.components)

    def draw(self, ax, idxs=None):
        # TODO can add other draw options to pass to components
        artists = []
        for c in self.components:
            artists += c.draw(ax, idxs)
        return artists


class PoseDistribution(Distribution):

    def __init__(self, gaussian, bingham, name=''):
        super().__init__(name)
        self.gauss = gaussian
        self.bingham = bingham

    def sample(self, n_samples=1):
        pos_samples = self.gauss.sample(n_samples).squeeze()
        rot_samples = self.bingham.sample(n_samples).squeeze()
        return np.concatenate([pos_samples, rot_samples], axis=-1)

    def get_sigma_points(self):
        # TODO this is hacked for now to just take SPs for gaussian pos
        # and then give each of them the mode orientation. Need to figure
        # out how to do SPs for Bingham, and also how to do unified pos/rot
        pos_sps = self.gauss.get_sigma_points()
        rot_mode = self.bingham.mode
        sps = []
        for sp in pos_sps:
            sps.append(np.concatenate([sp, rot_mode]))
        return np.array(sps)

    def log_pdf(self, x):
        # TODO I'm not sure if this is correct
        pos_log_pdf = self.gauss.log_pdf(x[:3])
        rot_log_pdf = self.bingham.log_pdf(x[3:])
        return pos_log_pdf + rot_log_pdf
        

class PoseMM(Distribution):
    # TODO I think you can make a more generic mixture model since there isn't much
    # difference between this, GMM, UMM, etc. Similar to MixtureSameFamily for torch
    
    def __init__(self, weights=[], pose_distributions=[], name=''):
        super().__init__(name)

        if weights and not np.allclose(sum(weights), 1.):
            raise ValueError(f"Weights must sum to 1, but they sum to {sum(weights)}")
        if weights and pose_distributions and len(weights) != len(pose_distributions):
            raise ValueError(f"Weights must be same length as number of distributions")

        self.weights = weights
        self.components = pose_distributions
        self._set_categorical()
        
    def log_pdf(self, x):
        # Same as GMM, see comments there
        c_vals = [c.log_pdf(x) + np.log(w) for w, c in self.get_components()]
        return max(c_vals)

    def get_components(self):
        return zip(self.weights, self.components)

    def sample(self, n_samples=1):
        idxs = self.categorical.sample((n_samples,))
        samples = []
        for idx in idxs:
            samples.append(self.components[idx].sample())
        return np.array(samples)

    def init_from_params(self, params, tf=None):
        self._orig_params = deepcopy(params)
        self.weights = []
        self.components = []
        for name, c_params in params['components'].items():
            v1 = c_params['v1']
            v2 = c_params['v2']
            v3 = c_params['v3']
            z1 = c_params['z1']
            z2 = c_params['z2']
            z3 = c_params['z3']
            pos = c_params['pos_mean']
            cov = c_params['pos_cov']
            
            if tf is not None:
                # Rotate params to new frame if TF was provided
                
                q = tf.rotation().ToQuaternion()
                v1 = q.multiply(pydrake.common.eigen_geometry.Quaternion(v1)).wxyz()
                v2 = q.multiply(pydrake.common.eigen_geometry.Quaternion(v2)).wxyz()
                v3 = q.multiply(pydrake.common.eigen_geometry.Quaternion(v3)).wxyz()

                bingham_mode = pydrake.common.eigen_geometry.Quaternion(c_params['rot_mode'])
                c_tf = pydrake.math.RigidTransform(p=pos, quaternion=bingham_mode)
                new_tf = tf.multiply(c_tf)
                pos = new_tf.translation()

                R = tf.rotation().matrix()
                cov = R @ cov @ R.T  # Cov is already cov_O_F
                
            bingham = pybingham.Bingham(v1, v2, v3, z1, z2, z3)
            bingham.compute_stats()            
            gauss = Gaussian(pos, cov)
            self.components.append(PoseDistribution(gauss, bingham, name))
            self.weights.append(c_params['weight'])
        self._set_categorical()

    def save_params(self, filename):
        # TODO this might get confusing. I basically wanted to save params before
        # transformation (in init_from_params) because the whole point of doing
        # this is I could load them from file and, given a new object pose, transform
        # to a new frame on creation. But this is saying if you didn't create it
        # that way, it will save the actual params from components. Should figure
        # out a cleaner way to handle all this.
        if self._orig_params is not None:
            params = self._orig_params
        else:
            params = {'components': {}}
            for w, c in zip(self.weights, self.components):
                if c.name in params:
                    raise ValueError("Already have params for a component with name "
                                     f"{c.name}, make sure components have unique names")
                params['components'][c.name] = {
                    'weight': w, 'rot_mode': c.bingham.mode,
                    'v1': c.bingham.V[0], 'v2': c.bingham.V[1], 'v3': c.bingham.V[2],
                    'z1': c.bingham.Z[0], 'z2': c.bingham.Z[1], 'z3': c.bingham.Z[2],
                    'pos_mean': c.gauss.get_mean(), 'pos_cov': c.gauss.get_cov()
                }
        file_util.save_pickle(params, filename)
        print(f"PoseMM parameters saved to: {filename}")

    def _set_categorical(self):
        if len(self.weights) > 0:
            self.categorical = td.Categorical(torch.tensor(self.weights))


class Classifier:
    """
    Note this isn't a proper distribution but more a convenience class 
    to manage things associated with a likelihood classifier.
    """
    
    def __init__(self, cfg):
        self.cfg = cfg
        # TODO can configure these from cfg
        self.model = ClassifierModel()
        self.generate_uniform_data()
        self.train()

    def pdf(self, x):
        # This just gives the softmax prediction for the goal category,
        # note it will actually be in range [0,1]

        if x.dtype == pydrake.autodiffutils.AutoDiffXd:
            x = pydrake.autodiffutils.ExtractValue(x)  # Torch won't handle drake autodiff type
        x = torch.tensor(x).squeeze().unsqueeze(0).float()
        outputs = self.model(x)  # Adds batch dim
        return outputs.softmax(dim=1).detach().numpy().squeeze()[1]

    def log_pdf(self, x):
        return np.log(self.pdf(x))
        
    def generate_uniform_data(self, n_samples=2000):
        """
        Generates a goal/not-goal dataset by uniformly sampling the region and
        determining if the sample is in one of the goal regions.
        """        
        gen = td.Uniform(torch.tensor(self.cfg['region_lb']), torch.tensor(self.cfg['region_ub']))
        self.samples = gen.sample((n_samples,))
        # TODO for now just adding zero angle for goal
        self.samples = torch.cat([self.samples, torch.zeros(n_samples, 1)], dim=-1)
        self.labels = torch.zeros(n_samples).long()
        for i, sample in enumerate(self.samples):
            is_goal = False
            for name, region in self.cfg['goal_regions'].items():
                in_region = True
                for j in range(len(sample)):
                    if sample[j] < region['lb'][j] or sample[j] > region['ub'][j]:
                        in_region = False
                        break
                if in_region:
                    is_goal = True
                    break
            self.labels[i] = int(is_goal)
        self.dataset = ClassifierDataset(self.samples, self.labels)

    def train(self, batch_size=32, learning_rate=0.001, n_epochs=100):
        if self.dataset is None:
            raise ValueError("Training data not set")

        loader = torch.utils.data.DataLoader(self.dataset, batch_size, shuffle=True)
        params = self.model.parameters()
        optim = torch.optim.Adam(params, lr=learning_rate)

        print(f"\nTraining model for {n_epochs} epochs...")
        for epoch in tqdm(range(1, n_epochs + 1)):
            for batch_idx, (samples, labels) in enumerate(loader):
                outputs = self.model(samples)
                loss = torch.nn.functional.cross_entropy(outputs, labels)
                optim.zero_grad()
                loss.backward()
                optim.step()
        print("Training complete.")

    def draw(self, ax, *args):
        # # TODO temporary for testing data
        # fig, ax = plt.subplots()
        # fig.set_size_inches(10, 10)
        # plt.tight_layout()
        

        # low = self.cfg['region_lb']
        # high = self.cfg['region_ub']

        # ax.set_xlim(low[0], high[0])
        # ax.set_ylim(low[1], high[1])

        # x_extent = high[0] - low[0]
        # y_extent = high[1] - low[1]
        # rect = patches.Rectangle(low, x_extent, y_extent, fc='none', ec='b')
        # ax.add_patch(rect)
        
        # for name, region in self.cfg['goal_regions'].items():
        #     low = region['lb']
        #     high = region['ub']
        #     x_extent = high[0] - low[0]
        #     y_extent = high[1] - low[1]
        #     rect = patches.Rectangle(low, x_extent, y_extent, fc='none', ec='r')
        #     ax.add_patch(rect)

        samples = self.samples.numpy()
        labels = self.labels.numpy()
        goal_idxs = np.where(labels == 1)
        # no_goal_idxs = np.where(labels == 0)
        
        plt.scatter(samples[goal_idxs,0], samples[goal_idxs,1], color='dodgerblue', s=35,
                    zorder=0)
        # plt.scatter(samples[no_goal_idxs,0], samples[no_goal_idxs,1], color='b')
            
        # plt.show()
    

class ClassifierDataset(torch.utils.data.Dataset):

    def __init__(self, samples, labels):
        self.samples = samples
        self.labels = labels

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx], self.labels[idx]


class ClassifierModel(torch.nn.Module):

    def __init__(self):
        super().__init__()
        self.layers = torch.nn.ModuleList()
        self.layers.append(torch.nn.Linear(3, 32))
        self.layers.append(torch.nn.ReLU())
        self.layers.append(torch.nn.Linear(32, 32))
        self.layers.append(torch.nn.ReLU())
        self.layers.append(torch.nn.Linear(32, 2))
        
    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x
            
    
def get_goal_distribution(cfg):
    if cfg['goal_distribution'] == 'gaussian':
        pG = Gaussian(np.array(cfg['goal_point']),
                      get_cov(cfg['goal_cov'], len(cfg['goal_point'])))
    elif cfg['goal_distribution'] == 'truncated_gaussian':
        pG = TruncatedGaussian(np.array(cfg['goal_lb']), np.array(cfg['goal_ub']),
                               np.array(cfg['goal_point']),
                               get_cov(cfg['goal_cov'], len(cfg['goal_point'])))
    elif cfg['goal_distribution'] == 'dirac':
        pG = DiracDelta(np.array(cfg['goal_point']))
    elif cfg['goal_distribution'] == 'uniform':
        pG = Uniform(np.array(cfg['goal_lb']), np.array(cfg['goal_ub']))
    elif cfg['goal_distribution'] == 'gmm':
        covs = [get_cov(c, len(cfg['goal_points'][0])) for c in cfg['goal_covs']]
        pG = GMM(cfg['goal_weights'], np.array(cfg['goal_points']), covs)
    elif cfg['goal_distribution'] == 'umm':
        pG = UMM(cfg['goal_weights'], np.array(cfg['goal_lbs']), np.array(cfg['goal_ubs']))
    # TODO need to update others to have this nested structure, will make things much
    # more nicely organized in the file and avoids polluting namespace
    elif cfg['goal_distribution']['type'] == 'classifier':
        pG = Classifier(cfg['goal_distribution'])
    else:
        raise ValueError(f"Unknown goal distribution type: {cfg['goal_distribution']}")
    return pG


def get_cov(val, ndims):
    if isinstance(val, float):
        return val * np.eye(ndims)
    elif isinstance(val, list):
        val = np.array(val)
        if val.ndim == 1:
            if len(val) != ndims:
                raise ValueError(f"Wrong size for diag cov, expected ({ndims},) "
                                 f"but got {val.shape}")
            return np.diag(val)
        elif val.ndim == 2:
            if val.shape[0] != ndims or val.shape[1] != ndims:
                raise ValueError(f"Wrong size for cov, expected ({ndims}, {ndims}) "
                                 f"but got {val.shape}")
            return val
        else:
            raise ValueError(f"Invalid number of dims for cov: {val.ndim}")
    else:
        raise ValueError(f"Unknown type for cov: {type(val)}")
    
    
if __name__ == '__main__':

    CONFIG_ROOT = osp.join(osp.dirname(osp.dirname(osp.abspath(__file__))), "config")
    cfg_fn = osp.join(CONFIG_ROOT, "dubins_example_classifier.yaml")
    cfg = file_util.load_yaml(cfg_fn)

    classifier = Classifier(cfg['goal_distribution'])
    classifier.generate_uniform_data(50)

    for i, sample in enumerate(classifier.samples):
        print("PDF", classifier.pdf(sample),
              "LOG", classifier.log_pdf(sample),
              "LABEL", classifier.labels[i].item())
    # print("PRED", preds.softmax(dim=1).round().long().detach().numpy()[:,1])
    # print("TRUE", classifier.labels.numpy())

    
    # import sys
    # import torch
    # from torch.distributions import MultivariateNormal as MVN
    # from scipy.stats import invwishart as IW
    # from sklearn.mixture import GaussianMixture as GMM_sklearn
    # from sklearn.mixture._gaussian_mixture import _compute_precision_cholesky
    
    # n_tests = 20
    # d = 10
    # prec = 1e-7

    # for _ in range(n_tests):
    #     mu = np.random.randn(d)
    #     Sigma = IW.rvs(d, 0.1 * np.eye(d))
    #     x = np.random.randn(d)
    #     g = Gaussian(mu, Sigma)
    #     torch_g = g.as_torch()
    #     lp_me = g.log_pdf(x)
    #     lp_torch = torch_g.log_prob(torch.tensor(x)).item()
    #     assert np.allclose(lp_me, lp_torch, rtol=prec), \
    #         f"Gaussian log_prob differs:\n\n    Expected={lp_torch}, Computed={lp_me}\n"


    #     mu1 = np.random.randn(d)
    #     mu2 = np.random.randn(d)
    #     Sigma1 = IW.rvs(d, 0.1 * np.eye(d))
    #     Sigma2 = IW.rvs(d, 0.1 * np.eye(d))
    #     w1 = np.random.rand()
    #     weights = [w1, 1. - w1]
    #     gmm = GMM(weights, [mu1, mu2], [Sigma1, Sigma2])
    #     gmm_skl = GMM_sklearn(n_components=2)
    #     gmm_skl.weights_ = np.array(weights)
    #     gmm_skl.means_ = np.stack([mu1, mu2])
    #     gmm_skl.covariances_ = np.stack([Sigma1, Sigma2])
    #     gmm_skl.precisions_cholesky_ = _compute_precision_cholesky(gmm_skl.covariances_, 'full')
    #     # gmm_skl.fit()
    #     gmm_lp_me = gmm.log_pdf(x)
    #     gmm_lp_sk = gmm_skl.score(np.expand_dims(x, 0))
    #     # These aren't always totally precise, computed slightly differently.
    #     # But seems always close even when different
    #     assert np.allclose(gmm_lp_me, gmm_lp_sk, rtol=1e-2), \
    #         f"GMM log_prob differs:\n\n    Expected={gmm_lp_me}, Computed={gmm_lp_sk}\n"
        
    # print("\nAll tests passed!\n")
    
