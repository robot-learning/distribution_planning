#!/usr/bin/env python
import sys
import os.path as osp
import re
import rospy
import tf2_ros
import numpy as np
from matplotlib import cm
from pyquaternion import Quaternion
from geometry_msgs.msg import TransformStamped
from visualization_msgs.msg import Marker, MarkerArray
from moveit_msgs.msg import DisplayRobotState

from ll4ma_util import ros_util, file_util, vis_util


CONFIG_ROOT = osp.join(file_util.dir_of_file(__file__, 1), "config")


def get_marker_msg(data, obj_name, component):
    cmap = cm.get_cmap('viridis', 100)
    colors = [cmap(v) for v in np.linspace(0, 1, 101)]
    
    obj_pos = data['pos']
    obj_quat = data['quat']
    reachable_pct = data['components'][component]['reachable_pct']
    color = colors[reachable_pct]
    
    obj_quat = Quaternion(*obj_quat) * q_tf
    obj_quat = [obj_quat.x, obj_quat.y, obj_quat.z, obj_quat.w]  # For ROS
    marker_id = int(regex.findall(obj_name)[0])
    obj_msg = ros_util.get_marker_msg(obj_pos, obj_quat, shape='mesh', mesh_resource=obj_mesh,
                                      color=color, alpha=0.9, marker_id=marker_id)
    return obj_msg


if __name__ == '__main__':
    rospy.init_node('visualize_obj_reachability')

    reachable_dir = osp.expanduser("~/tro_results/data/arm_reaching/reachable")
    obj_data = {}
    regex = re.compile(r'\d+')
    for fn in file_util.list_dir(reachable_dir, '.pickle'):
        idx = regex.findall(fn)[0]
        data = file_util.load_pickle(fn)
        for k, v in data.items():
            obj_data[f'{k}_{idx}'] = v

    # Get a msg for the table
    cfg = file_util.load_yaml(osp.join(CONFIG_ROOT, "drake_iiwa.yaml"))
    table_data = cfg['objects']['table']
    # I had to make up z value since they don't correspond between drake and here
    table_scale = [table_data['x_extent'], table_data['y_extent'], 0.37]
    table_msg = ros_util.get_marker_msg(table_data['position'], scale=table_scale,
                                        color=vis_util.get_color('tan'), alpha=1)
            
    obj_mesh = "package://distribution_planning/src/distribution_planning/" \
        "assets/004_sugar_box_textured.obj"

    # Need the inverse rotation since spawning in drake had initially rotated it
    q_tf = Quaternion(matrix=np.array([[ 0.,  0., 1.],
                                       [-1.,  0., 0.],
                                       [ 0., -1., 0.]]).T)

    left_pub = rospy.Publisher("/left", MarkerArray, queue_size=1)
    right_pub = rospy.Publisher("/right", MarkerArray, queue_size=1)
    front_pub = rospy.Publisher("/front", MarkerArray, queue_size=1)
    back_pub = rospy.Publisher("/back", MarkerArray, queue_size=1)
    top1_pub = rospy.Publisher("/top1", MarkerArray, queue_size=1)
    top2_pub = rospy.Publisher("/top2", MarkerArray, queue_size=1)
    table_pub = rospy.Publisher("/table", Marker, queue_size=1)
    
    left_array = MarkerArray()
    right_array = MarkerArray()
    front_array = MarkerArray()
    back_array = MarkerArray()
    top1_array = MarkerArray()
    top2_array = MarkerArray()
    
    for obj_name, data in obj_data.items():
        left_array.markers.append(get_marker_msg(data, obj_name, 'left'))
        right_array.markers.append(get_marker_msg(data, obj_name, 'right'))
        front_array.markers.append(get_marker_msg(data, obj_name, 'front'))
        back_array.markers.append(get_marker_msg(data, obj_name, 'back'))
        top1_array.markers.append(get_marker_msg(data, obj_name, 'top1'))
        top2_array.markers.append(get_marker_msg(data, obj_name, 'top2'))
        
    

    rate = rospy.Rate(100)
    rospy.loginfo("Visualization is active...")
    while not rospy.is_shutdown():
        left_pub.publish(left_array)
        right_pub.publish(right_array)
        front_pub.publish(front_array)
        back_pub.publish(back_array)
        top1_pub.publish(top1_array)
        top2_pub.publish(top2_array)
        table_pub.publish(table_msg)
        rate.sleep()
        
