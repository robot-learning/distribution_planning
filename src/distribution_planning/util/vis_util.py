import numpy as np


def get_cov_ellipse(cov, n_std=2):
    """
    Gets the width, height, angle params for the ellips associated 
    with the n_std standard deviation of a covariance matrix.

    See: https://stackoverflow.com/a/12321306/3711266
    """
    vals, vecs = np.linalg.eigh(cov)
    order = vals.argsort()[::-1]
    vals, vecs = vals[order], vecs[:,order]
    theta = np.degrees(np.arctan2(*vecs[::-1,0]))
    width, height = 2 * n_std * np.sqrt(vals)
    return width, height, theta


def update_ellipse(ellipse, mu, sigma, n_std=2):
    """
    Updates a matplotlib patches.Ellipse object associated with a covariance.
    """
    width, height, theta = get_cov_ellipse(sigma, n_std)
    ellipse.set_center(mu)
    ellipse.set_width(width)
    ellipse.set_height(height)
    ellipse.set_angle(theta)



