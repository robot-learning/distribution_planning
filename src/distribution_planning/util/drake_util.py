import numpy as np
from tqdm import tqdm

import pydrake.solvers.mathematicalprogram as mp
from pydrake.solvers.snopt import SnoptSolver as SNOPT
from pydrake.math import RigidTransform, RotationMatrix
from pydrake.common.eigen_geometry import Quaternion
from manipulation.scenarios import AddTriad


def add_constraint(prog, func, lb, ub, vars, name=''):
    constraint = prog.AddConstraint(func, lb=lb, ub=ub, vars=vars)
    if name:
        constraint.evaluator().set_description(name)


def add_bb_constraint(prog, lb, ub, vars, name=''):
    constraint = prog.AddBoundingBoxConstraint(lb, ub, vars)
    if name:
        constraint.evaluator().set_description(name)


def add_cost(prog, func, vars, name=''):
    cost = prog.AddCost(func, vars=vars)
    if name:
        cost.evaluator().set_description(name)


def get_snopt_options(max_iterations=100, log_filename="/tmp/snopt.out"):
    options = mp.SolverOptions()
    options.SetOption(mp.CommonSolverOption.kPrintFileName, log_filename)
    options.SetOption(SNOPT.id(), "Major iterations limit", max_iterations)
    options.SetOption(SNOPT.id(), "Print frequency", 1)
    return options


def solve(prog, solver_options=None, verbose=True):
    if verbose:
        print("\nRunning solver...")

    log_filename = solver_options.common_solver_options()[mp.CommonSolverOption.kPrintFileName]
    snopt_options = solver_options.GetOptions(SNOPT.id())
    max_iterations = snopt_options["Major iterations limit"]

    if verbose:
        pbar = tqdm(total=max_iterations, ncols=80)
    log = open(log_filename, 'w+')
    xs = []
    
    def pbar_callback(x):
        # Update progress bar only for SNOPT major iterations
        lines = log.read()
        idx = lines.rfind('Itns Major Minors')
        if idx > -1:
            xs.append(x.copy())
            header, info = lines[idx:].split('\n')[:2]
            for name, val in zip(header.split(), info.split()):
                if name != 'Major':
                    continue
                if verbose:
                    pbar.n = int(val)
                    pbar.refresh()
                break
    prog.AddVisualizationCallback(pbar_callback, prog.decision_variables())

    result = mp.Solve(prog, solver_options=solver_options)
    log.close()
    if verbose:
        pbar.close()
        print(f"\nSuccess? {result.is_success()}")
        print(f"Solver: {result.get_solver_id().name()}\n")

        names = []
        if not result.is_success():
            print("INFEASIBLE:")
            infeasible = result.GetInfeasibleConstraints(prog)
            for c in infeasible:
                name = c.evaluator().get_description()
                if name not in names:
                    print(f"  {name}")
                    names.append(name)
        print()

    xs.append(result.GetSolution())
            
    return result, np.array(xs)


def get_decision_var(prog, x, v):
    return x[prog.FindDecisionVariableIndices(v.flatten())].reshape(v.shape)


def get_tf(position, rotation=None, quaternion=None):
    # TODO can support other rotations
    if rotation is not None:
        R = RotationMatrix(rotation)
        tf = RigidTransform(R, position)
    elif quaternion is not None:
        q = Quaternion(quaternion)
        tf = RigidTransform(q, position)
    else:
        tf = RigidTransform(position)
    return tf


def draw_frame(frame, plant, scene_graph, tf=None, length=0.25, radius=0.01,
               alpha=1., name="frame"):
    tf = frame.GetFixedPoseInBodyFrame() if tf is None else tf
    source_id = plant.get_source_id()
    frame_id = plant.GetBodyFrameIdOrThrow(frame.body().index())
    AddTriad(source_id, frame_id, scene_graph, length, radius, alpha, tf, name)

