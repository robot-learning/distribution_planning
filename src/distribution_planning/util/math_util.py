import sys
import numpy as np
import torch
from pyquaternion import Quaternion


def cholesky(A):
    '''
    Computes Cholesky decomposition of A.

    Based on this implementation: https://rosettacode.org/wiki/Cholesky_decomposition#Python
    This is only guaranteed to work if A is symmetric positive definite.
    '''
    L = np.zeros(A.shape, dtype=A.dtype)
    for i in range(len(A)):
        for j in range(i+1):
            s = sum(L[i][k] * L[j][k] for k in range(j))
            L[i][j] = np.sqrt(A[i][i] - s) if (i == j) else (1.0 / L[j][j] * (A[i][j] - s))
    return L


def det(A):
    '''
    Computes determinate of a square matrix. This only works if A is symmetric positive definite.
    '''
    L = cholesky(A)  # LU decomposition of SPD matrix where U = L.T
    return np.prod(np.diag(L))**2


def pose_error(p_desired, R_desired, p_actual, R_actual):
    pos_error = np.linalg.norm(p_desired - p_actual)
    rot_error = orientation_error(R_desired, R_actual)
    return pos_error + rot_error
    

def orientation_error(R_desired, R_actual):
    # TODO this is part of the matrix log but I'm not convinced yet it's
    # correct to just take theta as there's more to that computation to
    # actually get the vector, but trying this for now
    R_diff = R_actual.T @ R_desired
    theta = np.arccos(0.5 * (np.trace(R_diff) - 1) * (1. - 1e-10))
    return theta


def vec2tril(v):
    M = len(v)
    N = np.floor(np.sqrt(2*M)).astype(int)
    computed_M = int(N*(N+1) / 2.)
    if computed_M != M:
        raise ValueError(f"Expected vector of length {computed_M} for N={N} but got length {M}")
    T = np.zeros((N, N), dtype=v.dtype)
    T[np.tril_indices(N)] = v
    return T


def batch_vec2tril(v):
    B, M = v.shape
    N = np.floor(np.sqrt(2*M)).astype(int)
    computed_M = int(N*(N+1) / 2.)
    if computed_M != M:
        raise ValueError(f"Expected vector of length {computed_M} for N={N} but got length {M}")
    L = torch.zeros(B, N, N).double()
    tril_idxs = np.tril_indices(N)
    L[:,tril_idxs[0],tril_idxs[1]] = v
    return L


def batch_tril2cov(L):
    if L.dim() != 3 or L.size(1) != L.size(2):
        raise ValueError(f"Input tensor must have shape (B, N, N) but got shape {L.size()}")
    return L @ L.transpose(1, 2)


def batch_cov(points):
    B, N, D = points.size()
    mean = points.mean(dim=1).unsqueeze(1)
    diffs = (points - mean).reshape(B * N, D)
    prods = torch.bmm(diffs.unsqueeze(2), diffs.unsqueeze(1)).reshape(B, N, D, D)
    bcov = prods.sum(dim=1) / (N - 1)
    return bcov


def sigma_points(mu, Sigma=None, L=None, beta=2, include_mean=True):
    if Sigma is None and L is None:
        raise ValueError("Must specify exactly one of 'Sigma' or 'L'")
    elif Sigma is not None:
        L = cholesky(Sigma)
    N = len(mu)
    size = 2*N + 1 if include_mean else 2*N
    sps = np.repeat(np.expand_dims(mu, 0), size, axis=0)
    for i in range(N):
        sps[i,:] += beta * L[i,:]
        sps[N+i,:] -= beta * L[i,:]
    return sps


def unscented_transform(x, N, g, beta=2, R=None):
    # TODO could infer N but need to compute tril number based on length
    n_sigma = 2 * N

    mu = x[:N]
    L = vec2tril(x[N:])
    
    sigma_points_prime = np.zeros((n_sigma, N), dtype=x.dtype)
    for i in range(N):
        sigma_points_prime[i,:] = g(mu + beta * L[i,:])
        sigma_points_prime[N+i,:] = g(mu - beta * L[i,:])

    mu_prime = np.mean(sigma_points_prime, axis=0)
    sigma_prime = np.zeros((N, N), dtype=x.dtype)
    for i in range(n_sigma):
        x = sigma_points_prime[i,:] - mu_prime
        sigma_prime += np.outer(x, x) / (2. * beta**2)
        
    if R is not None:
        sigma_prime += R
        
    L_prime = cholesky(sigma_prime)[np.tril_indices(N)]  # Computes matrix square root

    result = np.array(np.concatenate([mu_prime, L_prime], axis=-1)).flatten()
    return result


def batch_unscented_transform(x, g, beta=2, R=None):
    N = infer_size_from_mu_tril(x)
    if N is None:
        raise ValueError(f"Couldn't figure out size from batch UT input of length {x.size(-1)}")
    n_sigma = 2*N
    B = x.size(0)
    
    mu = x[:,:N]
    L = batch_vec2tril(x[:,N:])
    
    sigma_points_prime = torch.zeros((B, n_sigma, N)).double()
    for i in range(N):
        sigma_points_prime[:,i,:] = g(mu + beta * L[:,i,:])
        sigma_points_prime[:,N+i,:] = g(mu - beta * L[:,i,:])

    mu_prime = sigma_points_prime.mean(dim=1)
    sigma_prime = torch.zeros(B, N, N).double()
    for i in range(n_sigma):
        x = sigma_points_prime[:,i,:] - mu_prime
        sigma_prime += torch.bmm(x.unsqueeze(2), x.unsqueeze(1)) / (2. * beta**2)
        
    if R is not None:
        sigma_prime += R  # TODO don't know if this works for batch yet

    tril_idxs = np.tril_indices(N)
    L_prime = torch.linalg.cholesky(sigma_prime)[:,tril_idxs[0],tril_idxs[1]]
    
    return torch.cat([mu_prime, L_prime], dim=-1)


def infer_size_from_mu_tril(x, min_val=2, max_val=20):
    # Tries to infer what N is given a vector of size M = N+N*(N+1)/2. There's probably a cool
    # way to solve in closed form but I'm being lazy right now
    M = x.size(-1) if isinstance(x, torch.Tensor) and x.ndim == 2 else len(x)
    for N in range(min_val, max_val):
        if N + (N * (N+1) / 2.) == M:
            return N
    return None  # Couldn't find it


def interpolate_vectors(p1, p2, n_points):
    xs = np.linspace(0, 1, n_points)
    ps = np.stack([np.interp(xs, [0,1], np.stack([p1[i], p2[i]])) for i in range(len(p1))]).T
    return ps  # (n_points, D)


def random_quats(n_samples=100, dist_threshold=0.3, origin=[1,0,0,0]):
    if not isinstance(origin, Quaternion):
        origin = Quaternion(origin)
    X = []
    while len(X) < n_samples:
        q = Quaternion.random()
        dist = Quaternion.distance(origin, q)
        if dist < dist_threshold:
            X.append(q.elements)
    return np.stack(X)


def random_planar_pose(min_pos, max_pos, sample_axis=[1,0,0],
                       min_angle=-np.pi, max_angle=np.pi):
    pos_ranges = zip(min_pos, max_pos)
    pos = np.array([np.random.uniform(*pos_range) for pos_range in pos_ranges])
    angle = np.random.uniform(min_angle, max_angle)
    quat = Quaternion(axis=sample_axis, angle=angle).elements
    return pos, quat


if __name__ == '__main__':
    # v = np.arange(16, dtype=float).reshape(4,4)[np.tril_indices(4)] + 1
    # print("VEC", v)
    # print("TRIL", vec2tril(v))

    # vs = torch.tensor(np.expand_dims(v, 0).repeat(3, axis=0))
    # print("VS", vs.shape)
    # print("BATCH", batch_vec2tril(vs))

    # import time
    
    # B = 10000
    # N = 50
    # D = 2
    # points = torch.randn(B, N, D)
    # start = time.time()
    # my_covs = batch_cov(points)
    # print("My time:   ", time.time() - start)

    # start = time.time()
    # torch_covs = torch.zeros_like(my_covs)
    # for i, batch in enumerate(points):
    #     torch_covs[i] = batch.T.cov()

    # print("Torch time:", time.time() - start)
    # print("Same?", torch.allclose(my_covs, torch_covs, atol=1e-7))


    points = torch.randn(2, 20, 5)
    covs = batch_cov(points)
    Ls = torch.linalg.cholesky(covs)
    new_covs = batch_tril2cov(Ls)
    print("Same?", torch.allclose(covs, new_covs, atol=1e-10))
    print("OLD", covs)
    print("NEW", new_covs)
