import sys
import torch
import numpy as np

from distribution_planning.util import math_util


def linear_dynamics(x, u, A, B):
    return A.dot(x) + B.dot(u)


def batch_linear_dynamics(x, u, A, B):
    batch = x.size(0)
    return (torch.bmm(A, x.view(batch, -1, 1)) + torch.bmm(B, u.view(batch, -1, 1))).squeeze(-1)


def di_A(dt):
    return np.array([[1, 0, dt,  0],
                     [0, 1,  0, dt],
                     [0, 0,  1,  0],
                     [0, 0,  0,  1]])


def di_B(dt):
    # From Okamoto thesis
    return np.array([[ 0.5 * dt**2,  0],
                     [ 0,  0.5 * dt**2],
                     [dt,  0],
                     [ 0, dt]])


def di_dynamics(x, u, dt):
    return linear_dynamics(x, u, di_A(dt), di_B(dt))


def batch_di_dynamics(x, u, dt):
    batch = x.size(0)
    A = torch.tensor(di_A(dt)).unsqueeze(0).repeat(batch, 1, 1).double()
    B = torch.tensor(di_B(dt)).unsqueeze(0).repeat(batch, 1, 1).double()
    return batch_linear_dynamics(x, u, A, B)


def dubins_dynamics(x, u, dt):
    u += 1e-8  # Avoids divide by zero checks
    px, py, theta = x
    lin_speed, turn_rate = u
    dt_turn_rate = dt * turn_rate
    scale = lin_speed / turn_rate
    next_x = np.array([
        px + scale * (np.sin(theta + dt_turn_rate) - np.sin(theta)),
        py + scale * (np.cos(theta) - np.cos(theta + dt_turn_rate)),
        theta + dt_turn_rate
    ])
    return next_x


def batch_dubins_dynamics(x, u, dt):
    u += 1e-5  # Avoids divide by zero checks
    px, py, theta = x[:,0], x[:,1], x[:,2]
    lin_speed, turn_rate = u[:,0], u[:,1]
    dt_turn_rate = dt * turn_rate
    scale = lin_speed / turn_rate
    next_px = px + scale * (torch.sin(theta + dt_turn_rate) - torch.sin(theta))
    next_py = py + scale * (torch.cos(theta) - torch.cos(theta + dt_turn_rate))
    next_theta = theta + dt_turn_rate
    next_x = torch.stack([next_px, next_py, next_theta], dim=-1)
    return next_x


def kalman_filter(mu, Sigma, u, A, B, R, z, C, Q, dt):
    '''
    Implementation of Kalman filter following notation of [1] (Table 3.1, pg. 42)

        mu    : Previous estimate mean
        Sigma : Previous estimate covariance
        u     : Action to be applied
        A, B  : Linear dynamics control matrices
        R     : Control dynamics noise
        z     : Measurement z = Cx + delta, where delta ~ Q
        C     : Linear observation matrix
        Q     : Observation noise
        dt    : Timestep

    [1] Thrun, Sebastian, Wolfram Burgard, and Dieter Fox. Probabilistic Robotics. MIT Press, 2005.
    '''
    # print("U", u.shape)
    # print("A", A.shape)
    # print("B", B.shape)
    # print("R", R.shape)
    # print("MU", mu.shape)
    # sys.exit()
    
    # Update belief from dynamics before observation is incorporated
    mu_bar = linear_dynamics(mu, u, A, B)
    Sigma_bar = (A @ Sigma @ A.T) + R

    # Compute Kalman gain
    Sigma_bar_C_T = Sigma_bar @ C.T
    K = Sigma_bar_C_T @ np.linalg.inv(C @ Sigma_bar_C_T + Q)

    # Compute new params using observation and Kalman gain
    mu_new = mu_bar + K @ (z - C @ mu_bar)
    Sigma_new = (np.eye(len(mu)) - K @ C) @ Sigma_bar
    
    return mu_new, Sigma_new


def di_kalman_filter(mu, Sigma, u, R, z, C, Q, dt):
    return kalman_filter(mu, Sigma, u, di_A(dt), di_B(dt), R, z, C, Q, dt)


def unscented_kalman_filter(mu, Sigma, u, z, g, R, h, Q, dt, beta=2.):
    '''
    Implementation of unscented Kalman filter following notation of [1] (Table 3.4, pg. 70)

    [1] Thrun, Sebastian, Wolfram Burgard, and Dieter Fox. Probabilistic Robotics. MIT Press, 2005.
    '''
    raise NotImplementedError("TODO need to update order of args to match KF")
    N = len(mu)
    n_sps = 2*N+1
    
    X = torch.tensor(math_util.sigma_points(mu, Sigma, beta=beta))
    u = torch.tensor(u).unsqueeze(0).repeat(n_sps, 1)
    g_X = g(X, u, dt).numpy()
    mu_bar = np.mean(g_X, axis=0)
    Sigma_bar = np.zeros((N, N))
    for i in range(n_sps):
        x = X[i,:] - mu_bar
        Sigma_bar += np.outer(x, x) / (2. * beta**2)
    Sigma_bar += R

    X_bar = math_util.sigma_points(mu_bar, Sigma_bar, beta=beta)
    Z_bar = h(X_bar)
    z_hat = np.mean(Z_bar, axis=0)
    S = np.zeros((N, N))
    for i in range(n_sps):
        z = Z_bar[i,:] - z_hat
        S += np.outer(z, z) / (2. * beta**2)
    S += Q

    Sigma_xz = np.zeros((N,N))
    for i in range(n_sps):
        Sigma_xz += np.outer(X_bar[i,:] - mu_bar, Z_bar[i,:] - z_hat) / (2. * beta**2)

    K = Sigma_xz @ np.linalg.inv(S)
    mu_new = mu_bar + K @ (z - z_hat)
    Sigma_new = Sigma_bar - K @ S @ K.T
    return mu_new, Sigma_new
