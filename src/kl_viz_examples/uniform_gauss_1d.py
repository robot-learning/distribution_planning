import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from math import pi

import torch
import torch.distributions
kl = torch.distributions.kl.kl_divergence

_MIN_UNIFORM_DIST=1.e-1
_QUERY_MIN = -3
_QUERY_MAX = 3
_SAMPLE_RES = 500

def draw_dist_1D(d, x):
    '''
    Visualize a 1D PDF
    '''
    y = get_dist_1D(d,x)
    return plt.plot(x,y)

def get_dist_1D(d, x):
    '''
    Visualize a 1D PDF
    '''
    return torch.exp(d.log_prob(x))

def print_scores(p,q):
    # compute dist entropy
    h_p = p.entropy()
    h_q = q.entropy()

    # compute I-projection and M-projection scores
    i_score = kl(q,p)
    m_score = kl(p,q)

    # Compute integral of the Gaussian over the uniform distribution
    p_success = q.cdf(p.high)-q.cdf(p.low)

    print('p',p)
    print('q',q)
    print('p entropy', h_p)
    print('q entropy', h_q)
    print('i socre',i_score.item())
    print('m socre',m_score.item())
    print('p_g',p_success.item())
    print('')

def i_proj_opt_gaussian_uniform(mean=1,var=0.5774):

    # define the uniform distribution
    p = torch.distributions.uniform.Uniform(0,2,validate_args=False) 

    # max to match uniform m-projection at mean=(high+low)/2, var=sqrt((high-low)**2/12)

    # TODO: Randomly initialize the mean and variance in some meaningful way
    q = torch.distributions.normal.Normal(mean,var)

    fig, ax = plt.subplots()

    # Draw stuff
    x = torch.linspace(_QUERY_MIN, _QUERY_MAX, _SAMPLE_RES)
    p_pdf, = draw_dist_1D(p, x)
    q_pdf, = draw_dist_1D(q, x)

    print_scores(p,q)
    
    ax.set_xlabel('x')
    ax.set_ylabel('p(x)')
    ax.set_ylim([0,1.])
    ax.set_title('KL Test Tool')

    ax.margins(x=0)

    # adjust the main plot to make room for the sliders
    plt.subplots_adjust(bottom=0.35)

    # Make a horizontal slider to control the mean of the Gaussian
    ax_mean = plt.axes([0.2, 0.1, 0.65, 0.03])
    mean_slider = Slider(
        ax=ax_mean,
        label='Mean',
        valmin=-3.,
        valmax=3.,
        valinit=q.loc,
        color='Orange',
    )

    # Make a horizontal slider to control the variance of the Gaussian
    ax_var = plt.axes([0.2, 0.05, 0.65, 0.03])
    var_slider = Slider(
        ax=ax_var,
        label='Var',
        valmin=1.e-6,
        valmax=2.,
        valinit=q.scale,
        color='Orange',
    )

    def update_q(val):
        q.loc = torch.tensor([mean_slider.val])
        q.scale = torch.tensor([var_slider.val])
        q_pdf.set_ydata(get_dist_1D(q,x))
        print_scores(p,q)
        fig.canvas.draw_idle()

    # register the update function with each slider
    mean_slider.on_changed(update_q)
    var_slider.on_changed(update_q)

    # Make a horizontal slider to control the low of the uniform
    ax_low = plt.axes([0.2, 0.2, 0.65, 0.03])
    low_slider = Slider(
        ax=ax_low,
        label='Low',
        valmin=-3.,
        valmax=3.,
        valinit=p.low,
        color='Blue',
    )

    # Make a horizontal slider to control the variance of the uniform
    ax_high = plt.axes([0.2, 0.15, 0.65, 0.03])
    high_slider = Slider(
        ax=ax_high,
        label='High',
        valmin=-3.,
        valmax=3.,
        valinit=p.high,
        color='Blue',
    )
    def update_p(val):
        p.low = torch.tensor([low_slider.val])
        p.high = torch.tensor([high_slider.val])
        p_pdf.set_ydata(get_dist_1D(p,x))
        print_scores(p,q)
        fig.canvas.draw_idle()

    def update_p_low(val):
        # ensure low < high
        # TODO: These break at the slider min and max, should add some logic to handle that
        if high_slider.val < low_slider.val:
            high_slider.set_val(low_slider.val + _MIN_UNIFORM_DIST)
        update_p(val)

    def update_p_high(val):
        # ensure high > low
        if high_slider.val < low_slider.val:
            low_slider.set_val(high_slider.val - _MIN_UNIFORM_DIST)
        update_p(val)

    # register the update function with each slider
    low_slider.on_changed(update_p_low)
    high_slider.on_changed(update_p_high)

    plt.show()


def demo_2D():
    p_mean = torch.tensor([0.0, 0.0])
    p_var = torch.eye(2)
    p = torch.distributions.multivariate_normal.MultivariateNormal(p_mean,p_var)
    # mix = D.Categorical(torch.ones(5,))
    # comp = D.Independent(D.Normal(
    #     torch.randn(5,2), torch.rand(5,2)), 1)
    # q = orch.distributions.mixture_same_family.MixtureSameFamily(mix, comp)
    fig, ax = plt.subplots()

    # Draw stuff
    x_lin = torch.linspace(_QUERY_MIN, _QUERY_MAX, _SAMPLE_RES)
    y_lin = torch.linspace(_QUERY_MIN, _QUERY_MAX, _SAMPLE_RES)
    xx, yy = torch.meshgrid((x_lin, y_lin))
                                   
    X = torch.cat(tuple(torch.dstack((xx,yy))))

    P_X = get_dist_2D(p, X)
    cont_p = draw_dist_2D(x_lin, y_lin, P_X)

    # TODO: Setup a second GMM distribution
    # Q_X = get_dist_2D(q, X)
    # cont_q = draw_dist_2D(x_lin, y_lin, Q_X)

    # TODO: Define this function correctly
    # print_scores_2d(p,q)
    
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('KL Test Tool')

    ax.margins(x=0)

    # adjust the main plot to make room for the sliders
    plt.subplots_adjust(bottom=0.35, left=0.35)

    # Make a horizontal slider to control the mean of the Gaussian
    ax_mean_x = plt.axes([0.2, 0.1, 0.65, 0.03])
    mean_x_slider = Slider(
        ax=ax_mean_x,
        label='Mean X',
        valmin=-3.,
        valmax=3.,
        valinit=p.loc[0],
        color='Orange',
    )

    # Make a horizontal slider to control the variance of the Gaussian
    ax_var_x = plt.axes([0.2, 0.05, 0.65, 0.03])
    var_x_slider = Slider(
        ax=ax_var_x,
        label='Var X',
        valmin=1.e-6,
        valmax=2.,
        valinit=p.covariance_matrix[0,0],
        color='Orange',
    )

    # Make a vertical slider to control the mean of the Gaussian
    ax_mean_y = plt.axes([0.15, 0.2, 0.03, 0.65])
    mean_y_slider = Slider(
        ax=ax_mean_y,
        label='Mean Y',
        valmin=-3.,
        valmax=3.,
        valinit=p.loc[1],
        color='Orange',
        orientation='vertical'
    )

    # Make a vertical slider to control the variance of the Gaussian
    ax_var_y = plt.axes([0.05, 0.2, 0.03, 0.65])
    var_y_slider = Slider(
        ax=ax_var_y,
        label='Var Y',
        valmin=1.e-6,
        valmax=2.,
        valinit=p.covariance_matrix[1,1],
        color='Orange',
        orientation='vertical'
    )
    plt.show()
    return X, P_X

def get_dist_2D(d, X, show=False, use_full=True,show_scale=False):
    '''
    Visualize a 2D PDF
    '''
    return torch.exp(d.log_prob(X))

def draw_dist_2D(x_lin, y_lin, P_X):
    cont = plt.contour(x_lin.numpy(), y_lin.numpy(), P_X.reshape((_SAMPLE_RES,_SAMPLE_RES)))

if __name__ == '__main__':
    i_proj_opt_gaussian_uniform()    
