#!/usr/bin/env python
import sys
import numpy as np
import argparse
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

from distribution_planning.distributions import (
    Gaussian, Uniform, DiracDelta, GMM, entropy, kl_divergence as KL, NotDefinedError
)
from distribution_planning.util import vis_util


SLIDER_L = 0.2
SLIDER_W = 0.7
SLIDER_H = 0.02
SLIDER_DIFF = 0.03


def create_fig():
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(8, 10)
    ax.set_xlim(-10, 10)
    ax.set_ylim(-10, 10)
    ax.set_aspect('equal')
    plt.subplots_adjust(bottom=0.4, top=0.95)
    return fig, ax


def print_vals():
    kl_p_q = KL(p, q)
    try:
        kl_q_p = KL(q, p)
    except NotDefinedError:
        kl_q_p = 'Not Defined'
    h_q = entropy(q)
    try:
        h_p = entropy(p)
    except NotImplementedError:
        h_p = 'Entropy not implemented for p'
    ce_q_p = h_q + kl_q_p if not isinstance(kl_q_p, str) else kl_q_p  # Will give same str as KL
    ce_p_q = h_p + kl_p_q if not isinstance(h_p, str) else h_p
    print()
    print(f"M-proj KL(p || q) : {kl_p_q}")
    print(f"I-proj KL(q || p) : {kl_q_p}")
    print(f"             H(q) : {h_q}")
    print(f"             H(p) : {h_p}")
    print(f"         CE(q, p) : {ce_q_p}")
    print(f"         CE(p, q) : {ce_p_q}")


def update():
    fig.canvas.draw_idle()
    print_vals()
    

def update_mean(d, d_artists):
    d_artists[f'{d.name}_mean'].set_offsets(d.get_mean())
    update()


def update_mean_x(val, d, d_artists):
    d.mean[0] = val
    update_mean(d, d_artists)
    
    
def update_mean_y(val, d, d_artists):
    d.mean[1] = val
    update_mean(d, d_artists)


def update_cov(d, d_artists):
    for i in range(args.n_std):
        n_std = i+1
        w, h, theta = vis_util.get_cov_ellipse(d.get_cov(), n_std=n_std)
        d_artists[f'{d.name}_std_{n_std}'].set_width(w)
        d_artists[f'{d.name}_std_{n_std}'].set_height(h)
        d_artists[f'{d.name}_std_{n_std}'].set_angle(theta)
    update()
    

def update_cov_xx(val, d, d_artists):
    d.cov[0,0] = val
    update_cov(d, d_artists)


def update_cov_yy(val, d, d_artists):
    d.cov[1,1] = val
    update_cov(d, d_artists)


def update_cov_xy(val, d, d_artists):
    d.cov[0,1] = val
    d.cov[1,0] = val
    update_cov(d, d_artists)


def update_uniform(d, d_artists):
    low = d.get_low()
    high = d.get_high()
    d_artists.set_xy(low)
    d_artists.set_width(high[0] - low[0])
    d_artists.set_height(high[1] - low[1])
    update()
    
    
def update_low_x(val, d, d_artists):
    d.low[0] = val
    update_uniform(d, d_artists)


def update_high_x(val, d, d_artists):
    d.high[0] = val
    update_uniform(d, d_artists)


def update_low_y(val, d, d_artists):
    d.low[1] = val
    update_uniform(d, d_artists)


def update_high_y(val, d, d_artists):
    d.high[1] = val
    update_uniform(d, d_artists)


def update_dirac(d, d_artists):
    d_artists.set_offsets(d.get_point())
    update()
    

def update_x(val, d, d_artists):
    d.point[0] = val
    update_dirac(d, d_artists)


def update_y(val, d, d_artists):
    d.point[1] = val
    update_dirac(d, d_artists)

    
def set_global(name, obj):
    globals()[name] = obj


def get_global(name):
    return globals()[name]


def create_gaussian_sliders(d, d_artists, start_B):
    B = start_B
    set_global(f'ax_{d.name}_mean_x', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_mean_x_slider', Slider(
        ax=get_global(f'ax_{d.name}_mean_x'),
        label=f'{d.name} Mean X',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_mean()[0]
    ))
    get_global(f'{d.name}_mean_x_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_mean_x(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_mean_y', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_mean_y_slider', Slider(
        ax=get_global(f'ax_{d.name}_mean_y'),
        label=f'{d.name} Mean Y',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_mean()[1]
    ))
    get_global(f'{d.name}_mean_y_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_mean_y(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_cov_xx', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_cov_xx_slider', Slider(
        ax=get_global(f'ax_{d.name}_cov_xx'),
        label=f'{d.name} Cov XX',
        valmin=1e-5,
        valmax=10.,
        valinit=d.get_cov()[0,0]
    ))
    get_global(f'{d.name}_cov_xx_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_cov_xx(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_cov_yy', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_cov_yy_slider', Slider(
        ax=get_global(f'ax_{d.name}_cov_yy'),
        label=f'{d.name} Cov YY',
        valmin=1e-5,
        valmax=10.,
        valinit=d.get_cov()[1,1]
    ))
    get_global(f'{d.name}_cov_yy_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_cov_yy(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_cov_xy', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_cov_xy_slider', Slider(
        ax=get_global(f'ax_{d.name}_cov_xy'),
        label=f'{d.name} Cov XY',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_cov()[0,1]
    ))
    get_global(f'{d.name}_cov_xy_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_cov_xy(z, d, d_artists))


def create_uniform_sliders(d, d_artists, start_B):
    B = start_B
    set_global(f'ax_{d.name}_low_x', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_low_x_slider', Slider(
        ax=get_global(f'ax_{d.name}_low_x'),
        label=f'{d.name} Low X',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_low()[0]
    ))
    get_global(f'{d.name}_low_x_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_low_x(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_high_x', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_high_x_slider', Slider(
        ax=get_global(f'ax_{d.name}_high_x'),
        label=f'{d.name} High X',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_high()[0]
    ))
    get_global(f'{d.name}_high_x_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_high_x(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_low_y', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_low_y_slider', Slider(
        ax=get_global(f'ax_{d.name}_low_y'),
        label=f'{d.name} Low Y',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_low()[1]
    ))
    get_global(f'{d.name}_low_y_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_low_y(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_high_y', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_high_y_slider', Slider(
        ax=get_global(f'ax_{d.name}_high_y'),
        label=f'{d.name} High Y',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_high()[1]
    ))
    get_global(f'{d.name}_high_y_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_high_y(z, d, d_artists))


def create_dirac_sliders(d, d_artists, start_B):
    B = start_B
    set_global(f'ax_{d.name}_x', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_x_slider', Slider(
        ax=get_global(f'ax_{d.name}_x'),
        label=f'{d.name} X',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_point()[0]
    ))
    get_global(f'{d.name}_x_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_x(z, d, d_artists))

    B -= SLIDER_DIFF
    set_global(f'ax_{d.name}_y', fig.add_axes([SLIDER_L, B, SLIDER_W, SLIDER_H]))
    set_global(f'{d.name}_y_slider', Slider(
        ax=get_global(f'ax_{d.name}_y'),
        label=f'{d.name} Y',
        valmin=-10.,
        valmax=10.,
        valinit=d.get_point()[1]
    ))
    get_global(f'{d.name}_y_slider').on_changed(
        lambda z, d=d, d_artists=d_artists: update_y(z, d, d_artists))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_std', type=int, default=5)
    parser.add_argument('-d', '--target_distribution', type=str, default='gaussian',
                        choices=['gaussian', 'uniform', 'dirac', 'gmm'])
    args = parser.parse_args()

    
    fig, ax = create_fig()
    
    if args.target_distribution == 'gaussian':
        p = Gaussian(np.array([0., 5.]), 0.3 * np.eye(2), name='p')
        p_artists = p.draw(ax, n_std=args.n_std)
        create_gaussian_sliders(p, p_artists, 0.33)
    elif args.target_distribution == 'uniform':
        p = Uniform(np.array([-2., 3.]), np.array([2., 7.]), name='p')
        p_artists = p.draw(ax)
        create_uniform_sliders(p, p_artists, 0.33)
    elif args.target_distribution == 'dirac':
        p = DiracDelta([0., 5.], name='p')
        p_artists = p.draw(ax)
        create_dirac_sliders(p, p_artists, 0.33)
    elif args.target_distribution == 'gmm':
        p = GMM(np.array([0.5, 0.5]), np.array([[-4., 5.], [4., 5.]]),
                np.repeat(np.expand_dims(0.3 * np.eye(2), 0), 2, axis=0))
        p_artists = p.draw(ax)                         
    else:
        raise ValueError(f"Unknown target distribution type: {args.target_distribution}")

    q = Gaussian(np.array([0., -5.]), 0.3 * np.eye(2), name='q')
    q_artists = q.draw(ax, n_std=args.n_std, cmap='Purples')
    create_gaussian_sliders(q, q_artists, 0.15)
    
    plt.show()
