#!/usr/bin/env python
"""
Run the following in the directory containing this file to evaluate tests:
    
    $ pytest
"""
import math
import torch
import numpy as np

from distribution_planning.distributions.util import unscented_transform as UT


def test_ut_wikipedia():
    """
    Example from Wikipedia: https://en.wikipedia.org/wiki/Unscented_transform#Example
    """
    mu = torch.tensor([12.3, 7.6]).unsqueeze(0)
    sigma = torch.tensor([[1.44, 0.], [0, 2.89]]).unsqueeze(0)

    def g(x):
        r = torch.sqrt(x[:,0]**2 + x[:,1]**2)
        theta = torch.atan(x[:,1] / x[:,0])
        return torch.stack([r, theta], dim=-1)
        
    mu_prime, sigma_prime, sigma_points = UT(mu, sigma, g, math.sqrt(2))
    mu_prime = mu_prime.squeeze().numpy()
    sigma_prime = sigma_prime.squeeze().numpy()
    sigma_points = sigma_points.squeeze().numpy()

    mu_actual = np.array([14.545, 0.550])
    sigma_actual = np.array([[1.823, 0.043],
                             [0.043, 0.012]])
    points_actual = np.array([[15.927, 0.497],
                              [15.854, 0.683],
                              [13.045, 0.622],
                              [13.352, 0.400]])

    assert np.allclose(mu_prime, mu_actual, atol=1e-2)
    assert np.allclose(sigma_prime, sigma_actual, atol=1e-2)
    assert np.allclose(sigma_points[1:], points_actual[1:], atol=1e-2)
