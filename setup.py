#!/usr/bin/env python
from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

args = generate_distutils_setup(
    packages=[
        'distribution_planning',
        'distribution_planning.environments',
        'distribution_planning.problems',
        'distribution_planning.util'
    ],
    package_dir={'': 'src'}
)

setup(**args)
